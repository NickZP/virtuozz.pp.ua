<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    public function handle($request, Closure $next)
    {
        if(empty(\Auth::user()) || !\Auth::user()->hasRole(['admin'])){
            return redirect()->route('user.showlogin');
        }
                
        return $next($request);
    }
}
