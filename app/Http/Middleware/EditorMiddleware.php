<?php

namespace App\Http\Middleware;

use Closure;

class EditorMiddleware
{
    public function handle($request, Closure $next)
    {
        if(empty(\Auth::user()) || !\Auth::user()->hasRole(['admin', 'editor'])){
            return redirect()->route('user.showlogin');
        }
        
        return $next($request);
    }
}
