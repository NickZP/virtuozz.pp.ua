<?php

/*** Админка ***/
//секции
Breadcrumbs::register('sections', function($breadcrumbs)
{
    $breadcrumbs->push('Разделы', route('sections'));
});

Breadcrumbs::register('section_new', function($breadcrumbs)
{
    $breadcrumbs->parent('sections');
    $breadcrumbs->push('Создать раздел', route('sections'));
});

//категории
Breadcrumbs::register('categories', function($breadcrumbs)
{
    $breadcrumbs->push('Категории', route('categories'));
});

Breadcrumbs::register('category_new', function($breadcrumbs)
{
    $breadcrumbs->parent('categories');
    $breadcrumbs->push('Создать категорию', route('category_new'));
});

Breadcrumbs::register('category_edit', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('categories');
    $breadcrumbs->push('Редактирование категории: ' . $category->text, route('category_edit', $category->id));
});

//товары
Breadcrumbs::register('products', function($breadcrumbs)
{
    $breadcrumbs->push('Товары', route('products'));
});

Breadcrumbs::register('product_new', function($breadcrumbs)
{
    $breadcrumbs->parent('products');
    $breadcrumbs->push('Добавить товар', route('product_new'));
});

Breadcrumbs::register('product_edit', function($breadcrumbs, $product)
{
    $breadcrumbs->parent('products');
    $breadcrumbs->push('Редактировать товар: ' . $product->shortname, route('product_edit', $product->id));
});

//пользователи
Breadcrumbs::register('users', function($breadcrumbs)
{
    $breadcrumbs->push('Пользователи', route('admin.users.index'));
});

Breadcrumbs::register('user_new', function($breadcrumbs)
{
    $breadcrumbs->parent('users');
    $breadcrumbs->push('Добавить пользователя', route('admin.users.create'));
});

Breadcrumbs::register('user_edit', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('users');
    $breadcrumbs->push('Редактировать пользователя: ' . $user->user->name, route('admin.users.edit', $user->user->id));
});

Breadcrumbs::register('user_show', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('users');
    $breadcrumbs->push('Пользователь: ' . $user->name, route('admin.user.show', $user->id));
});

//редакторы
Breadcrumbs::register('editors', function($breadcrumbs)
{
    $breadcrumbs->push('Редакторы', route('admin.editors.index'));
});

Breadcrumbs::register('editor_new', function($breadcrumbs)
{
    $breadcrumbs->parent('editors');
    $breadcrumbs->push('Добавить редактора', route('admin.editors.create'));
});

Breadcrumbs::register('editor_edit', function($breadcrumbs, $editor)
{
    $breadcrumbs->parent('editors');
    $breadcrumbs->push('Редактировать редактора: ' . $editor->user->name, route('admin.editors.edit', $editor->user->id));
});

//статические страницы
Breadcrumbs::register('static-pages', function($breadcrumbs)
{
    $breadcrumbs->push('Статические страницы', route('admin.static-pages.index'));
});

Breadcrumbs::register('static-pages_new', function($breadcrumbs)
{
    $breadcrumbs->parent('static-pages');
    $breadcrumbs->push('Создать страницу', route('admin.static-pages.create'));
});

Breadcrumbs::register('static-pages_edit', function($breadcrumbs, $page)
{
    $breadcrumbs->parent('static-pages');
    $breadcrumbs->push('Редактировать страницу: ' . $page->title, route('admin.static-pages.edit', $page->id));
});

//продажи
Breadcrumbs::register('orders', function($breadcrumbs)
{
    $breadcrumbs->push('Заказы', route('admin.order.index'));
});

Breadcrumbs::register('orders_view', function($breadcrumbs, $order)
{
    $breadcrumbs->parent('orders');
    $breadcrumbs->push('Просмотр заказа', route('admin.order.view', $order->id));
});
/*** конец админки***/

/*** Клиентская ***/
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Главная', route('index'));
});

/* BEGIN Меню */
Breadcrumbs::register('section', function($breadcrumbs, $section)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push($section->text, route('frontend.main.getsection', \Request::segment(1)));
});

Breadcrumbs::register('category', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('section', $category->section);
    $breadcrumbs->push($category->text, route('frontend.main.getcategory', [\Request::segment(1), \Request::segment(2)]));
});

Breadcrumbs::register('product', function($breadcrumbs, $product, $category)
{
    $breadcrumbs->parent('category', $category);
    $breadcrumbs->push($product->shortname, route('frontend.main.getproduct', [\Request::segment(1), \Request::segment(2), \Request::segment(3)]));
});
/* END Меню */

//статические страницы
Breadcrumbs::register('page', function($breadcrumbs, $page)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push($page->title, action('Frontend\MainController@staticPage'));
});

Breadcrumbs::register('contact', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Контакты', action('Frontend\MainController@Contact'));
});