<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', ['as' => 'index', 'uses' => 'Frontend\MainController@index']);

//ajax
Route::post('/sections/update', ['as' => 'sections_update', 'uses' => 'SectionController@update']);
//Route::get('/sections/update', ['as' => 'sections_update', 'uses' => 'SectionController@update']);
//Route::get('/sections/update', function(){return View::make('admin.section');});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //если не показывает DebugBar
    //start DBar/*
    Route::get('/_debugbar/assets/stylesheets', [
    'as' => 'debugbar-css',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@css'
    ]);

    Route::get('/_debugbar/assets/javascript', [
        'as' => 'debugbar-js',
        'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@js'
    ]);

    Route::get('/_debugbar/open', [
        'as' => 'debugbar-open',
        'uses' => '\Barryvdh\Debugbar\Controllers\OpenController@handler'
    ]);
    //endDbar*/
    Route::auth();
    Route::get('404', ['as' => '404', 'uses' => 'Frontend\MainController@pageNotFound']);
    Route::get('/', ['as' => 'index', 'uses' => 'Frontend\MainController@index']);
    Route::post('search', ['as' => 'search', 'uses' => 'Frontend\MainController@search']);
    Route::get('login', ['as' => 'user.showlogin', 'uses' => 'Frontend\UserController@showLogin']);
    Route::post('login', ['as' => 'user.login', 'uses' => 'Frontend\UserController@login']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'Frontend\UserController@logout']);
    Route::get('registration', ['as' => 'user.showregister', 'uses' => 'Frontend\UserController@showRegister']);
    Route::post('registration', ['as' => 'user.register', 'uses' => 'Frontend\UserController@register']);
    Route::get('basket', ['as' => 'frontend.basket.show', 'uses' => 'Frontend\BasketController@show']);
    Route::post('basket', ['as' => 'frontend.basket.checkout', 'uses' => 'Frontend\BasketController@Checkout']);
    
    /*меню (navbar)*/
    Route::get('/about-company', ['as' => 'about-company', 'uses' => 'Frontend\MainController@staticPage']);
    Route::get('/dileram', ['as' => 'dileram', 'uses' => 'Frontend\MainController@staticPage']);
    Route::get('/faq', ['as' => 'faq', 'uses' => 'Frontend\MainController@staticPage']);
    Route::get('/sertifikat', ['as' => 'sertifikat', 'uses' => 'Frontend\MainController@staticPage']);
    Route::get('/oplata-dostavka', ['as' => 'oplata-dostavka', 'uses' => 'Frontend\MainController@staticPage']);
    Route::get('/contact', ['as' => 'contact', 'uses' => 'Frontend\MainController@Contact']);
    Route::post('/contact', ['as' => 'postcontact', 'uses' => 'Frontend\MainController@PostContact']);
    /*конец меню*/

    /*ajax*/
    Route::post('/product-ajax', ['as' => 'frontend.main.get-product-ajax', 'uses' => 'Frontend\MainController@getProductAjax']);
    Route::post('/products-sort-ajax', ['as' => 'frontend.main.products-sort-ajax', 'uses'  => 'Frontend\MainController@ProductsSortAjax']);

    Route::group(['prefix' => 'user', 'middleware' => ['auth']], function(){
        Route::get('profile', ['as' => 'frontend.user.index', 'uses' => 'Frontend\UserController@index']);
        Route::post('profile', ['as' => 'frontend.user.profile', 'uses' => 'Frontend\UserController@profile']);
        Route::get('orders', ['as' => 'frontend.order.index', 'uses' => 'Frontend\OrderController@index']);
    });
    
    /*!!!Админка!!!*/
    Route::group(['prefix' => 'admin'], function(){
        
        //Route::post('login', ['as' => 'login', 'uses' => 'UserController@login']);
        //Route::get('login', ['as' => 'show', 'uses' => 'UserController@showLogin']);
        
        Route::group(['middleware' => ['editor']], function () {
            
            $resourceExcept = ['except' => ['destroy', 'store', 'show']];
            
            Route::get('sections', ['as' => 'sections', 'uses' => 'SectionController@index']);
            Route::post('sections', ['as' => 'postSections', 'uses' => 'SectionController@postSections']);
            Route::get('sections/new', ['as' => 'section_new', 'uses' => 'SectionController@newSection']);
            Route::post('sections/new', ['as' => 'postSection_new', 'uses' => 'SectionController@postNewSection']);
            Route::get('sections/cancel', ['as' => 'section_cancel', 'uses' => 'SectionController@cancelSection']);
            
            Route::get('categories', ['as' => 'categories', 'uses' => 'CategoryController@index']);
            Route::post('categories', ['as' => 'categories', 'uses' => 'CategoryController@index']);
            Route::get('category/edit/{id}', ['as' => 'category_edit', 'uses' => 'CategoryController@edit']);
            Route::post('category/edit/', ['as' => 'postCategory_edit', 'uses' => 'CategoryController@postEdit']);
            Route::get('category/new', ['as' => 'category_new', 'uses' => 'CategoryController@newCategory']);
            Route::post('category/new', ['as' => 'postCategory_new', 'uses' => 'CategoryController@postNewCategory']);
            Route::get('category/delete/{id}', ['as' => 'category_delete', 'uses' => 'CategoryController@delete']);
            
            Route::get('products', ['as' => 'products', 'uses' => 'ProductController@index']);
            Route::post('products', ['as' => 'products', 'uses' => 'ProductController@index']);
            Route::get('product/new', ['as' => 'product_new', 'uses' => 'ProductController@newProduct']);
            Route::post('product/new', ['as' => 'postProduct_new', 'uses' => 'ProductController@postNewProduct']);
            Route::get('product/edit/{id}', ['as' => 'product_edit', 'uses' => 'ProductController@edit']);
            Route::post('product/edit', ['as' => 'postProduct_edit', 'uses' => 'ProductController@postEdit']);
            Route::get('product/delete/{id}', ['as' => 'product_delete', 'uses' => 'ProductController@delete']);
            
            Route::group(['middleware' => ['admin']], function() use ($resourceExcept){
                /*Пользователи*/
                Route::resource('users', 'UserController', $resourceExcept);
                Route::get('users/{id}', ['as' => 'admin.user.show', 'uses' => 'UserController@show'] );
                Route::post('users/new', ['as' => 'admin.users.store', 'uses' => 'UserController@store']);
                Route::get('users/{id}/delete', ['as' => 'admin.users.delete', 'uses' => 'UserController@delete']);
                //для ajax
                Route::post('users', ['as' => 'admin.users.index', 'uses' => 'UserController@index']);
                
                /*Редакторы*/
                Route::resource('editors', 'EditorController', $resourceExcept);
                Route::post('editors/new', ['as' => 'admin.editors.store', 'uses' => 'EditorController@store']);
                Route::get('editors/{id}/delete', ['as' => 'admin.editors.delete', 'uses' => 'EditorController@delete']);
                //для ajax
                Route::post('editors', ['as' => 'admin.editors.index', 'uses' => 'EditorController@index']);
                
                /*Статические страницы*/
                Route::resource('static-pages', 'StaticPageController', $resourceExcept);
                Route::post('static-pages/new', ['as' => 'admin.static-pages.store', 'uses' => 'StaticPageController@store']);
                Route::get('static-pages/{id}/delete', ['as' => 'admin.static-pages.delete', 'uses' => 'StaticPageController@delete']);
                
                /*Продажи*/
                Route::get('orders', ['as' => 'admin.order.index', 'uses' => 'OrderController@index']);
                Route::get('order/{id}', ['as' => 'admin.order.view', 'uses' => 'OrderController@view']);
                Route::post('order/{id}', ['as' => 'admin.order.state_change', 'uses' => 'OrderController@stateChange']);
                //для ajax
                Route::post('orders', ['as' => 'admin.order.index', 'uses' => 'OrderController@index']);

                /*SEO*/
                Route::get('seo', ['as' => 'admin.seo.index', 'uses' => 'SEOController@index']);
                //для ajax
                Route::post('seo', ['as' => 'admin.seo.index', 'uses' => 'SEOController@index']);
                Route::post('seo/save', ['as' => 'admin.seo.save', 'uses' => 'SEOController@save']);
                Route::post('seo/{id}', ['as' => 'admin.seo.get', 'uses' => 'SEOController@get']);
            });
        });
    });
    //
    //для разделы, категории, продукты
    /*Route::get('{category}', [
        'uses'  => 'Frontend\MainController@getCategory'
    ])->where('category', '([A-Za-z0-9\-\/]+)');
    */
    /*Ручные разделы (Для вывода индивидуальной информации)*/
    Route::get('armatura-steklo-compozit/armatura-steklo-compozit', ['as' => 'frontend.main.getcategoryarmatura', 'uses'  => 'Frontend\MainController@getCategoryArmatura']);
    Route::get('armatura-steklo-compozit/setka-compozit-polimer', ['as' => 'frontend.main.getcategorysetka', 'uses'  => 'Frontend\MainController@getCategorySetka']);

    /*Автоматические разделы*/
    Route::get('{section}', ['as' => 'frontend.main.getsection', 'uses'  => 'Frontend\MainController@getSection']);
    Route::get('{section}/{category}', ['as' => 'frontend.main.getcategory', 'uses'  => 'Frontend\MainController@getCategory']);
    Route::get('{section}/{category}/{product}', ['as' => 'frontend.main.getproduct', 'uses'  => 'Frontend\MainController@getProduct']);
});


Route::group(['middleware' => 'web'], function () {

    Route::get('/home', 'HomeController@index');
});
