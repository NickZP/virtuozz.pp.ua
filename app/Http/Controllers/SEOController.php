<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \App\Models\Product;
use \App\Models\SEO;

class SEOController extends Controller
{
    public function index(Request $request){

        $limit = \App\Helpers\Helper::getLimit();

        $products = Product::with('SEO')->paginate($limit);

        //$product = Product::find(4);
        //dd( empty($product->seo->og_description) );
        //dd( $products );

        if( $request->ajax() ){

            $products = Product::paginate($limit);
            
            return \Response::json(view('admin.SEOController.ajax.seoList', compact('products'))->render());            
        }

        return view('admin.SEOController.index', compact('products'));
    }

    public function save(Request $request){
        if( $request->ajax() ){

            $product = Product::find($request->get('id'));
            if( $product->seo ){

                $product->seo->keywords = $request->get('description');
                $product->seo->save();
            }
            else{

                $seo = SEO::create([
                    'keywords' => $request->get('description')
                ]);
                $product->seo_id = $seo->id;
                $product->save();
            }

            $product = Product::find($request->get('id'));
            //66ff66
            return \Response::json([
                                    'ok'    => 'true', 
                                    'count' => $product->seo->count_keywords
                                    ]); 
        }

        return \Response::json( ['ok' => 'false'] );
    }

    public function get(Request $request){
        if( $request->ajax() ){
            
            $product = Product::find($request->get('id'));

            if( $product->seo ){
                return \Response::json( $product->seo->keywords ); 
            }

            return \Response::json( '' );
        }
    }
}
