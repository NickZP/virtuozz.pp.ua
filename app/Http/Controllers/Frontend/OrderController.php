<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

class OrderController extends Controller
{
    use \App\Http\Controllers\Traits\Order;
    
    public function index(){
        //$order = \Auth::user()->orders->first();
        //dd($order->state->name);        
        return view('frontend.OrderController.index');
    }
}
