<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserInfo;
use App\Models\Section;

class UserController extends Controller
{
    use \App\Http\Controllers\Traits\User;
    
    public function showLogin(){
        if(\Auth::check()){
            return \Redirect::route('index');    
        }
        
        return view('frontend.UserController.showLogin');
    }
    
    public function showRegister()
    {
        if(\Auth::check()){
            return \Redirect::route('index');    
        }
        
        return view('frontend.UserController.showRegister');
    }
    
    public function index(){
        return view('frontend.UserController.index');
    }
    
    public function profile(Requests\Frontend\UserController\profileRequest $request){

        $user = \Auth::user();
        $user->email = $request->get('email');
        if ( ! empty($request->get('old_password')) && ! empty($request->get('password'))){
            $user->password = bcrypt($request->get('password'));
        }
        $user->save();
        
        $user->userInfo->fill($request->only('email', 'phone'))->save();
        
        $messages[] = "Данные обновлены";
        
        return \Redirect::back()->with(['messages' => $messages]);
    }
    
    public function orders(){
        return view('frontend.UserController.order');
    }
}