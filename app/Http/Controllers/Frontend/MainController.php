<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Section;
use App\Models\Category;
use App\Models\VariantSale;
use App\Models\StaticPage;
use App\Helpers\Helper;
use \File;

use SEO;

class MainController extends Controller
{    
    public function index()
    {     
        return view('frontend.MainController.index', [
                                                        'sections' => Section::all()
                                                     ]);
    }
    
    public function product($id)
    {
        
        $product = Product::find($id);
        
        return view('frontend.MainController.product', ['product' => $product]);
    }
    
    public function staticPage()
    {
        $page = StaticPage::where('tag', \Request::segment(1))->first();
        
        if(is_null($page)){
            //$message = 'Данная страница не существует';
            
            return view('frontend.404');
        }
        
        return view('frontend.MainController.staticPage', compact('page'));
    }
    
    /*
    public function getPage(){
        //dd(\Route::current()->getName());
        $route = \Route::current();
        $slug = $route->getParameter('slug');
        //dd(\Route::current());
        if(!is_null(\Request::segment(2))){
            $category = Category::where("route", "/" . \Request::segment(1) . "/" . \Request::segment(2))->get();
            
            return view('frontend.MainController.getPage-category', ['sections' => $this->sections]);
            //dd($category);
        }
        else{
            if(!is_null(\Request::segment(1))){
                $section = Section::where("route", "/" . \Request::segment(1))->get();
                if(!$section->isEmpty()){
                    $section = $section->load('categories')->first();
                    
                    return view('frontend.MainController.getPage-section', ['section' => $section, 'sections' => $this->sections]);
                }
                else{
                    return '404';
                }
            }
        }
        dd(\Request::segment(2));
    }
    */
    
    public function getSection($section){
        
        $section = Section::where("route", "/" . $section)->get();
        if(!$section->isEmpty()){
            $section = $section->load('categories')->first();

            $description = $section->text . Helper::getSEODescription();
            SEO::metatags()->setDescription($description);
            $arr = array_merge(
                    [$section->text], 
                    Helper::getCommonSEOKeywords()
                    );
            SEO::metatags()->addKeyword($arr);
            
            return view('frontend.MainController.getPage-section', compact('section'));
        }
        else{
           $message = 'Данный раздел отсутствует';
           
           return view('frontend.404')->with(['message' => $message]);
        }
    }
    
    public function getCategory($section, $category)
    {
        $arr = $this->categoryCreateContent($section, $category);



        return view('frontend.MainController.getPage-category', [
            'products'      => $arr['products'],
            'category'      => $arr['category'],
            'category_url'  => $arr['category_url'],
            'sort_arr'      => $arr['sort_arr'],
            'view'          => 'th',
        ]);
    }
    
    public function getProduct($section, $category, $product)
    {    
        $product = Product::whereHas('categories', function($query) use ($section, $category){
                                    $query->where("route", "/" . $section . "/" . $category);
                                })
                            ->where('route', $product)
                            ->get();
       
        $categoryProduct = Category::where("route", "/" . $section . "/" . $category)->first();

        if(!$product->isEmpty()){

            $product = $product->first();
            //удаляем символ " с названия
            $name = str_replace("\"", "", [ $product->shortname, $product->name ]);
            $description =  $name[0] . Helper::getSEODescription();
            SEO::metatags()->setDescription($description);

            //добавление keywords
            $arr = array_merge(
                                [$name[0]], 
                                [$categoryProduct->text], 
                                Helper::getCommonSEOKeywords()
                                );
            if ( !is_null($product->seo) ){
                if ( !empty($product->seo->count_keywords) ){
                    $arr = array_merge(
                                        [$name[0]], 
                                        [$categoryProduct->text], 
                                        $product->seo->keywords_arr, 
                                        Helper::getCommonSEOKeywords()
                                        );
                }
            }
            $product_url = Helper::getDomain() . '/' . $section . '/' . $category . '/' .  $product->route;
            SEO::metatags()->addKeyword($arr);

            SEO::opengraph()->setType('product');
            SEO::opengraph()->setTitle($name[0]);
            SEO::opengraph()->setDescription($name[1]);
            SEO::opengraph()->setSiteName('virtuozz');
            SEO::opengraph()->setUrl($product_url);
            SEO::opengraph()->addImage(Helper::getDomain() . '/' . $product->image);

            return view('frontend.MainController.getPage-product', [
                                    'product'       => $product,
                                    'category'      => $categoryProduct,
                                    'product_url'   => $product_url,
                                ]);
        }
        else{
           $message = 'Данный товар отсутствует';
           
           return view('frontend.404')->with(['message' => $message]);
        }
    }
    
    /* При нажатии "купить" (модальное окно) подгружаются варианты загрузок */
    public function getProductAjax()
    {
        if(\Request::ajax()){
            $product = Product::find(\Request::get('id'));
            if($product){
                return \Response::json(view('frontend.MainController.ajax.variant-sale', compact('product'))->render());   
            }
            else{
                return '404';
            }
        }
    }
    
    public function search(Request $request)
    {
        $limit = Helper::getLimit();
        
        if($request->ajax()){
            $products = Product::where('name', 'LIKE', '%'.$request->get('search').'%')
                                ->skip($request->get('page') * $limit)
                                ->paginate($limit);
                                
            return \Response::json(view('frontend.MainController.ajax.content', [
                                    'products'  => $products,
                                    'count'     => $request->get('page') * $limit - 9
                                ])->render());
        }
        else{
            $products = Product::where('name', 'LIKE', '%'.$request->get('search').'%')->paginate($limit);
        
            return view('frontend.MainController.search', [
                                    'products'  => $products,
                                    'search'    => $request->get('search'),
                                    'count'     => 1
                                ]);
        }
        
    }

    public function getCategoryArmatura(Request $request)
    {
        $url = explode("/", $request->path());
        $arr = $this->categoryCreateContent($url[0], $url[1]);

        return view('frontend.MainController.get-category-armatura', [
                                    'products'      => $arr['products'],
                                    'category'      => $arr['category'],
                                    'category_url'  => $arr['category_url'],
                                    'sort_arr'      => $arr['sort_arr'],
                                    'view'          => 'th',
                                ]);

    }

    public function getCategorySetka(Request $request)
    {
        $url = explode("/", $request->path());
        $arr = $this->categoryCreateContent($url[0], $url[1]);
        $path_picture = 'img/gidro/setka';
        $public_path_setka = public_path($path_picture);
        $pictures = [];
        if( File::isDirectory($public_path_setka) ){
            $pictures = scandir( $public_path_setka );
        }

        return view('frontend.MainController.get-category-setka', [
                                    'products'      => $arr['products'],
                                    'category'      => $arr['category'],
                                    'category_url'  => $arr['category_url'],
                                    'sort_arr'      => $arr['sort_arr'],
                                    'path_picture'  => $path_picture,
                                    'pictures'      => $pictures,
                                    'view'          => 'th',
        ]);

    }

    protected function categoryCreateContent($section, $category)
    {
        $arr_result = [];

        if(is_null($category)){
           $message = 'Данная категория отсутствует';
           
           return view('frontend.404')->with(['message' => $message]);
        }

        $route_category = "/" . $section . "/" . $category;
        $categoryProduct = Category::where("route", $route_category)->first();
        
        $products = Product::getSortName($route_category);
        
        $description = $categoryProduct->text . Helper::getSEODescription();
        SEO::metatags()->setDescription($description);
        $arr = array_merge(
                [$categoryProduct->text], 
                [$categoryProduct->section->text],
                Helper::getCommonSEOKeywords()
                );
        SEO::metatags()->addKeyword($arr);

        $category_url = \App\Helpers\Helper::getDomain() . $categoryProduct->route.'/';

        $arr_result['products']     = $products;
        $arr_result['category']     = $categoryProduct;
        $arr_result['category_url'] = $category_url;
        $arr_result['sort_arr']     = ['name' => 'название', 'price' => 'цена'];

        return $arr_result;
    }

    public function ProductsSortAjax(Request $request){
        if ( $request->ajax() ){
            $category = Category::where("route", $request->get('uri'))->first();

            if ($request->get('sort-name') === 'name'){
                $products = Product::getSortName($request->get('uri'), $request->get('sort-orderby'), $request->get('page'));
            }
            else{
                $price = ($request->get('sort-orderby') === 'desc') ? 'max' : 'min';
                $products = Product::getSortPrice($request->get('uri'), $price, $request->get('page'));
            }

            $category_url = \App\Helpers\Helper::getDomain() . $category->route.'/';
            return \Response::json(view('frontend.MainController.include.listProducts', [
                'products'      => $products,
                'category'      => $category,
                'view'          => $request->get('view'),
                'category_url'  => $category_url,
            ])->render());
        }
    }

    public function Contact()
    {
        return view('frontend.MainController.contact');
    }

    public function PostContact(Requests\Frontend\ContactRequest $request)
    {
        try {
            \Mail::send('emails.contact', ['data' => $request], function($message){
                $message->to('zp220284@gmail.com')->subject('Обратная связь');
            });

            $messages[] = "Сообщение отправлено";

            return \Redirect::route('contact')->with(['messages' => $messages]);
        }
        catch (Exception $e) {
            if( count(\Mail::failures()) > 0 ){
                $errors = 'Ошибка отправки письма, пожалуйста, повторите попытку позже';
            }
        }
    }
    
    public function pageNotFound()
    {
        return view('frontend.404');
    }
}
