<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Section;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderHistory;
use App\Models\OrderInfo;

class BasketController extends Controller
{    
    public function show(Request $request){
        
        $sum = 0;
        $new_arr = [];
        if(isset($_COOKIE['basket'])){
            $orders = $_COOKIE['basket'];
            $orders = json_decode($orders);
            
            $ids = [];
            foreach($orders as $order){
                $ids[] = $order->item_id;
                $new_arr[$order->title . $order->measurement] = $order->amount;
            }
            
            $products = Product::whereIn('id', array_unique($ids))->get();
        }
        else{
            $products = [];
        }
        
        $parametrs = [
                        'products' => $products, 
                        'new_arr' => $new_arr, 
                        'sum' => $sum,
                     ];
                     
        if($request->ajax()){
            return \Response::json(view('frontend.BasketController.ajax.zakaz', $parametrs)->render())->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');    
        }
        else{
            return \Response::view('frontend.BasketController.show', $parametrs)->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        }      
    }
    
    public function Checkout(Requests\Frontend\BasketController\checkoutRequest $request){
                
        if(isset($_COOKIE['basket'])){
            $orders = $_COOKIE['basket'];
            $orders = json_decode($orders);
        }
        else{
            return redirect('/');
        }

        
        $orderDB = new Order;
        
        if(\Auth::check()){
            $orderDB->user_id = \Auth::user()->id;
        }
        else{
            $orderDB->user_id = 0;
        }
        
        $orderDB->order_state_id = 1;
        $orderDB->save();
        
        $orderHistory = $orderDB->history()->create([
                'email'     => $request->email,
                'name'      => $request->name,
                'phone'     => $request->phone,
                'history'   => $_COOKIE['basket'],
            ]);
            
        $orderStateHistory = $orderDB->orderStateHistories()->create([
                'order_state_id'    => 1,
                'changed_at'        => \Carbon\Carbon::now(),
        ]);
    
        $total_price = 0;
        foreach($orders as $order){
            $variantSale = Product::find($order->item_id)
                                    ->variantsales()
                                    ->where('measurement', $order->measurement)
                                    ->first();
            
            $orderDB->infos()->create([
                    'variant_sale_id'   => $variantSale->id,
                    'amount'            => $order->amount,
                ]);
                
            $total_price += $variantSale->price * $order->amount;
        }
        
        $orderHistory->price = $total_price;
        $orderHistory->save();
        
        SetCookie("basket","");
        
        //Отправка письма клиенту
        try {
            \Mail::send('emails.basket.checkout', ['order' => $orderDB], function($message) use ($request){
                $message->to($request->email)->subject('Ваш заказ');
            });
        }
        catch (Exception $e) {
            if( count(\Mail::failures()) > 0 ){
                $errors = 'Ошибка отправки письма, пожалуйста, повторите попытку позже';
            }
        }
        
        //Отправка письма администратору о новом заказе
        try {
            \Mail::send('emails.basket.admin.order', ['order' => $orderDB], function($message){
                $message->to('zp220284@gmail.com')->subject('Новый заказ');
            });
        }
        catch (Exception $e) {
            if( count(\Mail::failures()) > 0 ){
                $errors = 'Ошибка отправки письма, пожалуйста, повторите попытку позже';
            }
        }

        return view('frontend.BasketController.checkout', ['order' => $orderDB, 'total_price' => $total_price ]);
    }
}
