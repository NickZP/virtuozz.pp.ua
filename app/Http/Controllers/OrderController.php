<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use \App\Models\Order;
use \App\Models\OrderState;

class OrderController extends Controller
{
    public function index(Request $request){
        
        $limit = \App\Helpers\Helper::getLimit();

        if( $request->ajax() ){
            $orders = Order::with('history', 'user', 'state')
                            ->Priority()
                            ->skip($request->get('page') * $limit)
                            ->paginate($limit);
            
            return \Response::json(view('admin.orderController.ajax.orderList', ['orders' => $orders])->render()); 
        }
        else{
            return view('admin.orderController.index', ['orders' => Order::with('history', 'user', 'state')
                                                                            ->Priority()
                                                                            ->paginate($limit)
                                                        ]);
        }
    }
    
    public function view($id){
        $order = Order::find($id);
        //dd($order->orderStateHistories()->get()->last()->comment);
        
        if($order){
            $state = OrderState::all()->lists('display_name', 'id');

            return view('admin.orderController.view', compact('order', 'state'));
        }
    }
    
    public function stateChange($id, Request $request){
        //dd($id);
        //dd($request->get('comment'));
        
        $order = Order::find($id);
        
        //Если состояние заказа не изменилось, то возвращаем на ту же страницу
        //Иначе начинаем обработку
        if ( $order->state()->value('id') == $request->get('status') ){
            return Redirect()->back();
        }
        else{
            $order->order_state_id = $request->get('status');
            $order->save();
            
            $order->orderStateHistories()->create([
                   'order_state_id' => $request->get('status'),
                   'comment'        => $request->get('comment'),
                   'changed_at'     => \Carbon\Carbon::now(),
                ]);
            
            $messages[] = "Заказ №$id обработан";   
                    
            return \Redirect::route('admin.order.index')->with(['messages' => $messages]);
        }
        
    }
}
