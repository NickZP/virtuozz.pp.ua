<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;
//use App\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Section;

class SectionController extends Controller
{
    private $nameClass = 'sectionController';

    public function index()
    {

        $messages[] = '';
        if (\Session::has('messages')){
            $messages[] = \Session::get('messages');
        }

        return view('admin.sectionController.index', ['sections' => Section::Priority()->get(), 'messages' => $messages]);
    }
    
    public function newSection(){
        
        return view('admin.sectionController.newSection');
    }
    
    public function postNewSection(\App\Http\Requests\SectionController\NewSectionRequest $request){
        
        $section = Section::create([
            'text'      => $request->get('name'),
            'route'     => $request->get('route'),
            'priority'  => $request->get('priority'),
            'select'    => $request->get('select') ? 1 : 0,
            'image'     => '',
        ]);

        $section->imageUpdate($request->file('image'));
        
        $messages = "раздел добавлен";
        
        return \Redirect::route('sections')->with(['messages' => $messages]);
    }
    
    public function postSections(Request $request){
        $method = $request->get('action');
        
        if (method_exists($this, $method)){
            $this->$method($request);
            return \Redirect::back();
        }
        else{
            $messages = "действие не было выбрано";
            return \Redirect::back()->withErrors([$messages]);
        }
    }

    public function update(Request $request)
    {
        foreach ($request->get('id') as $key => $id){
            $section = Section::find($id);
            $arraySection = [
                                'text'      => $request->get('text')[$key],
                                'route'     => $request->get('route')[$key],
                                'priority'  => $id,
                                'select'    => $request->get('select-'.$id) ? 1 : 0,
                            ];
                            
            //сохраняем изображение
            if(!is_null($request->file('image')[$key])){
                $section->imageUpdate($request->file('image')[$key]);
            }
            $section->fill($arraySection)->save();
        }
        
        $messages = "Данные обновлены";
        
        return \Redirect::back()->with(['messages' => $messages]);
    }
    
    public function delete(Request $request){
        
        //находим все чекбоксы (checked)
        $arrayKeys = array_keys($request->all());
        
        foreach($arrayKeys as $value){
            $pos = strpos($value, "chkbx-section");
            
            if ($pos !== false) {
                $keys = explode('-', $value);
                $id = array_pop($keys);
                
                Section::destroy($id);
            }
        }     
        
        $messages = "Раздел удалён";
        return \Redirect::back()->with(['messages' => $messages]);
    }
    
    public function cancelSection(){
        
        return \Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
