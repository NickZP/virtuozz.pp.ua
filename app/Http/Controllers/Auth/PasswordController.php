<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Traits\CaptchaTrait;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Mail\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Models\Section;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords, CaptchaTrait;
    
    private $sections;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        
        $this->sections = Section::with('categories')->Priority()->get();
    }
    
    public function showLinkRequestForm()
    {
        if (property_exists($this, 'linkRequestView')) {
            return view($this->linkRequestView);
        }

        if (view()->exists('auth.passwords.email')) {
            return view('auth.passwords.email', ['sections' => $this->sections]);
        }

        return view('auth.password');
    }
    
    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
                                    'email'                 => 'required|exists:users,email', 
                                    'g-recaptcha-response'  => 'required'
                                ]);

        if ( $this->captchaCheck($request) ){
            $broker = $this->getBroker();

            $response = Password::broker($broker)->sendResetLink($request->only('email'), function (Message $message) {
                $message->subject($this->getEmailSubject());
            });

            switch ($response) {
                case Password::RESET_LINK_SENT:
                    return $this->getSendResetLinkEmailSuccessResponse($response);

                case Password::INVALID_USER:
                default:
                    return $this->getSendResetLinkEmailFailureResponse($response);
            }
        }
        else{
            $messages[] = 'Письмо не было отправлено';
            return \Redirect::back()->with(['messages' => $messages]);
        }
    }
    
    protected function getSendResetLinkEmailSuccessResponse($response)
    {  
        return \Redirect::to('/password/reset')->with(['messages' => [trans($response)]]);
    }
    
    public function showResetForm(Request $request, $token = null)
    {
        if (is_null($token)) {
            return $this->getEmail();
        }

        $email = $request->input('email');

        if (property_exists($this, 'resetView')) {
            return view($this->resetView, ['sections' => $this->sections])->with(compact('token', 'email'));
        }

        if (view()->exists('auth.passwords.reset')) {
            return view('auth.passwords.reset', ['sections' => $this->sections])->with(compact('token', 'email'));
        }

        return view('auth.reset')->with(compact('token', 'email'));
    }
    
    protected function getResetSuccessResponse($response)
    {
        return \Redirect::to('/password/reset')->with('status', trans($response));
    }
}
