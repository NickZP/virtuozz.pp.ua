<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\Models\Product;
use \App\Models\ProductInfo;
use \App\Models\Section;
use \App\Models\Measurement;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        
        $limit = \App\Helpers\Helper::getLimit();
        
        $viewText = ['all' => 'Все'];
        
        $messages[] = '';
        if (\Session::has('messages')){
            $messages[] = \Session::get('messages');
        }
        
        $products = Product::paginate($limit);
        $sections = Section::with('categories')->Priority()->get();
        
        if( $request->ajax() ){
            $text = $request->get('search');
            if($text !== $viewText['all']){
                    $products = \App\Models\Category::where('text', '=', $text)
                                                        ->first()
                                                        ->products()
                                                        ->paginate($limit);
            }
            
            return \Response::json(view('admin.productController.ajax.productList', ['products' => $products])->render());            
        }
        
        return view('admin.productController.index')->with(['products'  => $products, 
                                                            'sections'  => $sections, 
                                                            'text'      => $viewText,
                                                            'messages'  => $messages
                                                        ]);
    }
    
    public function newProduct(){
        
        $sessionArr = [];
        
        if (!empty(\Session::getOldInput('measure')))
        {
            $sessionArr['num_measure']      = \Session::getOldInput('num_measure');
            $sessionArr['measure']          = \Session::getOldInput('measure');
            $sessionArr['price']            = \Session::getOldInput('price');
        }
        
        
        $measures = Measurement::lists('display_name', 'display_name');
        
        $sections = Section::with('categories')->Priority()->get();
        
        return view('admin.productController.newProduct')->with([
                                                                    'sections' => $sections, 
                                                                    'measures' => $measures, 
                                                                    'sessions' => $sessionArr
                                                                ]);
    }
    
    public function postNewProduct(Requests\ProductController\postNewProductRequest $request)
    {
        $product = new Product;
        $product->name = $request->get('name');
        $product->shortname = $request->get('shortname');
        $product->route = $request->get('route');
        $product->save();
        
        $product->productInfo()->create([
            'info'          => $request->get('body'),
            'desc_short'    => $request->get('desc-short'),
        ]);
        
        $product->categories()->attach($request->get('sections'));
        
        //сохраняем варианты цен
        $count = count($request->get('price'));
        for($i = 0; $i < $count; $i++){
            \App\Models\VariantSale::create([
                                                'product_id'    => $product->id,
                                                'measurement'   => trim($request->get('num_measure')[$i]) 
                                                                        . ' ' . $request->get('measure')[$i],
                                                'price'         => $request->get('price')[$i],
                                                'default'       => ($request->get('sale') == $i) ? 1 : 0,
                                            ]);
        }
        
        
        //сохраняем изображение
        $product->newImage($request->file('image'));
        
        $messages = "продукт добавлен";
        
        return \Redirect::route('products')->with(['messages' => $messages]);
        
    }
    
    public function edit($id)
    {
        $product = Product::find($id);
        $variantsales = \App\Models\VariantSale::where('product_id', '=', $id)->get();
        $sessionArr = [];
        $j = 0;
        
        if (!empty(\Session::getOldInput('measure')))
        {
            $sessionArr['num_measure']      = \Session::getOldInput('num_measure');
            $sessionArr['measure']          = \Session::getOldInput('measure');
            $sessionArr['price']            = \Session::getOldInput('price');
        }
        
        $measures = Measurement::lists('display_name', 'display_name');

        /*$measures = [ 
                        'гр' => 'гр', 
                        'кг' => 'кг', 
                        'мл' => 'мл', 
                        'л'  => 'л',
                    ];*/
        
        if ($product){
            $productCategories = $product->categories->lists('id')->toArray();
            //dd($productCategories );
            $sections = Section::with('categories')->get();
            return view('admin.productController.edit')->with([
                                                                'product'       => $product, 
                                                                'variantsales'  => $variantsales,
                                                                'sections'      => $sections,
                                                                'productCategories' => $productCategories,
                                                                'measures'      => $measures, 
                                                                'sessions'      => $sessionArr,
                                                                'j'             => $j,
                                                            ]);
        }
    }
    
    public function postEdit(Requests\ProductController\postEditProductRequest $request)
    {
        $product = Product::find($request->get('id'));
        //dd($product->categories()->lists('id', 'id')->toArray());
        if($product){
            $product->name = $request->get('name');
            $product->shortname = $request->get('shortname');
            $product->route = $request->get('route');
            
            $ids = \App\Models\VariantSale::where('product_id', '=', $request->get('id'))->lists('id', 'id')->toArray();
            $num = 0;
            //обновляем существующие варианты продаж
            if (!empty($request->get('dbnum_measure'))){
                foreach($request->get('dbnum_measure') as $key => $val){
                    $sale = \App\Models\VariantSale::find($key);
                    $sale->measurement = $request->get('dbnum_measure')[$key] 
                                            . ' ' . $request->get('dbmeasure')[$key];
                    $sale->price       = $request->get('dbprice')[$key];
                    $sale->default     = ($request->get('sale') == $num) ? 1 : 0;
                    $sale->save();
                    unset($ids[$key]);
                    $num++;
                }
            }
            if(count($ids)){
                \App\Models\VariantSale::destroy($ids);
            }
            //добавляем новые варианты продаж
            if (!empty($request->get('num_measure'))){
                $count = count($request->get('num_measure'));
                for($i = 0; $i < $count; $i++){
                    \App\Models\VariantSale::create([
                                                'product_id'    => $product->id,
                                                'measurement'   => trim($request->get('num_measure')[$i]) 
                                                                        . ' ' . $request->get('measure')[$i],
                                                'price'         => $request->get('price')[$i],
                                                'default'       => ($request->get('sale') == $num) ? 1 : 0,
                                            ]);
                    $num++;
                }
            }
            
            $image = $request->file('image');
            if(isset($image)){
                $product->imageUpdate($image);
            }
            
            $product->save();

            $product->productInfo()->update([
                                                'info' => $request->get('body'),
                                                'desc_short' => $request->get('desc-short'),
                                            ]);
            
            $product->categories()->detach($product->categories()->lists('id', 'id')->toArray());
            $product->categories()->attach($request->get('sections'));
            
            $messages = "Продукт обновлен";
            return \Redirect::route('products')->with(['messages' => $messages]);
        }
    }
    
    public function delete($id){
        Product::destroy($id);
        $product_info = ProductInfo::where('product_id', $id)->first();
        ProductInfo::destroy($product_info->id);

        $messages = "Продукт удалён";
        return \Redirect::route('products')->with(['messages' => $messages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
