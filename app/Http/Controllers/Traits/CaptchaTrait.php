<?php

namespace App\Http\Controllers\Traits;

use App\Helpers\Curl;
use Illuminate\Http\Request;

trait CaptchaTrait
{
    public function captchaCheck(Request $request){

        $curl = new Curl;

        $response = json_decode($curl->post('https://www.google.com/recaptcha/api/siteverify', [
            'secret'    => config('services.recaptcha.secret'),
            'response'  => $request->input('g-recaptcha-response'),
            'remoteip'  => $request->ip()
        ]));

        if( !$response->success ){
            return false;
        }

        return true;
    }
}