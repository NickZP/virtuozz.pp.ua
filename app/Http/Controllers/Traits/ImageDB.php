<?php

namespace App\Http\Controllers\Traits;

use Symfony\Component\HttpFoundation\File\UploadedFile;

trait ImageDB
{
    public function getImageName($path_image)
    {
        $image = explode('/', $path_image);

        return array_pop($image);
    }

    public function compareImage($image1, $image2)
    {
        return (strtolower($image1) === strtolower($image2)) ? true : false;
    }

    public function saveImage($dir, UploadedFile $image)
    {
        if(!\File::exists($dir)) {
            \File::makeDirectory($dir, 0775, true);
        }

        $image->move(realpath($dir), $image->getClientOriginalName());

        //удаляем "." в пути
        return substr($dir, 1) . '/' . $image->getClientOriginalName();
    }
}