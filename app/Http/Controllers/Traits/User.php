<?php

namespace App\Http\Controllers\Traits;

use App\Helpers\Curl;
use Illuminate\Http\Request;
use App\Models\User as UserModel;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Http\Requests;

trait User
{
    use \App\Http\Controllers\Traits\CaptchaTrait;

    public function login(Request $request, UserModel $user)
    {
        $data = $request->all();
        
        $user = $user->login($data);
        
        if(!$user){
            $messages[] = 'Ошибка авторизации';
            return \Redirect::back()->with(['messages' => $messages]);
        }    
        
        if($user->hasRole(['user'])){
            return \Redirect::route('index');
        }
        
        return \Redirect::route('sections');
        
    }
    
    public function logout()
    {
        \Auth::logout();
        return \Redirect::route('index');
    }
    
    public function register(Requests\Frontend\UserController\UserRegistrationRequest $request, UserModel $user)
    {

        if ( $this->captchaCheck($request) ){

            $data = $request->all();
            
            $user = $user->register($data);
            
            if($user instanceof Exception){
                return $user;
            }
            
            \Auth::login($user, true);
            
            return \Redirect::route('index');
        }
        else{
            $messages[] = 'Вы не зарегестрировались';
            return \Redirect::back()->with(['messages' => $messages]);
        }
    }

}
