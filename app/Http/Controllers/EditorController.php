<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserInfo;
use App\Models\Role;

use Illuminate\Http\Request;
use App\Http\Requests;

class EditorController extends Controller
{
    public function index( Request $request )
    {
        $messages[] = \Session::has('messages') ? \Session::get('messages') : '';
        
        $limit = \App\Helpers\Helper::getLimit();
        
        
        if( $request->ajax() ){
            $editors = User::whereHas('roles', function($q){
                    $q->where('name', '<>' ,'user');
                })
                ->where('name', 'LIKE', '%'.$request->get('search').'%')
                ->skip($request->get('page') * $limit)
                ->paginate($limit);
            return \Response::json(view('admin.editorController.ajax.editorList', ['editors' => $editors])->render()); 
        }
        else{
            $editors = User::whereHas('roles', function($q){
                $q->where('name', '<>', 'user');
            })->paginate($limit);
        }
        
        return view('admin.editorController.index', ['editors' => $editors, 'messages' => $messages]);
    }

    public function create()
    {
        $roles = Role::where('name', '<>', 'user')->lists('display_name', 'name');
        $role = Role::where('name', 'editor')->value('display_name');
        //$userRole = \Auth::user()->roles->lists('name', 'name')->first();
        
        return view('admin.editorController.create', ['roles' => $roles, 'role' => $role]);
    }

    public function store(Requests\EditorController\storeEditorRequest $request)
    {
        $role = Role::where('name', $request->get('roles'))->first();
        
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_type' => $role->id,
            'password'  => \Hash::make($request->get('password'))
        ]);
        
        $info = new UserInfo(['phone' => '']);
        $user->userInfo()->save($info);
        
        $user->attachRole($role);
        
        $messages = "Редактор добавлен";
        
        return \Redirect::route('admin.editors.index')->with(['messages' => $messages]);
    }

    public function edit($id)
    {
        $editor = User::find($id);
        
        if($editor){
            $role = $editor->roles->lists('name', 'name')->first();
        
            $roles = Role::where('name', '<>', 'user')->lists('display_name', 'name');

            return view('admin.editorController.edit', ['editor' => $editor->userInfo, 'roles' => $roles, 'role' => $role]);
        }
    }

    public function update(Requests\EditorController\updateEditorRequest $request, $id)
    {
        $editor = User::find($id);
        
        if($editor){
            $editor->fill($request->only('name', 'email', 'password'))->save();
            
            $roleUser = $editor->roles->lists('name', 'name')->first();
            $oldRole = Role::where('name', $roleUser)->first();
            
            $newRole = Role::where('name', $request->get('roles'))->first();
            
            $editor->detachRole($oldRole);
            $editor->attachRole($newRole);
            
            $messages = "Редактор обновлён";
            
            return \Redirect::route('admin.editors.index')->with(['messages' => $messages]);
        }
    }

    public function delete($id){
        $user = User::find($id);
        if($user){
            $user->delete();
            
            $messages = "Редактор удалён";
            
            return \Redirect::route('admin.editors.index')->with(['messages' => $messages]);
        }
    }
}
