<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Section;
use App\Models\Category;
use DateTime;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $limit = \App\Helpers\Helper::getLimit();
        
        $viewText = [  
                        'action'    => 'Действия',
                        'all'       => 'Все',
                    ];
        
        $messages[] = '';
        if (\Session::has('messages')){
            $messages[] = \Session::get('messages');
        }
        
        if( $request->ajax() ){
            $text = $request->get('search');
            if($text !== $viewText['all']){
                    $categories = Category::whereHas('section', function($query) use ($text){
                        $query->where('text', 'LIKE', $text);
                    })->paginate($limit);//get();
                }
            else{
                $categories = Category::paginate($limit);
            }
            
            return \Response::json(view('admin.categoryController.ajax.categoryList', ['categories' => $categories])->render());            
        }

        return view('admin.categoryController.index', [ 'categories'    => Category::paginate($limit), 
                                                        'sections'      => Section::Priority()->get(),
                                                        'text'          => $viewText,
                                                        'messages'      => $messages,
                                                        ]
                    );
    }
    
    public function newCategory()
    {
        $sections = Section::Priority()->lists('text', 'id');
        
        return view('admin.categoryController.newCategory')->with(['sections' => $sections]);
    }

    public function postNewCategory(Requests\CategoryController\postNewCategoryRequest $request)
    {
        $section = Section::find($request->get('Sections'));

        $category = Category::create([
            'text'          => $request->get('name'),
            'route'         => '/' . $section->route . '/' .$request->get('route'),
            'section_id'    => $request->get('Sections'),
            'description'   => $request->get('description'),
            'image'         => '',
            
        ]);
        
        if(!is_null($request->file('image'))){
            $category->imageUpdate($request->file('image'));
        }
        
        $messages = "категория добавлена";
        
        return \Redirect::route('categories')->with(['messages' => $messages]);
    }
    
    public function edit($id)
    {
        $category = Category::find($id);
        
        if ($category){
            $sections = Section::Priority()->lists('text', 'id');
            return View('admin.categoryController.edit')->with(['category' => $category, 'sections' => $sections]);
        }
    }
    
    public function postEdit(Requests\CategoryController\postEditCategoryRequest $request)
    {
        $category = Category::find($request->get('id'));
        if($category){
            $category->text         = $request->get('name');
            $category->route        = $request->get('route');
            $category->section_id   = $request->get('sections');
            $category->description  = $request->get('description');
            $category->save();
            
            //сохраняем изображение
            if(!is_null($request->file('image'))){
                $category->imageUpdate($request->file('image'));
            }
            
            $messages = "категория обновлена";
            return \Redirect::route('categories')->with(['messages' => $messages]);
        }
    }
    
    public function delete($id)
    {
        Category::destroy($id);
        
        $messages = "категория удалёна";
        return \Redirect::back()->with(['messages' => $messages]);
    }
}
