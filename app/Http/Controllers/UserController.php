<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserInfo;
use App\Models\Role;

class UserController extends Controller
{
    use \App\Http\Controllers\Traits\User;
    
    public function index(Request $request)
    {
        $messages[] = \Session::has('messages') ? \Session::get('messages') : '';
        
        $limit = \App\Helpers\Helper::getLimit();
        
        if( $request->ajax() ){
            $users = User::whereHas('roles', function($q){
                    $q->where('name', 'user');
                })
                ->where('name', 'LIKE', '%'.$request->get('search').'%')
                ->skip($request->get('page') * $limit)
                ->paginate($limit);
            
            return \Response::json(view('admin.userController.ajax.userList', ['users' => $users])->render()); 
        }
        else{
            $users = User::whereHas('roles', function($q){
                $q->where('name', 'user');
            })->paginate($limit);
        }
        
        return view('admin.userController.index', ['users' => $users, 'messages' => $messages]);
    }

    public function create()
    {
        return view('admin.userController.create');
    }

    public function store(Requests\UserController\storeUserRequest $request)
    {
        $role = Role::where('name', 'user')->first();
        
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_type' => $role->id,
            'password'  => \Hash::make($request->get('password'))
        ]);
        
        $info = new UserInfo(['phone' => $request->get('phone')]);
        $user->userInfo()->save($info);
        
        $user->attachRole($role);
        
        $messages = "Пользователь добавлен";
        
        return \Redirect::route('admin.users.index')->with(['messages' => $messages]);
    }

    public function showLogin()
    {
        //
        return view('user.login');
    }
    
    public function show($id){
        
        $user = User::find($id);
        
        if($user){
            return view('admin.userController.show', compact('user'));
        }
    }

    public function edit($id)
    {
        $user = User::find($id)->userInfo;
        
        if($user){
            return view('admin.userController.edit', ['user' => $user]);
        }
    }

    public function update(Requests\UserController\updateUserRequest $request, $id)
    {
        $user = User::find($id);
        
        if($user){
            $user->userInfo->fill($request->only('phone'))->save();
            $user->fill($request->only('name', 'email'));
            
            if ( ! empty($request->get('password')) && ! empty($request->get('password_confirmation'))){
                $user->password = bcrypt($request->get('password'));
            }
            
            $user->save();
            
            $messages = "Пользователь обновлён";
            
            return \Redirect::route('admin.users.index')->with(['messages' => $messages]);
        }
    }

    public function delete($id){
        $user = User::find($id);
        if($user){
            $user->delete();
            
            $messages = "Пользователь удалён";
            
            return \Redirect::route('admin.users.index')->with(['messages' => $messages]);
        }
    }
    
    public function search(){
        
    }
}
