<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\StaticPage;

class StaticPageController extends Controller
{
    public function index(){
        
        $messages[] = \Session::has('messages') ? \Session::get('messages') : '';
        
        return view('admin.staticPageController.index', [
                                            'staticPages' => StaticPage::all(), 
                                            'messages' => $messages
                                            ]
                   );
    }
    
    public function create(){
        return view('admin.staticPageController.create');  
    }
    
    public function store(Requests\StaticPageController\storeStaticPageRequest $request){
        StaticPage::create([
            'tag'       => $request->get('tag'),
            'title'     => $request->get('title'),
            'content'   => $request->get('body')
        ]);
        
        $messages = "Страница добавлена";
        
        return \Redirect::route('admin.static-pages.index')->with(['messages' => $messages]);
    }
    
    public function edit($id){
        $page = StaticPage::find($id);
        
        if($page){
            return view('admin.staticPageController.edit', ['page' => $page]);
        }
    }
    
    public function update(Requests\StaticPageController\updateStaticPageRequest $request, $id){
        $page = StaticPage::find($id);
        
        if($page){
            $page->tag      = $request->get('tag');
            $page->title    = $request->get('title');
            $page->content  = $request->get('body');
            $page->save();
            
            $messages = "Страница обновлена";
            
            return \Redirect::route('admin.static-pages.index')->with(['messages' => $messages]);
        }
    }
    
    public function delete($id){
        $page = StaticPage::find($id);
        if($page){
            $page->delete();
            
            $messages = "Страница удалена";
            
            return \Redirect::route('admin.static-pages.index')->with(['messages' => $messages]);
        }
    }
}
