<?php

namespace App\Http\Requests\SectionController;

use App\Http\Requests\Request;

class IndexSectionRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'      => 'required',
            'route'     => 'required|unique_tag:sections,route',
        ];
    }
}
