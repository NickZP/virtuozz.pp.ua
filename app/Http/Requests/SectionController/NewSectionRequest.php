<?php

namespace App\Http\Requests\SectionController;

use App\Http\Requests\Request;

class NewSectionRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'      => 'required',
            'route'     => 'required|unique_tag:sections,route',
            'priority'  => 'required',
            'image'     => 'required|mimes:jpg,jpeg,png|max:6000',
        ];
    }
}
