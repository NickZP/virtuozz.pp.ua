<?php

namespace App\Http\Requests\UserController;

use App\Http\Requests\Request;
use App\Models\User;

class updateUserRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'                  => 'required',
            'email'                 => 'required|email|unique:users,email,'.$this->route('users'),
            'phone'                 => 'required',
            'password'              => 'required_with:password_confirmation|min:6|confirmed',
            'password_confirmation' => 'min:6'
        ];
    }
}
