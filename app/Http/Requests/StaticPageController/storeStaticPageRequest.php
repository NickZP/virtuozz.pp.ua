<?php

namespace App\Http\Requests\StaticPageController;

use App\Http\Requests\Request;

class storeStaticPageRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'tag'   => 'required|unique:static_pages,tag',
            'title' => 'required',
            'body'  => 'required'
        ];
    }
}
