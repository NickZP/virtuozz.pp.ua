<?php

namespace App\Http\Requests\StaticPageController;

use App\Http\Requests\Request;

class updateStaticPageRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'tag'   => 'required|unique:static_pages,tag,'.$this->route('static_pages'),
            'title' => 'required',
            'body'  => 'required'
        ];
    }
}
