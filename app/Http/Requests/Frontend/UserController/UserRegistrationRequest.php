<?php

namespace App\Http\Requests\Frontend\UserController;

use App\Http\Requests\Request;

class UserRegistrationRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'                  => 'required',
            'email'                 => 'required|email|unique:users,email',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'g-recaptcha-response'  => 'required'
        ];
    }
}
