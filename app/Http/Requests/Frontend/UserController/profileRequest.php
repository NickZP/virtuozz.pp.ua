<?php

namespace App\Http\Requests\Frontend\UserController;

use App\Http\Requests\Request;

class profileRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email'                 => 'required|email|unique:users,email,'.\Auth::user()->id,
            'phone'                 => 'phone',
            'old_password'          => 'required_with:password,password_confirmation|old_password',
            'password'              => 'min:6|confirmed',
            'password_confirmation' => 'min:6'
        ];
    }
}
