<?php

namespace App\Http\Requests\Frontend\BasketController;

use App\Http\Requests\Request;

class checkoutRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email',
            'name'  => 'required',
            'phone' => 'required|phone',
        ];
    }
}
