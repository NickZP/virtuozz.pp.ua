<?php

namespace App\Http\Requests\ProductController;

use App\Http\Requests\Request;

class postNewProductRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        //dd($this->all());
        
        $rules = [
            "name"      => "required",
            "shortname" => "required",
            "route"     => "required",
            "sale"      => "required",
            "sections"  => "required",
            "body"      => "",
            "image"     => "required",
        ];
        if ($this->get('num_measure')){
            foreach($this->get('num_measure') as $key => $val)
            {
              $rules['num_measure.'.$key] = 'required';
            }
            foreach($this->get('measure') as $key => $val)
            {
              $rules['measure.'.$key] = 'required';
            }
            foreach($this->get('price') as $key => $val)
            {
              $rules['price.'.$key] = 'required';
            }
        }
        else{
            $rules['num_measure']   = 'required';
            $rules['measure']       = 'required';
            $rules['price']         = 'required';
        }
        
        return $rules;
    }
}
