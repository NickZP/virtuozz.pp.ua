<?php

namespace App\Http\Requests\ProductController;

use App\Http\Requests\Request;

class postEditProductRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            "name"      => "required",
            "shortname" => "required",
            "route"     => "required",
            "sale"      => "required",
            "sections"  => "required",
            "body"      => "",
        ];
        
        $allDelete = true;
                
        if ($this->get('num_measure')){
            foreach($this->get('num_measure') as $key => $val)
            {
              $rules['num_measure.'.$key] = 'required';
            }
            foreach($this->get('measure') as $key => $val)
            {
              $rules['measure.'.$key] = 'required';
            }
            foreach($this->get('price') as $key => $val)
            {
              $rules['price.'.$key] = 'required';
            }
            
            $allDelete = false;
        }
        
        if ($this->get('dbnum_measure')){
            foreach($this->get('dbnum_measure') as $key => $val)
            {
              $rules['dbnum_measure.'.$key] = 'required';
            }
            foreach($this->get('dbmeasure') as $key => $val)
            {
              $rules['dbmeasure.'.$key] = 'required';
            }
            foreach($this->get('dbprice') as $key => $val)
            {
              $rules['dbprice.'.$key] = 'required';
            }
            
            $allDelete = false;
        }
        
        if($allDelete){
            $rules['num_measure']   = 'required';
            $rules['measure']       = 'required';
            $rules['price']         = 'required';
        }
        
        return $rules;
    }
}
