<?php

namespace App\Http\Requests\EditorController;

use App\Http\Requests\Request;

class updateEditorRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'                  => 'required',
            'email'                 => 'required|email|unique:users,email,'.$this->route('editors'),
            'roles'                 => 'required',
            'password'              => 'required_with:password_confirmation|min:6|confirmed'
        ];
    }
}
