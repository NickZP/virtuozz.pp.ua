<?php

namespace App\Http\Requests\CategoryController;

use App\Http\Requests\Request;

class postEditCategoryRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'  => 'required',
            'route' => 'required',
        ];
    }
}
