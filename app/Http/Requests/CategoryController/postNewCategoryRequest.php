<?php

namespace App\Http\Requests\CategoryController;

use App\Http\Requests\Request;

class postNewCategoryRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'      => 'required',
            'route'     => 'required',
            'image'     => 'mimes:jpg,jpeg,png|max:6000',
        ];
    }
}
