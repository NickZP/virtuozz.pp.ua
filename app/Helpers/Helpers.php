<?php

namespace App\Helpers;

use SEO;

class Helper{
    static private $limit = 10;
    
    static private $limitProduct = 9;
    
    public static function isCurrent($url)
    {
        $currentUrl = \Request::path();
        
        if($currentUrl === '/'){
            return;
        }
        
        $pathArr = explode('/', $currentUrl);
        
        if(count($pathArr) > 2){
            $pathArr[1] = ($pathArr[1] === 'category') ? 'categories' : $pathArr[1];
            $currentUrl = $pathArr[0] . '/' . $pathArr[1];
        }
        
        $strFind = strpos($url, $currentUrl);
        if ($strFind !== false) {
            return 'active';
        } 
        
        return '';        
    }
    
    public static function isCollapse($url){
        $currentUrl = \Request::path();
        $pathArr = explode('/', $currentUrl);
        $strFind = strpos($url, $currentUrl);
        if ($strFind !== false) {
            return true;
        } 
    }
    
    public static function imgProduct(){
        
        return '/uploads/products/';
    }
    
    public static function imgSection(){
        
        return '/uploads/sections/';
    }
    
    public static function imgSectionDirID($id){
        return '.' . self::imgSection() . $id;
    }
    
    public static function imgCategory(){
        
        return '/uploads/categories/';
    }
    
    public static function imgSeederProduct(){
        
        return '/uploads/products/';
    }
    
    public static function getLimit(){
        return self::$limit;
    }
    
    public static function getLimitProduct(){
        return self::$limitProduct;
    }

    public static function getDomain(){
        return 'http://' . $_SERVER['SERVER_NAME'];
    }

    public static function getCommonSEOKeywords(){
        $arr = SEO::metatags()->getKeywords();
        return array_splice($arr, 0, 3);
    }

    public static function getSEODescription(){
        return ' - купить в интернет-магазине virtuoz.pp.ua. Запорожский склад Виртуоз';
    }
}