<?php

namespace App\Providers;

use Validator;
use DB;
use Illuminate\Support\ServiceProvider;

class CustomValidatorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        \Validator::extend('phone', function($attribute, $value, $parameters, $validator) {
            if(preg_match("/\(\d\d\d\) \d\d\d-\d\d-\d\d/", $value)){
                return true;
            }
            else{
                return false;
            }
        });
        
        \Validator::extend('old_password', function($attribute, $value, $parameters, $validator) {
            if (!\Hash::check($value, \Auth::user()->password)){
                return false;
            }
            else{
                return true;
            }
        });
        
        \Validator::extend('unique_tag', function($attribute, $value, $parameters, $validator) {
            if (empty(DB::table($parameters[0])->where($parameters[1], '/' . $value)->get())){
                return true;
            }
            else{
                return false;
            }
        });
    }

    public function register()
    {
        //
    }
}
