<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\Section;

class MenuVertServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.template.v-menu', function($view){
            $view->with('sections', Section::with('categories')->Priority()->get());
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
