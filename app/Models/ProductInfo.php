<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductInfo extends Model
{
    protected $fillable = ['product_id', 'info', 'desc_short'];
    
    public $timestamps = false;
    
    public function product(){
        return $this->belongsTo(Product::class);
    }
}
