<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderInfo extends Model
{
    protected $fillable = ["variant_sale_id", "amount"];
    
    public $timestamps = false;
    
    public function order(){
        return $this->belongsTo(Order::class);
    }
    
    public function variantsale(){
        return $this->belongsTo(VariantSale::class, 'variant_sale_id');
    }
}
