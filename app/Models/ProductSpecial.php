<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSpecial extends Model
{
    protected $fillable = ['name', 'display_name', 'description'];
    
    public $timestamps = false;
    
    public function products(){
        return $this->belongsToMany(Product::class);
    }
}
