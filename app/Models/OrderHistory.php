<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    protected $fillable = ["history", "email", "name", "phone", "address"];
    
    public $timestamps = false;
    
    public function order(){
        return $this->belongsTo(Order::class);
    }
    
    public function getHistoryAttribute($value){
        return json_decode($value);
    }
    
    public function getCurrencyPriceAttribute(){
        return $this->attributes['price'] . 'грн';
    }
}
