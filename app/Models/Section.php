<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = ['text', 'priority', 'select', 'image', 'route'];

    public function categories(){
        return $this->hasMany(Category::class);
    }
    
    public function updateAll($arr){
        foreach($arr as $key => $value){
            $arr_db = array();
            foreach($arr[$key] as $k => $v){
                $arr_db[$k] = $v;
            }
            $this->where('id', $key)
                    ->update($arr_db);
            unset($arr_db);
        }
    }
    
    public function getText(){
        return $this->select('text')->get();
    }
    
    public function scopePriority($query){
        return $query->orderBy('priority', 'asc');
    }
    
    public function getRouteAttribute($value){
        return substr($value, 1);
    }
    
    public function setRouteAttribute($value){
        $this->attributes['route'] = '/' . $value;
    }
    
    public function copyImageFromSection($file){
        $dir =  \App\Helpers\Helper::imgSectionDirID($this->attributes['id']);
        if(!\File::exists($dir)) {
            $res = \File::makeDirectory($dir, 0775, true);
        }
        
        $dest = $dir . "/" . $file;
        
        $this->attributes['image'] = \App\Helpers\Helper::imgSection() . $this->attributes['id'] . '/' . $file;
        parent::save();
        
        $fileCopy = public_path() . "/imgOldSite/sections/" . $file; 
        \File::copy($fileCopy, $dest);
    }
    
    public function imageUpdate($imageNew){
        $image = $this->attributes['image'];
        
        $dir = \App\Helpers\Helper::imgSectionDirID($this->attributes['id']);
        if(!\File::exists($dir)) {
            \File::makeDirectory($dir, 0775, true);
        }
        
        $this->attributes['image'] = \App\Helpers\Helper::imgSection() . $this->attributes['id'] . '/' . $imageNew->getClientOriginalName();
        $imageNew->move(realpath($dir), $imageNew->getClientOriginalName());
        
        parent::save();

        //при обновлении удалить старый рисунок
        if(\File::exists ( public_path() . $image )) {
            \File::delete( public_path() . $image );
        } 
    }
    
    public function delete(){
        
        if(\File::exists ( public_path() . $this->attributes['image'] )) {
            \File::delete( public_path() . $this->attributes['image'] );
            
            \File::deleteDirectory(\App\Helpers\Helper::imgSectionDirID($this->attributes['id'])); 
        } 
                
        parent::delete();
    }
}
