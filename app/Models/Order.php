<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public function history(){
        return $this->hasOne(OrderHistory::class);
    } 
    
    public function infos(){
        return $this->hasMany(OrderInfo::class);
    }
    
    public function orderStateHistories(){
        return $this->hasMany(OrderStateHistory::class);
    }  
    
    /*
     priority()
    */
    public function scopePriority($query){
        return $query->orderBy('order_state_id')->orderBy('created_at');
    }
    
    public function state(){
        return $this->belongsTo(OrderState::class, 'order_state_id');
    }
}
