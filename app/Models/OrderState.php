<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderState extends Model
{
    protected $fillable = ['name', 'display_name'];
    
    public function orderStateHistories(){
        return $this->hasMany(OrderStateHistory::class);
    }  
    
    public function orders(){
        return $this->hasMany(Order::class, 'order_state_id');
    }
}
