<?php

namespace App\Models;

use App\Http\Controllers\Traits\ImageDB;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use DateTime;

class Category extends Model
{
    use ImageDB;

    protected $fillable = ['section_id', 'text', 'image', 'route', 'description'];
    
    public function section(){
        return $this->belongsTo(Section::class);
    }
    
    public function products(){
        return $this->belongsToMany(Product::class);
    }
    
    /*public function getCategories(){
        //$this->section()->find()->;
        return $this->rightjoin('section','category.section_id', '=', 'section.id')
                    ->select(
                            'category.id', 
                            'category.section_id', 
                            'category.text', 
                            'section.text as section_text'
                            )
                    ->get();
    }*/
    
    /*public function getCategory($text){
        //$query = $this-find()->section()->where('text', 'LIKE', $text)->get();
        //dd($query);
        return $this->with(['section' => function($query) use ($text){
            $query->where('text', 'LIKE', $text);
        }]);
        /*
        return $this->rightjoin('section','category.section_id', '=', 'section.id')
                    ->where('section.text', 'LIKE', $text)
                    ->select(
                            'category.id', 
                            'category.section_id', 
                            'category.text', 
                            'section.text as section_text'
                            )
                    ->get();
         * 
    }*/
    
    public function add($data){
        $section_id = Section::where('text', 'LIKE', $data['section'])->select('id')->get(); 
        $this->insert(
                    array(
                        'section_id' => $section_id[0]['id'], 
                        'text' => $data['text'],
                        'created_at' => new DateTime(),
                        'updated_at' => new DateTime()
                    )
                );
    }
    
    public function rename($data){
        return $this->where('id', $data['id'])
                        ->update(array('text' => $data['text']));
    }
    
    public function move($data){
        $section_id = Section::where('text', 'LIKE', $data['section'])->select('id')->get(); 

        return $this->where('id', $data['id'])
                        ->update(array('section_id' => $section_id[0]['id']));
    }
    
    public function del($data)
    {
        $n = count($data);
        for($i = 0; $i < $n; $i++){
            $this->where('id', $data[$i])
                    ->delete();
        }
    }
    
    public function copyImageFromCategory($file, $name){
        $dir =  '.' . "/public" . \App\Helpers\Helper::imgCategory() . $this->attributes['id'];
        if(!\File::exists($dir)) {
            $res = \File::makeDirectory($dir, 0775, true);
        }
        
        $dest = $dir . "/" . $file;
        
        $this->attributes['image'] = \App\Helpers\Helper::imgCategory() . $this->attributes['id'] . '/' . $file;
        parent::save();
        
        $fileCopy = public_path() . "/imgOldSite/categories/" . $name . "/" . $file; 
        \File::copy($fileCopy, $dest);
    }
    
    
    public function imageUpdate(UploadedFile $imageNew){
        $image = $this->attributes['image'];

        //без "." путь не находит папку сервера
        $dir = '.' . \App\Helpers\Helper::imgCategory() . $this->attributes['id'];

        $this->attributes['image'] = $this->saveImage($dir, $imageNew);

        if ( parent::save() ){
            if(\File::exists ( public_path() . $image ) && ! $this->compareImage($this->getImageName($image), $imageNew->getClientOriginalName()) ) {
                \File::delete( public_path() . $image );
            }
        }
    }
    
    public function delete(){
        if(\File::exists ( public_path() . $this->attributes['image'] )) {
            \File::delete( public_path() . $this->attributes['image'] );
        } 
                
        parent::delete();
    }
}
