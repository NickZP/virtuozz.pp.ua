<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SEO extends Model
{
    protected $table = 'seo';

    public $timestamps = false;

    protected $fillable = ['keywords', 'description', 'og_description'];

    public function product(){
        return $this->hasOne(Product::class);
    }

    /*
    * удаляем проблеы перед сохранением в БД
    */
    public function setKeywordsAttribute($value){
        $arr = explode( ',', $value);
        foreach ($arr as $key => $val){
            $arr[$key] = trim($val);
        }

        $this->attributes['keywords'] = implode(',', $arr);
    }

    public function getCountKeywordsAttribute(){

        if ( !empty($this->attributes['keywords']) ){
            $arr = explode( ',', $this->attributes['keywords']);
            return count($arr);
        }
        
        return 0;  
    }

    public function getKeywordsArrAttribute(){
        if ( !empty($this->attributes['keywords']) ){
            return explode( ',', $this->attributes['keywords']);;
        }

        return [];
    }
}
