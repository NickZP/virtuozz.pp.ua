<?php

namespace App\Models;

//use App\Models\UserInfo;
//use App\Models\Role;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait;

    protected $fillable = [
        'name', 'email', 'password', 'user_type',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function userInfo(){
        return $this->hasOne(UserInfo::class);
    }  
    
    public function roles(){
        return $this->belongsToMany(Role::class);
    } 
    
    public function orders(){
        return $this->hasMany(Order::class)->priority();
    } 
    
    public function login($data)
    {
        if(\Auth::attempt([ 'email' => $data['name'], 'password' => $data['password']]))
        {
            return \Auth::user();
        }
        else{
           
            return false;
        }
    }
    
    public function register($data)
    {
        try{
            $role = Role::where('name', 'user')->first();
            
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => \Hash::make($data['password']),
                'user_type' => $role->id
            ]);
            
            $info = new UserInfo([
                 'name' => $data['name'],
                 'email' => $data['email']
                ]);
            
            $user->attachRole($role);
            $user->userInfo()->save($info);
        }
        catch(Exception $e){
            return $e;
        }
        
        return $user;
    }
}
