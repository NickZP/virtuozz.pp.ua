<?php

namespace App\Models;

use App\Http\Controllers\Traits\ImageDB;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use DB;
use App\Helpers\Helper;

class Product extends Model
{
    use ImageDB;

    protected $fillable = ['name', 'shortname', 'route', 'comment', 'product_sale', 'image'];
    
    public function productInfo(){
        return $this->hasOne(ProductInfo::class);
    } 
    
    public function productSpecials(){
        return $this->belongsToMany(ProductSpecial::class);
    } 
    
    public function categories(){
        return $this->belongsToMany(Category::class);
    }
    
    public function variantsales(){
        return $this->hasMany(VariantSale::class);
    }

    public function SEO(){
        return $this->belongsTo(SEO::class, 'seo_id');
    }
    
    public function copyImage($file, $path){
        $dir =  '.' . "/public" . \App\Helpers\Helper::imgSeederProduct() . $this->attributes['id'];
        if(!\File::exists($dir)) {
            $res = \File::makeDirectory($dir, 0775, true);
        }
        
        $fileCopy   = public_path() . "/imgOldSite/products/" . $path . '/' .$file;
        $fileNew    = $file;
        if(!\File::exists($fileCopy)){
            $fileNew    = "not_image.jpg";
            $fileCopy   = public_path() . "/imgOldSite/products/" . $fileNew;
        }
        
        $dest = $dir . "/" . $fileNew;
        
        $this->attributes['image'] = \App\Helpers\Helper::imgSeederProduct() . $this->attributes['id'] . '/' . $fileNew;
        parent::save();
        
        \File::copy($fileCopy, $dest);

    }

    public function newImage($image){
        $dir =  '.' . \App\Helpers\Helper::imgSeederProduct() . $this->attributes['id']; 
        if(!\File::exists($dir)) {
            $res = \File::makeDirectory($dir, 0775, true);
        }  

        $this->attributes['image'] = \App\Helpers\Helper::imgSeederProduct() . $this->attributes['id'] . '/' . $image->getClientOriginalName();
        parent::save();
        $image->move($dir, $image->getClientOriginalName());
    }
    
    public function imageUpdate(UploadedFile $imageNew){
        $image = $this->attributes['image'];

        $dir = '.' . \App\Helpers\Helper::imgProduct() . $this->attributes['id'];

        $this->attributes['image'] = $this->saveImage($dir, $imageNew);

        if ( parent::save() ){
            if(\File::exists ( public_path() . $image ) && ! $this->compareImage($this->getImageName($image), $imageNew->getClientOriginalName()) ) {
                \File::delete( public_path() . $image );
            }
        }
    }

    protected static function strJoinForSort(Builder $builder){
        return      $builder->leftJoin('category_product', 'categories.id', '=', 'category_product.category_id')
                    ->leftJoin('products', 'category_product.product_id', '=', 'products.id')
                    ->leftJoin('variant_sales', 'products.id', '=', 'variant_sales.product_id')
                    ->rightJoin('product_infos', 'variant_sales.product_id', '=', 'product_infos.product_id');
    }

    /*
     * формируем SELECT для запроса сортировки
     * если $price != max, то устанавливаем её в min (по умолчанию)
     */
    protected static function strSelectForSort($price){
        if ( isset($price) && $price === 'max'){
            $sql = 'max(variant_sales.price) as price,';
        }
        else{
            $sql = 'min(variant_sales.price) as price,';
        }

        $sql .= 'categories.text,
                 products.id,
                 products.name,
                 products.shortname, 
                 products.route, 
                 products.image, 
                 variant_sales.measurement,
                 product_infos.desc_short';

        return $sql;
    }

    protected static function strGroupByForSort(Builder $builder){
        return $builder->groupBy('products.id');
    }

    protected static function strOrderByForSort(Builder $builder, $price){
        if ( isset($price) && $price === 'max'){
            return $builder->orderBy('price', 'desc');
        }
        else{
            return $builder->orderBy('price');
        }
    }

    /*
     * Сортировка продуктов в категории
     * по:
     *    name (имени)
     *    price (цене)
     * \DB::table('categories')
            ->leftJoin('category_product', 'categories.id', '=', 'category_product.category_id')
            ->leftJoin('products', 'category_product.product_id', '=', 'products.id')
            ->leftJoin('variant_sales', 'products.id', '=', 'variant_sales.product_id')
            ->select(\DB::raw('min(variant_sales.price) as min_price, categories.text, products.shortname, variant_sales.measurement, variant_sales.price'))
            ->where('categories.route', "/" . $section . "/" . $category)
            ->groupBy('products.shortname')
            ->orderBy('min_price')
            ->orderBy('products.shortname')
            ->paginate(10);
     */
    public static function getSortPrice($category_route, $price = 'min', $count = 1){
        $limit = Helper::getLimitProduct();

        $db_builder = DB::table('categories');
        $db_builder = self::strJoinForSort($db_builder)
                            ->select(DB::raw(self::strSelectForSort($price)))
                            ->where('categories.route', $category_route);
        $db_builder = self::strGroupByForSort($db_builder);
        $db_builder = self::strOrderByForSort($db_builder, $price);
        $products   = $db_builder->orderBy('products.shortname')
                                ->skip($count * $limit)
                                ->paginate($limit);

        return $products;
    }

    public static function getSortName($category_route, $name = 'asc', $count = 1){
        $limit = Helper::getLimitProduct();

        $db_builder = DB::table('categories');
        $db_builder = self::strJoinForSort($db_builder)
            ->select(DB::raw(self::strSelectForSort('min')))
            ->where('categories.route', $category_route);
        $db_builder = self::strGroupByForSort($db_builder);
        if (isset($name) && $name === 'desc' ){
            $db_builder = $db_builder->orderBy('products.shortname', 'desc');
        }
        $db_builder = $db_builder->orderBy('products.shortname', 'asc');
        $db_builder = $db_builder->skip($count * $limit)
                                ->paginate($limit);

        return $db_builder;
    }
    
    public function delete(){
        $dir = \App\Helpers\Helper::imgProduct() . $this->attributes['id'];      
        \File::delete($dir. DIRECTORY_SEPARATOR . $this->attributes['image']);
        
        parent::delete();
    }
}
