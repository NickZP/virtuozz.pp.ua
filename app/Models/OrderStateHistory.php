<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStateHistory extends Model
{
    protected $fillable = ['order_id', 'order_state_id', 'comment', 'changed_at'];
    
    protected $dates = ['changed_at'];
    
    public function order(){
        $this->belongsTo(Order::class);
    }
    
    public function state(){
        $this->belongsTo(OrderState::class);
    }
}
