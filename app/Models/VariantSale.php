<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariantSale extends Model
{
    protected $fillable = ['product_id', 'measurement', 'price', 'default'];
    
    public function product(){
        return $this->belongsTo(Product::class);
    }
    
    public function orderInfo(){
        return $this->hasOne(OrderInfo::class);
    }
    
    public function getCurrencyPriceAttribute(){
        return $this->attributes['price'] . 'грн';
    }

    public function scopePrice($query, $product_id, $price){
        return $query->where('product_id', $product_id)
                    ->where('price', $price);
    }
}
