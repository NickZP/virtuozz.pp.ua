@extends('frontend.app')
@section('content')
    <h2>Регистрация</h2>
    <section>
        @if (Auth::check())
            Вы {{ Auth::user()->name }}
            {!! link_to_route('logout', 'Выйти') !!}
        @else
            {!! Form::open(['route' => 'register', 'class' => 'form-horizontal']) !!}
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Имя</label>
                    <div class="col-sm-4">
                        {!! Form::text('name', null, ['type' => 'name', 'class' => 'form-control',
                            'id' => 'inputName', 'placeholder' => 'введите имя']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-4">
                        {!! Form::text('email', null, ['type' => 'name', 'class' => 'form-control',
                            'id' => 'inputEmail', 'placeholder' => 'введите email']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="col-sm-2 control-label">Пароль</label>
                    <div class="col-sm-4">
                        {!! Form::password('password', ['type' => 'password', 'class' => 'form-control',
                            'id' => 'inputPassword', 'placeholder' => 'введите пароль']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputRepeatPassword" class="col-sm-2 control-label">Пароль</label>
                    <div class="col-sm-4">
                        {!! Form::password('repeat-password', ['type' => 'password', 'class' => 'form-control',
                            'id' => 'inputRepeatPassword', 'placeholder' => 'повторите пароль']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-4">
                        {!! Form::submit('Зарегистрироваться', ['class' => 'btn btn-site']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        @endif
    </section>
@stop