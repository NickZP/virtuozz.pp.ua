@extends('frontend.app')
@section('content')
    @if (Auth::check())
        Вы {{ Auth::user()->name }}
        {!! link_to_route('logout', 'Выйти') !!}
    @else
    <h2 class="text-center">Авторизация</h2>
    {!! Form::open(['route' => 'login', 'class' => 'form-horizontal']) !!}
        <div class="form-group">
            <label for="inputName" class="col-sm-2 control-label">E-mail</label>
            <div class="col-sm-4">
                {!! Form::text('name', null, ['type' => 'name', 'class' => 'form-control',
                    'id' => 'inputName', 'placeholder' => 'введите имя']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">Пароль</label>
            <div class="col-sm-4">
                {!! Form::password('password', ['type' => 'password', 'class' => 'form-control',
                    'id' => 'inputPassword', 'placeholder' => 'пароль']) !!}
            </div>
        </div>
        <div class="form-group">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
                {!! Form::submit('Войти', ['class' => 'btn btn-site']) !!}
            </div>
        </div>
        
    {!! Form::close() !!}
    @endif
@stop