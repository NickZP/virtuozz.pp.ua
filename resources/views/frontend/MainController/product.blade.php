@extends('frontend.app')
@section('content')
    <div class="row">
        <div class="col-md-4 thumbnail">
            <img src="/uploads/products/{!!$product->id!!}/{!! $product->image !!}" alt="{!! $product->shortname !!}" width="400">
        </div>
        <div class="col-md-8">
            <h3>{!! $product->name !!}</h3>
            <div class="sale">
                <h4>Доступные варианты</h4>
                @foreach($product->variantsales as $sale)
                    <div class="row">
                        <div class="col-xs-2">{!! $product->shortname !!}</div>
                        <div class="col-xs-2">{!! $sale->measurement !!}</div>
                        <div class="col-xs-2">{!! $sale->price !!}</div>
                        <div class="col-xs-2">
                            <input type="text" class="form-control" value="1">
                        </div>
                        <div class="col-xs-2">
                          <button class="btn btn-success" type="button">В корзину</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-12">
            <h4>Описание</h4>
            {!! $product->body !!}
        </div>
    </div>
@stop
