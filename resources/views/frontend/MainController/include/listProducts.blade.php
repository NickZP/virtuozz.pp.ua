@if ($view === 'th')
    @foreach($products as $product)
        <div class="col-lg-4">
            <div class="product-buy">
                <a class="image image-text" href="{!! $category_url.$product->route !!}">
                    {!! HTML::image($product->image, $product->name, ["class" => "img-responsive", "alt" => $product->shortname]) !!}
                    <div class="text">
                        {{ $product->shortname }}
                    </div>
                </a>
                <div class="sale">
                    <span>от</span>
                    <span class="large">
                        {{ $product->price }}
                    </span>
                    <span>за</span>
                    <span class="large">
                        {{ App\Models\VariantSale::price($product->id, $product->price)->value('measurement') }}
                    </span>
                </div>
                <div>
                    <button
                        class="btn btn-buy btn-site btn-block"
                        type="button"
                        data-id="{{ $product->id }}"
                        data-url="{{ $product->route }}"
                        data-toggle="modal"
                        data-target="#modal-buy-product"
                    >
                        Купить
                    </button>
                </div>
            </div>
        </div>
    @endforeach
@else
    <div class="product-view-list">
    @foreach($products as $product)
        <div class="row item product-buy">
            <div class="col-lg-2 product-img">
                <a class="image image-product-small" href="{!! $category_url.$product->route !!}">
                    {!! HTML::image($product->image, $product->name, ["class" => "img-responsive", "alt" => $product->shortname]) !!}
                </a>
            </div>
            <div class="col-lg-7">
                <div class="product-name text">
                    {{ $product->shortname }}
                </div>
                <div class="product-description">
                    @if( !empty($product->desc_short) )
                        {!! $product->desc_short !!}
                    @endif
                    <span class="product_more">
                        <a class="underline" href="{!! $category_url.$product->route !!}">
                            Подробнее
                        </a>
                    </span>
                    ...
                </div>
            </div>
            <div class="col-lg-3 product-price">
                <div class="sale">
                    <span>от</span>
                    <span class="large">
                        {{ $product->price }}
                    </span>
                    <span>за</span>
                    <span class="large">
                        {{ App\Models\VariantSale::price($product->id, $product->price)->value('measurement') }}
                    </span>
                </div>
                <div>
                    <button
                            class="btn btn-buy btn-site btn-block"
                            type="button"
                            data-id="{{ $product->id }}"
                            data-url="{{ $product->route }}"
                            data-toggle="modal"
                            data-target="#modal-buy-product"
                    >
                        Купить
                    </button>
                </div>
            </div>
        </div>
    @endforeach
    </div>
@endif

@if($products->render())
    <div class="col-lg-12">
        <div class="pagination-block {{\Request::ajax()?'ajax':''}}" >
            <div class="text-center">{!! $products->render() !!}</div>
        </div>
    </div>
@endif