@extends('frontend.app')

@section('title', $category->text)

@section('content')

    {!! Breadcrumbs::render('category', $category) !!}
    
    @if($products->isEmpty())
        <h3>В данной категории продукты отсутствуют</h3>
    @else

        <p>
            <a 
                href="#" 
                class="underline"
                data-toggle="modal" 
                data-target="#modal-armatura-price"
            >
                Прайс арматура полимерная композитная.
            </a>
        </p>
        <p>
            <a 
                href="#" 
                class="underline"
                data-toggle="modal" 
                data-target="#modal-armatura-compare-table"
            >
                Сравнительные характеристики стальной и композитной стеклопластиковой арматуры.
            </a>
        </p>
        <p>
            <a 
                href="#" 
                class="underline"
                data-toggle="modal" 
                data-target="#modal-armatura-replacement-table"
            >
                Таблица равно-прочной замены металлической арматуры на стеклопластиковую.
            </a>
        </p>
        <p>
            <a 
                href="#" 
                class="underline"
                data-toggle="modal" 
                data-target="#modal-armatura-certificate"
            >
                Сертификат арматуры.
            </a>
        </p>

        <div class="row paginate-post content-list" data-url="{{ $category->route }}">
            @include('frontend.MainController.include.listProducts')
        </div>

    @endif

    @include('frontend.template.modal.get-category-armatura.price')
    @include('frontend.template.modal.get-category-armatura.compare-table')
    @include('frontend.template.modal.get-category-armatura.replacement-table')
    @include('frontend.template.modal.get-category-armatura.certificate')
    @include('frontend.template.modal.getPage-category.buy')
    
@endsection
