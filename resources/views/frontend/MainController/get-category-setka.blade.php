@extends('frontend.app')

@section('title', $category->text)

@section('css')
    <link rel="stylesheet" href="{{ asset('css/colorbox/colorbox.css') }}">
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/jquery.colorbox-min.js') }}"></script>
    <script>
        $('.gallery').colorbox({rel:'gallery', width: '700px'});
    </script>
@stop

@section('content')

    {!! Breadcrumbs::render('category', $category) !!}

    <p>
        <a
                href="#"
                class="underline"
                data-toggle="modal"
                data-target="#modal-setka-price"
        >
            Прайс композитная полимерная кладочная сетка.
        </a>
    </p>
    <div>
        <p>Кладочная полимерная сетка реализуется нарезками в картах размером 1х2м.</p>
        <p>Кладочная сетка с песком  реализуется в рулонах, шириной 1.2м и длиной 50 пог.м.</p>
    </div>

    <div class="row">
        @foreach( $pictures as $key => $picture)
            @if ( !in_array( $picture, array( '.', '..' ) ) )
                <div class="col-sm-4">
                    <a class="gallery image-text" href="{!! '../' . $path_picture . '/' . $picture !!}">
                    {!! HTML::image(
                                        $path_picture . '/' . $picture,
                                        $picture,
                                        [ "class" => "img-responsive", "alt" => $picture ]
                                   )
                    !!}
                        <div class="text">
                            Композитная полимерная кладочная сетка
                        </div>
                    </a>
                </div>
            @endif
        @endforeach
    </div>

    <div class="row paginate-post content-list" data-url="{{ $category->route }}">
        @include('frontend.MainController.include.listProducts')
    </div>
    @include('frontend.template.modal.get-category-setka.price')
    @include('frontend.template.modal.getPage-category.buy')

@endsection
