@extends('frontend.app')

@section('title', $section->text)

@section('content')

    {!! Breadcrumbs::render('section', $section) !!}
    
    <div class="row">
        @foreach($section->categories as $category)
            <div class="col-lg-4">
                <a class="image-text" href="{!! \App\Helpers\Helper::getDomain() . $category->route !!}">
                    {!! HTML::image($category->image, $category->text, ["class " => "img-responsive"]) !!}
                    <div class="text">
                        {!! $category->text !!}
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endsection
