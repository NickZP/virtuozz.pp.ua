@extends('frontend.app')

@section('title', $page->title)

@section('content')

    {!! Breadcrumbs::render('page', $page) !!}
    
    <h2>{!! $page->title !!}</h2>
    <div>{!! $page->content !!}</div>
@endsection

