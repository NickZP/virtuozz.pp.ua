@extends('frontend.app')

@section('title', ' - Контакты')

@section('content')

    {!! Breadcrumbs::render('contact') !!}

    @if ($errors->any())
        @include('admin.particial.errors')
    @endif

    @if (Session::has('messages'))
        @include('admin.particial.session')
    @endif

    <h3 class="text-center">Форма обратной связы:</h3>

    {!! Form::open(['route' => 'postcontact', 'class' => 'form-horizontal contact']) !!}
    <div class="form-group">
        <label for="inputName" class="col-sm-3 control-label">Имя:</label>
        <div class="col-sm-7">
            {!! Form::text('name', null, ['type' => 'name', 'class' => 'form-control',
                'id' => 'inputName', 'placeholder' => 'Введите Имя']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail" class="col-sm-3 control-label">E-mail:</label>
        <div class="col-sm-7">
            {!! Form::text('email', null, ['type' => 'email', 'class' => 'form-control',
                'id' => 'inputEmail', 'placeholder' => 'Введите Email']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="message" class="col-sm-3 control-label">Сообщение:</label>
        <div class="col-sm-7">
            {!! Form::textarea('message', null, ['id' => 'message', 'rows' => 5, 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-5 col-sm-4">
            {!! Form::submit('Отправить', ['class' => 'btn btn-site']) !!}
        </div>
    </div>

    {!! Form::close() !!}

@endsection

