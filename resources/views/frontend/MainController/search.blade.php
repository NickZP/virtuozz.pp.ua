@extends('frontend.app')

@section('title', 'Поиск')

@section('scripts')
    @parent
    <script>
        if($('input[name="search"]').hasClass('search-ajax')){
            $('.search-ajax').val("{!! $search !!}");
        }
    </script>
@stop

@section('content')

    <h2 class="text-center">Результаты поиска</h2>
    <div class="row" id="contentList" data-url="{!! \Request::segment(1) !!}">
        @include('frontend.MainController.ajax.content')
    </div>
    
@endsection
