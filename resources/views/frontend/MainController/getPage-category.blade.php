@extends('frontend.app')

@section('title', $category->text)

@section('css')
    <link rel="stylesheet" href="{{ asset('css/zoomove.min.css') }}">
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/zoomove.min.js') }}"></script>
    <script>
        $('.zoo-item').ZooMove();
    </script>
@stop

@section('content')

    {!! Breadcrumbs::render('category', $category) !!}

    <div class="category-description">
        <div class="name">
            {{ $category->text }}
        </div>
        <div class="row">
            <div class="col-lg-3">
                <figure
                        class="image img-responsive img-block zoo-item"
                        data-zoo-image="{{ $category->image }}"
                        data-zoo-scale="1.7"
                        data-zoo-cursor="true"
                >
                    Загрузка ...
                </figure>
            </div>
            <div class="col-lg-9 text">
                <div class="bold">Cвойства категории:</div>
                <div>
                    {{ $category->description }}
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="sorter">
        <form action="">
            <div class="sort">
                <label>Сортировать:</label>
                {!! Form::select('sort', $sort_arr, $sort_arr['name'], ['class' => '']) !!}
                <span class="glyphicon glyphicon-arrow-up sort-by" aria-hidden="true" data-orderby="asc"></span>
            </div>

            <div class="view">
                <label>Отображать:</label>
                <span class="glyphicon glyphicon-th active" aria-hidden="true" data-view="th"></span>
                <span class="glyphicon glyphicon-list" aria-hidden="true" data-view="list"></span>
            </div>
        </form>
    </div>
    
    @if($products->isEmpty())
        <h3>В данной категории продукты отсутствуют</h3>
    @else

        <div class="row paginate-post content-list" data-url="{{ $category->route }}">
            @include('frontend.MainController.include.listProducts')
        </div>

    @endif
    
    @include('frontend.template.modal.getPage-category.buy')
    
@endsection
