@extends('frontend.app')

@section('title', $product->shortname)


@section('css')
    <link rel="stylesheet" href="{{ asset('css/zoomove.min.css') }}">
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/zoomove.min.js') }}"></script> 
    <script>
        $('.zoo-item').ZooMove();
    </script> 
@stop

@section('content')

    {!! Breadcrumbs::render('product', $product, $category) !!}

    <div class="row">
        <div class="col-lg-4">
            <!--{!! HTML::image($product->image, $product->shortname, ["class " => "img-responsive img-block img-product"]) !!}-->
            <figure 
                class="img-responsive img-block zoo-item" 
                data-zoo-image="{{ $product->image }}" 
                data-zoo-scale="1.7"
                data-zoo-cursor="true"
            >
                Загрузка ...
            </figure>
        </div>
        <div class="col-lg-8">
            <div class="row padding-right">
                <h4 class="line-bottom product-name" data-url="{!! \Request::url() !!}">{!! $product->name !!}</h4>
                <p>Доступные варианты</p>
                
                    @include('frontend.MainController.ajax.variant-sale')
                    
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <h4 class="line-bottom">Описание</h4>
            <div>
                {!! $product->productInfo->info !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('frontend.template.disqus')
        </div>
    </div>
@endsection
