@foreach( $product->variantsales as $sale)
    <div class="row sale-line">
        <div class="col-lg-3">
            <span class="measurement">{!! $sale->measurement !!}</span>
        </div>
        <div class="col-lg-3">
            <span><span class="price">{!! $sale->price !!}</span> грн</span>
        </div>
        <div class="col-lg-3 amount">
            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
            <input type="text" class="form-control number" name="col" value="1">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        </div>
        <div class="col-lg-3">
            <button class="btn btn-buy btn-site btn-block btn-small" type="button" data-id="{!! $product->id !!}">Купить</button>
        </div>
    </div>
@endforeach