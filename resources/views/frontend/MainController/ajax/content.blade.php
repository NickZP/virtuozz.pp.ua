<table width="100%" class="table table-responsive table-striped search">
    <thead class="text-center">
        <tr>
            <th>№</th>
            <th></th>
            <th>Название продукта</th>
        </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
                <tr>
                    <td align="center">{!! $count++ !!}.</td>
                    <td>{!! HTML::image($product->image, $product->shortname, ["class " => "img-responsive"]) !!}</td>
                    <td><a href="{!! $product->categories->first()->route .'/'. $product->route !!}">{!! $product->name !!}</a></td>
                </tr>
        @endforeach
    </tbody>
</table>
<div class="pagination-block">
    <div class="text-center">{!! $products->render() !!}</div>
</div>