@extends('frontend.app')

@section('title', 'главная')

@section('content')
    @foreach($sections as $section)
        <div class="col-lg-4">
            <a class="image-text" href="{!! \App\Helpers\Helper::getDomain() . '/' . $section->route !!}">
                {!! HTML::image($section->image, $section->text, ["class " => "img-responsive", "width" => "200"]) !!}
                <div class="text">
                    {!! $section->text !!}
                </div>
            </a>
        </div>
    @endforeach
@endsection

