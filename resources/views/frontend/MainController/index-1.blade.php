@extends('frontend.app')

@section('navbar')
    @include('frontend.template.navbar')
@endsection

@section('v-menu')
    @include('frontend.template.v-menu')
@endsection

@section('content')
        <div class="row">
            <ul>
                @foreach($products as $product)
                    <li class="col-md-3 col-xs-6 thumbnail">
                        <a href="#">
                            <img src="/uploads/products/{!!$product->id!!}/{!! $product->image !!}" alt="" width="150">
                        </a>
                        <div class="caption">
                            <h4>{!! link_to_route('product', $product->name, ['id' => $product->id]) !!}</h4>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-primary selection" type="button">
                                {!! $product->variantsales->first()->price !!} - 
                                {!! $product->variantsales->first()->measurement !!}
                            </button>
                            <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                                @foreach( $product->variantsales as $sale)
                                    <li><a href="#">{!! $sale->price !!} - {!! $sale->measurement !!}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div>
                          <button class="btn btn-success" type="button">В корзину</button>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="text-center">{!! $products->render() !!}</div>
@endsection

