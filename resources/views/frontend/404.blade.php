@extends('frontend.app')

@section('title', 'страница не найдена')

@section('content')
    <h3 class="text-center">Извините, такой страницы не существует</h3>
    
    @if(isset($message))
        <h4> {{ $message }}</h4>
    @endif
@endsection