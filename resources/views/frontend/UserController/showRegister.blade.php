@extends('frontend.app')

@section('title', ' - Регистрация')

@section('scripts-head')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@stop

@section('content')

    @if(Session::has('messages'))
        @include('admin.particial.error')
    @endif

    @if ($errors->any())
        @include('admin.particial.errors')
    @endif
    
    <h3 class="text-center">Регистрация:</h3>
    
    {!! Form::open(['route' => 'user.register', 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Имя:</label>
                <div class="col-sm-7">
                    {!! Form::text('name', null, [
                                        'type' => 'name', 
                                        'class' => 'form-control', 
                                        'id'    => 'name',
                                        'placeholder' => 'Введите имя'
                                    ]) 
                    !!}
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">E-Mail:</label>
                <div class="col-sm-7">
                    {!! Form::text('email', null, [
                                        'type' => 'email', 
                                        'class' => 'form-control',
                                        'id'    => 'email',
                                        'placeholder' => 'Введите email'
                                    ]) 
                    !!}
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Пароль (мин. 6 символов):</label>
                <div class="col-sm-7">
                    {!! Form::password('password', [
                                        'class' => 'form-control',
                                        'id'    => 'password',
                                        'placeholder' => 'Введите пароль'
                                    ]) 
                    !!}
                </div>
            </div>
            <div class="form-group">
                <label for="password_confirmation" class="col-sm-3 control-label">Подтверждение пароля:</label>
                <div class="col-sm-7">
                    {!! Form::password('password_confirmation', [
                                        'class' => 'form-control',
                                        'id' => 'password_confirmation', 
                                        'placeholder' => 'Повторите пароль'
                                    ]) 
                    !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 g-recaptcha" data-sitekey="6Lf1GSQTAAAAAKPo9MgQ-w2hgkPMSBCSDlxgN06x"></div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-5 col-sm-4">
                    {!! Form::submit('Регистрация', ['class' => 'btn btn-site']) !!}
                    <a href="{!! route('user.login') !!}">Вход</a>
                </div>
            </div>
        
    {!! Form::close() !!}
    
@endsection

