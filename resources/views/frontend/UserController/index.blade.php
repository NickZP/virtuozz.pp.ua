@extends('frontend.app')

@section('title', ' - Профиль')

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/jquery.mask.min.js') }}"></script> 
    <script>
        $('.phone').mask('(000) 000-00-00', {placeholder: "(___) ___-__-__"});
    </script> 
@stop

@section('content')

    @if (Session::has('messages'))
        @include('admin.particial.session')
    @endif
    
    @if ($errors->any())
        @include('admin.particial.errors')
    @endif
    
    <h3 class="text-center">Профиль:</h3>
    
    <a href="{!! route('frontend.order.index') !!}" class="btn btn-site">Мои заказы</a>
    
    {!! Form::model(\Auth::user()->userInfo, ['route' => 'frontend.user.profile', 'class' => 'form-horizontal']) !!}
        <div class="form-group">
            <label for="Email" class="col-sm-3 control-label">E-mail:</label>
            <div class="col-sm-7">
                {!! Form::text('email', null, ['type' => 'email', 'class' => 'form-control',
                    'id' => 'Email', 'placeholder' => 'Ваше имя']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="Phone" class="col-sm-3 control-label">Телефон:</label>
            <div class="col-sm-7">
                {!! Form::text('phone', null, ['type' => 'email', 'class' => 'form-control phone',
                    'id' => 'Phone', 'placeholder' => 'Ваш телефон']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="old_password" class="col-sm-3 control-label">Пароль:</label>
            <div class="col-sm-7">
                {!! Form::input('password', 'old_password', '', ['class' => 'form-control',
                    'id' => 'old_password', 'placeholder' => 'Пароль',]) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Новый пароль:</label>
            <div class="col-sm-7">
                {!! Form::password('password', ['type' => 'password', 'class' => 'form-control',
                    'id' => 'password', 'placeholder' => 'Новый пароль']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="password_confirmation" class="col-sm-3 control-label">Повторите новый пароль:</label>
            <div class="col-sm-7">
                {!! Form::password('password_confirmation', ['type' => 'password', 'class' => 'form-control',
                    'id' => 'password_confirmation', 'placeholder' => 'Повторите новый пароль']) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-5 col-sm-4">
                {!! Form::submit('Сохранить', ['class' => 'btn btn-site']) !!}
            </div>
        </div>
        
    {!! Form::close() !!}
    
@endsection

