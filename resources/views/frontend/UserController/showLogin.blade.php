@extends('frontend.app')

@section('title', ' - Вход')

@section('content')

    @if(Session::has('messages'))
        @include('admin.particial.error')
    @endif
    
    <h3 class="text-center">Пожалуйста, авторизуйтесь:</h3>
    
    {!! Form::open(['route' => 'user.login', 'class' => 'form-horizontal']) !!}
        <div class="form-group">
            <label for="inputName" class="col-sm-3 control-label">E-mail:</label>
            <div class="col-sm-7">
                {!! Form::text('name', null, ['type' => 'name', 'class' => 'form-control',
                    'id' => 'inputName', 'placeholder' => 'Введите имя']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Пароль:</label>
            <div class="col-sm-7">
                {!! Form::password('password', ['type' => 'password', 'class' => 'form-control',
                    'id' => 'inputPassword', 'placeholder' => 'Пароль']) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-5 col-sm-4">
                {!! Form::submit('Войти', ['class' => 'btn btn-site']) !!}
                <div class="text-block">
                    <a href="{!! url('/password/reset') !!}">Забыли свой пароль?</a><br/>
                    <a href="{!! route('user.register') !!}">Зарегистрироваться</a>
                </div>
            </div>
        </div>
        
    {!! Form::close() !!}
    
@endsection

