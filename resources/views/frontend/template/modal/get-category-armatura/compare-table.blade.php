<div id="modal-armatura-compare-table" class="modal modal-table fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal"></button>
                <h4 class="modal-title text-center text-uppercase">
                    Сравнительные характеристики стальной и композитной стеклопластиковой арматуры
                </h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Характеристики</th>
                                <th colspan="2">
                                    <div>Арматура</div>
                                    <div class="th-caption">
                                        <div class="left">Металлическая класса А-III (А400 - 25Г2С)</div>
                                        <div class="right">Композитная полимерная стеклопластиковая (АКС)</div>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Материал</td>
                                <td>Сталь</td>
                                <td>Стеклоровинг, связанный полимером на основе эпоксидной смолы</td>
                            </tr>
                            <tr>
                                <td>Деформативность</td>
                                <td>Упруго-пластические</td>
                                <td>Идеально - упругий</td>
                            </tr>
                            <tr>
                                <td>Предел прочности при растяжении, МПа</td>
                                <td>390</td>
                                <td>1300</td>
                            </tr>
                            <tr>
                                <td>Модуль упругости, МПа</td>
                                <td>210 000</td>
                                <td>55 000</td>
                            </tr>
                            <tr>
                                <td>Относительное удлинение, %</td>
                                <td>25</td>
                                <td>2.2</td>
                            </tr>
                            <tr>
                                <td>Коэффициент теплопроводности, Вт/(мּ0С)</td>
                                <td>46</td>
                                <td>0,35</td>
                            </tr>
                            <tr>
                                <td>Коэффициент линейного расширения, αх10-5/0С</td>
                                <td>13-15</td>
                                <td>9-12</td>
                            </tr>
                            <tr>
                                <td>Плотность, кг/м³</td>
                                <td>7850</td>
                                <td>1900</td>
                            </tr>
                            <tr>
                                <td>Коррозионная стойкость к агрессивным средам</td>
                                <td>Коррозирует</td>
                                <td>Нержавеющий материал</td>
                            </tr>
                            <tr>
                                <td>Теплопроводность</td>
                                <td>Теплопроводна</td>
                                <td>Нетеплопроводная</td>
                            </tr>
                            <tr>
                                <td>Электропроводность</td>
                                <td>Электропроводна</td>
                                <td>Неэлектропроводная - является диэлектриком</td>
                            </tr>
                            <tr>
                                <td>Выпускаемые профили</td>
                                <td>6 - 80</td>
                                <td>4 - 20</td>
                            </tr>
                            <tr>
                                <td>Длина</td>
                                <td>Стержни длиной 11.7 м</td>
                                <td>В соответствии с заявкой покупателя и см. ниже.</td>
                            </tr>
                            <tr>
                                <td>Экологичность</td>
                                <td>Экологична</td>
                                <td>Не токсична, по степени воздействия на организм человека и окружающую среду относится к 4 классу опасности (малоопасные).</td>
                            </tr>
                            <tr>
                                <td>Долговечность</td>
                                <td>В соответствии со строительными нормами</td>
                                <td>Прогнозируемая долговечность не менее 80 лет</td>
                            </tr>
                            <tr>
                                <td>Условная замена арматуры по физико-механическим свойствам</td>
                                <td>Ø 6 А-III
                                    <div>Ø 8 A-III</div>
                                    <div>Ø 10 A-III</div>
                                    <div>Ø 12 А-III</div>
                                    <div>Ø 14 А- III</div>
                                    <div>Ø 16 А- III</div>
                                    <div>Ø 18 А- III</div>
                                    <div>Ø 20 А- III</div>
                                </td>
                                <td>Ø 4 АКС
                                    <div>Ø 6 АКС</div>
                                    <div>Ø 7 АКС</div>
                                    <div>Ø 8 АКС</div>
                                    <div>Ø 10 АКС</div>
                                    <div>Ø 12 АКС</div>
                                    <div>Ø 14 АКС</div>
                                    <div>Ø 16 АКС</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Вес, кг. (при равнопрочной замене)</td>
                                <td>Ø 6 А-III  - 0,222
                                    <div>Ø 8 А-III  - 0,395</div>
                                    <div>Ø 10 A-III – 0,617</div>
                                    <div>Ø 12 А-III – 0,888</div>
                                    <div>Ø 14 А- III – 1,210</div>
                                    <div>Ø 16 А- III – 1,580</div>
                                    <div>Ø 18 А- III – 2,000</div>
                                    <div>Ø 20 А- III – 2,470</div>
                                </td>
                                <td>Ø 4 АКС– 0,02
                                <div>Ø 6 АКС– 0,05</div>
                                <div>Ø 7 АКС– 0,07</div>
                                <div>Ø 8 АКС– 0,09</div>
                                <div>Ø 10 АКС– 0,12</div>
                                <div>Ø 12 АКС– 0,20</div>
                                <div>Ø 14 АКС– 0,26</div>
                                <div>Ø 16 АКС– 0,35</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>