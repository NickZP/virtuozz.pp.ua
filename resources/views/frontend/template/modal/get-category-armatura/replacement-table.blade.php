<div id="modal-armatura-replacement-table" class="modal modal-table fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal"></button>
                <h4 class="modal-title text-center text-uppercase">Таблица равно-прочной замены металлической арматуры на стеклопластиковую</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Свойства</th>
                                <th colspan="4">
                                    <div>Арматура</div>
                                    <div class="th-caption">
                                        <div class="left">Металлическая</div>
                                        <div class="right">Стеклопластиковая</div>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                            <tr>
                                <td>Количество погонных метров в тонне</td>
                                <td>6 АIII
                                    <div>8 АIII</div>
                                    <div>10 AIII</div>
                                    <div>12 АIII</div>
                                    <div>14 АIII</div>
                                    <div>16 АIII</div>
                                    <div>18 АIII</div>
                                    <div>20 АIII</div>
                                    <div>22 АIII</div>
                                    <div>24 АIII</div>
                                </td>
                                <td>4504,50
                                    <div>2531,60</div>
                                    <div>1620,70</div>
                                    <div>1126,10</div>
                                    <div>826,40</div>
                                    <div>632,90</div>
                                    <div>500,00</div>
                                    <div>404,80</div>
                                    <div>335,50</div>
                                    <div>259,70</div>
                                </td>
                                <td>4 АCП
                                    <div>6 АCП</div>
                                    <div>7 АCП</div>
                                    <div>8 АCП</div>
                                    <div>10 АCП</div>
                                    <div>12 АCП</div>
                                    <div>14 АCП</div>
                                    <div>16 АCП</div>
                                    <div>18 АCП</div>
                                    <div>20 АCП</div>
                                </td>
                                <td>50000
                                    <div>20000</div>
                                    <div>14285</div>
                                    <div>12500</div>
                                    <div>8333</div>
                                    <div>5000</div>
                                    <div>3846</div>
                                    <div>2857</div>
                                    <div>2173</div>
                                    <div>1724</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Замена арматуры по физико-механическим свойствам</td>
                                <td colspan="2">6 АIII
                                    <div>8 АIII</div>
                                    <div>10 AIII</div>
                                    <div>12 АIII</div>
                                    <div>14 АIII</div>
                                    <div>16 АIII</div>
                                    <div>18 АIII</div>
                                    <div>20 АIII</div>
                                    <div>22 АIII</div>
                                    <div>24 АIII</div>
                                </td>
                                <td colspan="2">4 АCП
                                    <div>6 АCП</div>
                                    <div>7 АCП</div>
                                    <div>8 АCП</div>
                                    <div>10 АCП</div>
                                    <div>12 АCП</div>
                                    <div>14 АCП</div>
                                    <div>16 АCП</div>
                                    <div>18 АCП</div>
                                    <div>20 АCП</div>                               
                                </td>
                            </tr>
                            <tr>
                                <td>Вес 1 пог.м., кг.(при равнопрочной замене)</td>
                                <td>6 АIII
                                    <div>8 АIII</div>
                                    <div>10 AIII</div>
                                    <div>12 АIII</div>
                                    <div>14 АIII</div>
                                    <div>16 АIII</div>
                                    <div>18 АIII</div>
                                    <div>20 АIII</div>
                                    <div>22 АIII</div>
                                    <div>24 АIII</div>
                                </td>
                                <td>0,222
                                    <div>0,395</div>
                                    <div>0,617</div>
                                    <div>0,888</div>
                                    <div>1,21</div>
                                    <div>1,58</div>
                                    <div>2,0</div>
                                    <div>2,47</div>
                                    <div>2,98</div>
                                    <div>3,85</div>
                                </td>
                                <td>4 АКП
                                    <div>6 АКП</div>
                                    <div>7 АКП</div>
                                    <div>8 АКП</div>
                                    <div>10 АКП</div>
                                    <div>12 АКП</div>
                                    <div>14 АКП</div>
                                    <div>16 АКП</div>
                                    <div>18 АКП</div>
                                    <div>20 АКП</div>
                                </td>
                                <td>0,02
                                    <div>0,05</div>
                                    <div>0,07</div>
                                    <div>0,08</div>
                                    <div>0,12</div>
                                    <div>0,20</div>
                                    <div>0,26</div>
                                    <div>0,35</div>
                                    <div>0,46</div>
                                    <div>0,58</div>
                                </td>
                            </tr>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>