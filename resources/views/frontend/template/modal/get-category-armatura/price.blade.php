<div id="modal-armatura-price" class="modal modal-table fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal"></button>
                <h4 class="modal-title text-center text-uppercase">Прайс арматура полимерная композитная</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Диаметр арматуры arvit™, мм</th>
                                <th>Аналог стальной арматуры AIII, мм</th>
                                <th>Объем заказа, м<div>Цена за 1 м</div></th>
                                <th>Объем заказа, м<div>Цена за 1 м</div></th>
                                <th>Объем заказа, м<div>Цена за 1 м</div></th>
                                <th>Объем заказа, м<div>Цена за 1 м</div></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>4</td>
                                <td>6</td>
                                <td>до 4500<div>2,50 грн.</div></td>
                                <td>4500-20000<div>2,40 грн.</div></td>
                                <td>20000-90000<div>2,30 грн.</div></td>
                                <td>более 90000<div>договорная</div></td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>8</td>
                                <td>до 2500<div>4,35 грн.</div></td>
                                <td>2500-12500<div>4,15 грн.</div></td>
                                <td>12500-50000<div>3,95 грн.</div></td>
                                <td>более 50000<div>договорная</div></td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>10</td>
                                <td>до 1500<div>6,70 грн.</div></td>
                                <td>1500-8000<div>6,50 грн.</div></td>
                                <td>8000-32000<div>6,15 грн.</div></td>
                                <td>более 32000<div>договорная</div></td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>12</td>
                                <td>до 1000<div>9,20 грн.</div></td>
                                <td>1000-5500<div>8,90 грн.</div></td>
                                <td>5500-22500<div>8,35 грн.</div></td>
                                <td>более 22500<div>договорная</div></td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>14</td>
                                <td>до 800<div>13,40 грн.</div></td>
                                <td>800-4000<div>12,80 грн.</div></td>
                                <td>4000-16500<div>12,10 грн.</div></td>
                                <td>более 16500<div>договорная</div></td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>16</td>
                                <td>до 600<div>17,50 грн.</div></td>
                                <td>600-3000<div>16,50 грн.</div></td>
                                <td>3000-12500<div>16,00 грн.</div></td>
                                <td>более 12500<div>договорная</div></td>
                            </tr>
                            <tr>
                                <td>14</td>
                                <td>18</td>
                                <td>до 500<div>24,00 грн.</div></td>
                                <td>500-2500<div>23,00 грн.</div></td>
                                <td>2500-10000<div>22,00 грн.</div></td>
                                <td>более 10000<div>договорная</div></td>
                            </tr>
                            <tr>
                                <td>16</td>
                                <td>20</td>
                                <td>до 400<div>29,00 грн.</div></td>
                                <td>400-2000<div>27,50 грн.</div></td>
                                <td>2000-8000<div>25,90 грн.</div></td>
                                <td>более 8000<div>договорная</div></td>
                            </tr>
                            <tr>
                                <td>18</td>
                                <td>25</td>
                                <td colspan="2">до 1000<div>36,50 грн.</div></td>
                                <td colspan="2">более 1000<div>договорная</div></td>
                            </tr>
                            <tr>
                                <td>20</td>
                                <td>28</td>
                                <td colspan="2">до 1000<div>47,00 грн.</div></td>
                                <td colspan="2">более 1000<div>договорная</div></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>