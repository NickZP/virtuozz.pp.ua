<div id="modal-armatura-certificate" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal"></button>
                <h4 class="modal-title text-center text-uppercase">Сертификат арматуры</h4>
            </div>
            <div class="modal-body">
                <img src="/img/armatura-sertificat.jpg" alt="сертификат арматуры" class="img-responsive" width="100%">
            </div>
        </div>
    </div>
</div>