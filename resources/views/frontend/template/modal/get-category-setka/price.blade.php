<div id="modal-setka-price" class="modal modal-table fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal"></button>
                <h4 class="modal-title text-center text-uppercase">
                    Прайс композитная полимерная кладочная сетка
                </h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Размер сетки</th>
                            <th>
                                <div class="b-border">Розничная цена, грн.</div>
                                <div>При заказе</div>
                            </th>
                            <th>
                                <div class="b-border">Мелкий опт, грн.</div>
                                <div>При заказе от</div>
                            </th>
                            <th>
                                <div class="b-border">Средний опт, грн.</div>
                                <div>При заказе от</div>
                            </th>
                            <th>
                                <div class="b-border">Крупный опт, грн.</div>
                                <div>При заказе от</div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    d2мм.
                                    <div>50х50</div>
                                </td>
                                <td>
                                    <div class="b-border">До 300 кв.м.</div>
                                    <div>23.50</div>
                                </td>
                                <td>
                                    <div class="b-border">300-600 кв.м.</div>
                                    <div>22.55</div>
                                </td>
                                <td>
                                    <div class="b-border">600-1200кв.м.</div>
                                    <div>21.60</div>
                                </td>
                                <td>
                                    <div class="b-border">От 1200 кв.м.</div>
                                    <div>20.70</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d2мм.
                                    <div>100х100</div>
                                </td>
                                <td>
                                    <div class="b-border">До 300 кв.м.</div>
                                    <div>14.50</div>
                                </td>
                                <td>
                                    <div class="b-border">300-600 кв.м.</div>
                                    <div>13.90</div>
                                </td>
                                <td>
                                    <div class="b-border">600-1200кв.м.</div>
                                    <div>13.30</div>
                                </td>
                                <td>
                                    <div class="b-border">От 1200 кв.м.</div>
                                    <div>12.80</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d2мм. с песком
                                    <div>50х50</div>
                                </td>
                                <td>
                                    <div class="b-border">1-5 рулонов
                                        <div>(60-300кв.м.)</div>
                                    </div>
                                    <div>24.54</div>
                                </td>
                                <td>
                                    <div class="b-border">5-10 рулонов
                                        <div>(300-600 кв.м.)</div>
                                    </div>
                                    <div>23.55</div>
                                </td>
                                <td>
                                    <div class="b-border">10-20 рулонов
                                        <div>(600-1200кв.м.)</div>
                                    </div>
                                    <div>22.60</div>
                                </td>
                                <td>
                                    <div class="b-border">От 20 рулонов
                                        <div>(1200 кв.м.)</div>
                                    </div>
                                    <div>21.60</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d2мм. с песком
                                    <div>100х100</div>
                                </td>
                                <td>
                                    <div class="b-border">1-5 рулонов
                                        <div>(60-300кв.м.)</div>
                                    </div>
                                    <div>15.30</div>
                                </td>
                                <td>
                                    <div class="b-border">5-10 рулонов
                                        <div>(300-600 кв.м.)</div>
                                    </div>
                                    <div>14.70</div>
                                </td>
                                <td>
                                    <div class="b-border">10-20 рулонов
                                        <div>(600-1200кв.м.)</div>
                                    </div>
                                    <div>14.10</div>
                                </td>
                                <td>
                                    <div class="b-border">От 20 рулонов
                                        <div>(1200 кв.м.)</div>
                                    </div>
                                    <div>13.50</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d3мм.
                                    <div>50х50</div>
                                </td>
                                <td>
                                    <div class="b-border">До 300 кв.м.</div>
                                    <div>37.00</div>
                                </td>
                                <td>
                                    <div class="b-border">300-600 кв.м.</div>
                                    <div>35.50</div>
                                </td>
                                <td>
                                    <div class="b-border">600-1200кв.м.</div>
                                    <div>34.00</div>
                                </td>
                                <td>
                                    <div class="b-border">От 1200 кв.м.</div>
                                    <div>32.60</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d3мм.
                                    <div>100х100</div>
                                </td>
                                <td>
                                    <div class="b-border">До 300 кв.м.</div>
                                    <div>23.00</div>
                                </td>
                                <td>
                                    <div class="b-border">300-600 кв.м.</div>
                                    <div>22.10</div>
                                </td>
                                <td>
                                    <div class="b-border">600-1200кв.м.</div>
                                    <div>21.15</div>
                                </td>
                                <td>
                                    <div class="b-border">От 1200 кв.м.</div>
                                    <div>20.20</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d2мм. с песком
                                    <div>50х50</div>
                                </td>
                                <td>
                                    <div class="b-border">1-5 рулонов
                                        <div>(60-300кв.м.)</div>
                                    </div>
                                    <div>38.00</div>
                                </td>
                                <td>
                                    <div class="b-border">5-10 рулонов
                                        <div>(300-600 кв.м.)</div>
                                    </div>
                                    <div>36.50</div>
                                </td>
                                <td>
                                    <div class="b-border">10-20 рулонов
                                        <div>(600-1200кв.м.)</div>
                                    </div>
                                    <div>35.00</div>
                                </td>
                                <td>
                                    <div class="b-border">От 20 рулонов
                                        <div>(1200 кв.м.)</div>
                                    </div>
                                    <div>33.40</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d2мм. с песком
                                    <div>100х100</div>
                                </td>
                                <td>
                                    <div class="b-border">1-5 рулонов
                                        <div>(60-300кв.м.)</div>
                                    </div>
                                    <div>25.00</div>
                                </td>
                                <td>
                                    <div class="b-border">5-10 рулонов
                                        <div>(300-600 кв.м.)</div>
                                    </div>
                                    <div>24.00</div>
                                </td>
                                <td>
                                    <div class="b-border">10-20 рулонов
                                        <div>(600-1200кв.м.)</div>
                                    </div>
                                    <div>23.00</div>
                                </td>
                                <td>
                                    <div class="b-border">От 20 рулонов
                                        <div>(1200 кв.м.)</div>
                                    </div>
                                    <div>22.00</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>