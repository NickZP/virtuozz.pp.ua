<div id="modal-buy-product" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal"></button>
                <h4 class="modal-title" data-url=""></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <img src="" alt="" class="img-responsive" width="200px">
                    </div>
                    <div class="col-sm-8">
                        <p>Доступные варианты</p>
                        <div class="variants"></div>
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <a 
                            href="#"
                            class="btn btn-site about" 
                            type="button" 
                            data-url="{!! $category->route !!}" 
                            >
                            Перейти на страницу продукта
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>