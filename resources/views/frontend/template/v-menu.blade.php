<div class="list-group-head">Категории<span class="glyphicon glyphicon-align-justify"></span>
</div>
<div class="list-group panel">
    @foreach($sections as $section)
        <a href="{!! '#'.$section->route !!}"
            class="list-group-item list-group-item-success" 
            data-parent="#MainMenu">
            <span 
                class="section-text {!! \App\Helpers\Helper::isCurrent($section->route) ? 'active' : '' !!}" 
                data-url="{{ $section->route }}">
                  {!! $section->text !!}
            </span>
            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
        </a>
            <div class="collapse" id="{!! $section->route !!}">
            @if(empty($section->categories->id))
                @foreach($section->categories as $category)
                    <a class="list-group-item" data-url="{{ $category->route }}"> 
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="category-text">{!! $category->text !!}</span>
                    </a>
                @endforeach
            @endif
        </div>
    @endforeach
</div>