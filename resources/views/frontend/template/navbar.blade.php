<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"></a>
    </div>

    <div class="collapse navbar-collapse" id="menu">
      <ul class="nav navbar-nav">
        <li>{!! link_to_route("about-company", "О компании") !!}</li>
        <li>{!! link_to_route("dileram", "Оптовым покупателям") !!}</li>
        <li>{!! link_to_route("sertifikat", "Сертификаты") !!}</li>
        <li>{!! link_to_route("oplata-dostavka", "Оплата и доставка") !!}</li>
        <li>{!! link_to_route("contact", "Контакты") !!}</li>
      </ul>
      
      {!! Form::open(['route' => 'search', 'class' => 'navbar-form navbar-right', 'role' => 'search']) !!}
        <div class="form-group">
          <input type="text" class="form-control search-ajax" placeholder="Поиск" name="search">
        </div>
        <button type="submit" class="btn btn-default btn-search">
          <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
        </button>
      {!! Form::close() !!}
      
    </div>
  </div>
</nav>