<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" >
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <meta name="robots" content="index,follow" >
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" >
        {!! SEO::generate() !!}
        <meta name="_token" content="{!! csrf_token() !!}">
        
        <title>Интернет-магазин Виртуоз, Запорожский склад @yield('title')</title>

        <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
        <link href="{{ elixir('css/all.css') }}" rel="stylesheet" type="text/css">
        @section('css')
        @show

        <script type="text/javascript" src="{{ asset('js/respond.js') }}"></script>
        @section('scripts-head')
        @show
    </head>
        <body>
        <div class="content">
            <div class="not_worked center">Склад не работает. Заказы не обрабатываются.</div>
            <div class="wrap">
                    <header class="top-header">
                        <div class="blck-navigation">
                            <div class="container">
                                <div class="row">
                                    <div class="navigation">
                                        <a href="{!! route('index') !!}">Домой</a>
                                        @if(\Auth::check())
                                            <span class="hello">Здравствуйте, {!! \Auth::user()->name !!}</span>
                                            <a href="{!! route('frontend.user.index') !!}" class="vl">Профиль</a>
                                            <a href="{!! route('logout') !!}">Выйти</a>
                                        @else 
                                            <a href="{!! route('user.register') !!}">Регистрация</a>
                                            <a href="{!! route('user.login') !!}">Войти</a>
                                        @endif
                                    </div>
                                </div>
                             </div>
                        </div>
                        <div class="messageStackSuccess larger" style="display: none;">
                            <img 
                                width="31"
                                height="27" 
                                title=" Success " 
                                alt="Success" 
                                src="/img/success-product-sale.png"
                            >  
                            Продукт добавлен в корзину
                       </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-3 logo">
                                    <a href="{!! \URL::route('index') !!}">
                                        {!! HTML::image('img/logo.png', 'Интернет магазин строительной химии в Запорожье "Виртуоз"') !!}
                                    </a>
                                </div>
                                <div class="col-lg-6 company-name">
                                    <div class="info">
                                        <div class="name">Региональный склад (Запорожье)</div>
                                        <div class="phone-contact"></div>
                                    </div>
                                </div>
                                <div class="col-lg-3 cart">
                                    <div class="cart-main">
                                        <div class="cart-header">
                                            <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
                                            <a href="{!! route('frontend.basket.show') !!}">Ваши покупки:</a>
                                        </div>
                                        <span class="cart-info">Сейчас в вашей корзине:
                                            <span class="orders">
                                                <span class="count_order">0</span>&nbsp;товаров
                                            </span>
                                        </span>
                                    </div>
                                    <div class="cart-content">
                                        <div class="cart-item">
                                            <div class="left-info"><a href=""><img></a></div>
                                            <div class="center-info">
                                                <a href="" class="cart-name"></a>
                                                <br>
                                                <span class="quantity">1 <em class="spr">x</em> </span>
                                                <span class="cart-price"></span>    
                                            </div>
                                        </div>
                                        <div class="cart-bottom">
                                            <span class="total"><span></span></span>
                                            <div class="btns">
                                                <a href="{!! route('frontend.basket.show') !!}" class="btn btn-site" role="button">
                                                    <span>Оформление заказа</span>
                                                </a>
                                            </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 menu">
                                    @include('frontend.template.navbar')
                                </div>
                            </div>
                        </div>
                    </header>
                    <div class="container">                
                        
                        <div class="row main">
                            <div class="col-lg-3 col-xs-4 clear-left">
                                <div id="MainMenu">
                                    @include('frontend.template.v-menu')
                                </div>
                            </div>
                            <div class="col-lg-9 col-xs-8">
                                <section id="content" class="main-container">
                                    @yield('content')
                                </section>
                            </div>
                        </div>
                        
                    </div>
                
                </div><!-- end wrap-->
            
                <footer>
                    <nav class="navbar navbar-default navbar-bottom" role="navigation">
                        <div class="container">
                            <div class="row main">
                            </div>
                            <div>
                                <div class="navigation">
                                    <a href="{!! route('about-company') !!}">О компании</a>
                                    <a href="{!! route('dileram') !!}">Оптовым покупателям</a>
                                    <a href="{!! route('sertifikat') !!}">Сертификаты</a>
                                    <a href="{!! route('oplata-dostavka') !!}">Оплата и доставка</a>
                                    <a href="{!! route('contact') !!}">Контакты</a>
                                </div>
                                <div class="copyright text-center">
                                    Copyright &copy; 2016
                                </div>
                            </div>
                        </div>
                        
                    </nav>
                </footer> 
            </div>       
        @section('scripts')
            <script type="text/javascript" src="{{ asset('js/all.js') }}"></script>
        @show
    </body>
</html>