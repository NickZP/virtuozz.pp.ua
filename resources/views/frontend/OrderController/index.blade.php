@extends('frontend.app')

@section('title', 'Мои заказы')

@section('content')

    <div class="panel-group" id="accordionOrder" role="tablist" aria-multiselectable="true">
        
        @foreach (\Auth::user()->orders as $index => $order)
            <div class="panel panel-default panel-{{ $order->state->name }}">
                <div class="panel-heading" role="tab" id="heading{{ $index }}">
                    <h4 class="panel-title">
                        <a 
                            role="button" 
                            data-toggle="collapse" 
                            data-parent="#accordion" 
                            href="#collapse{{ $index }}" 
                            aria-expanded="true" 
                            aria-controls="collapse{{ $index }}"
                        >
                            <span class="date">{{ $order->created_at }}</span>
                            Заказ №
                            <span class="number">{{ $order->id }}</span>
                            <span class="sum">{{ $order->history->price }} грн</span>
                        </a>
                        <span class="status">{{ $order->state->display_name }}</span>
                    </h4>
                </div>
                <div 
                    id="collapse{{ $index}}" 
                    class="panel-collapse collapse" 
                    role="tabpanel" 
                    aria-labelledby="heading{{ $index }}"
                >
                    <div class="panel-body">
                        @foreach ($order->history->history as $product)
                        <div class="row">
                            <div class="col-md-1">
                                <img src="{{ $product->img }}" alt="{{ $product->title }}" width="50px">
                            </div>
                            <div class="col-md-7">
                                <a href="{{ $product->url }}">{{ $product->title }}</a>
                            </div>
                            <div class="col-md-1">
                                <span class="measure">{{ $product->measurement }}</span>    
                            </div>
                            <div class="col-md-3">
                                <div class="right">
                                    <span class="amount">{{ $product->amount }}</span> 
                                    <span class="bold">x</span>
                                    <span>{{ $product->price }}грн</span>  
                                    <span class="bold">=</span>  
                                    <span class="sum">{{ $product->amount * $product->price }}</span>грн
                                </div>
                            </div>                       
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
        
    </div>
@endsection