<table width="100%" class="table table-responsive table-striped">
    <thead class="text-center">
        <tr>
            <th>Изображение</th>
            <th>Название</th>
            <th>Упаковка</th>
            <th>Цена, грн</th>
            <th>Количество</th>
            <th>Итого, грн</th>
            <th>Удалить</th>
        </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
            @foreach($product->variantsales as $key => $variant)
                @if(!empty($new_arr[$product->name . $variant->measurement]))
                    <tr>
                        <td><img height=50 src="{!! $product->image !!}"></td>
                        <td><a href="{!! $product->categories->first()->route .'/'. $product->route !!}">{!! $product->name !!}</a></td>
                        <td class="measurement text-center">{!! $variant->measurement !!}</td>
                        <td class="text-center">{!! $variant->price !!}</td>
                        <td class="text-center">
                            <div class="amount">
                                <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                {!! Form::text('col', $new_arr[$product->name . $variant->measurement], ["class" => "form-control number"]) !!}
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </div>
                        </td>
                        <td class="price_product text-center">{!! $variant->price * $new_arr[$product->name . $variant->measurement] !!}</td>
                        <td class="text-center">
                            <a class="del_order" href="#"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                        </td>
                        {!! Form::hidden('sum', $sum += $variant->price * $new_arr[$product->name . $variant->measurement]) !!}
                        {!! Form::hidden('id', $product->id, ['class' => 'id']) !!}
                    </tr>
                @endif
            @endforeach
        @endforeach
    </tbody>
</table>
<div class="form-group">
    <div class="col-sm-offset-8 col-sm-4">
        <div class="cart-total">
            <span class="cart-total-title">Итого:</span>
            <span class="cart-total-currency">
                <span class="cart-cost">{!! number_format($sum, 2, '.', "") !!}</span>
                <span class="cart-currency">грн</span>
            </span>
        </div>
    </div>
</div>