@extends('frontend.app')

@section('title', 'Корзина')

@section('content')
    <div class="row">
        @if(count($products)==0)
            <h2>Ничего не выбрано</h2>
        @else
            <h2>Ваш заказ</h2>
                <table width=100% class="table-responsive table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Изображение</th>
                            <th>Название</th>
                            <th>Упаковка</th>
                            <th>Цена</th>
                            <th>Количество</th>
                            <th>Итого</th>
                            <th>Действие</th>
                        </tr>
                    </thead>
                    @foreach($products as $product)
                        @foreach($product->variantsales as $variant)
                            @if(!empty($new_arr[$product->name . $variant->measurement]))
                                <tr>
                                    <td></td>
                                    <td><img height=50 src="{!! $product->image !!}"></td>
                                    <td>{!! $product->name !!}</td>
                                    <td>{!! $variant->measurement !!}</td>
                                    <td>{!!$variant->price !!}</td>
                                    <td>
                                        <div class="amount">
                                            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                            <input type="text" class="form-control number" name="col[]" value="{!! $new_arr[$product->name . $variant->measurement]!!}">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </div>
                                    </td>
                                    <td class="price_product">
                                    <td>
                                        <button type="button" class="btn btn-danger del_product">Удалить</button>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                </table>
        @endif
    </div>
@endsection
