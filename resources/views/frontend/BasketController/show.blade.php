@extends('frontend.app')

@section('title', 'Корзина')

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/jquery.mask.min.js') }}"></script> 
    <script>
        //$('.amount > .number').mask('Z00', {translation:  {'Z': {pattern: /[1-9]/, optional: true}}});
        $('.phone').mask('(000) 000-00-00', {placeholder: "(___) ___-__-__"});
    </script> 
@stop

@section('content')
    <div class="row">
        @if($errors->any())
            @include('admin.particial.errors')
        @endif
        
        @if(count($products)==0)
            <h2>Ничего не выбрано</h2>
        @else
            <h2>Ваш заказ</h2>
            {!! Form::open(['route' => 'frontend.basket.checkout', 'class' => 'form-horizontal']) !!}
                <div class="zakaz">
                    @include('frontend.BasketController.ajax.zakaz')
                </div>
                
                <div class="">
                    <div class="form-group">
                        <label for="email" class="col-sm-2 label-left">*E-mail:</label>
                        <div class="col-sm-4">
                            @if(!\Auth::check())
                                {!! Form::text('email', null, [
                                                'class' => 'form-control',
                                                'id' => 'email', 
                                                'placeholder' => 'E-mail'
                                                ]) 
                                !!}
                            @else
                                {!! Form::text('email', \Auth::user()->email, [
                                                'class' => 'form-control',
                                                'id' => 'email', 
                                                'placeholder' => 'E-mail'
                                                ]) 
                                !!}
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="name" class="col-sm-2 label-left">*Имя:</label>
                        <div class="col-sm-4">
                            @if(!\Auth::check())
                                {!! Form::text('name', null, [
                                                'class' => 'form-control',
                                                'id' => 'name', 
                                                'placeholder' => 'Имя'
                                                ]) 
                                !!}
                            @else
                                {!! Form::text('name', \Auth::user()->name, [
                                                'class' => 'form-control',
                                                'id' => 'name', 
                                                'placeholder' => 'Имя'
                                                ]) 
                                !!}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-sm-2 label-left">*Телефон:</label>
                        <div class="col-sm-4">
                            @if(!\Auth::check())
                                {!! Form::text('phone', null, [
                                                'class' => 'form-control phone',
                                                'id' => 'phone', 
                                                'placeholder' => 'Телефон'
                                                ]) 
                                !!}
                            @else
                                {!! Form::text('phone', \Auth::user()->userinfo->phone, [
                                        'class' => 'form-control phone',
                                        'id' => 'phone', 
                                        'placeholder' => 'Телефон'
                                        ]) 
                                !!}
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-sm-offset-4 col-sm-4">
                        {!! Form::submit('Оформить заказ', ['class' => 'btn btn-site btn-block']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        @endif
    </div>
@endsection
