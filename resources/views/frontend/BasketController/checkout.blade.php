@extends('frontend.app')

@section('title', 'Заказ принят')

@section('content')
    <div class="row">
    
        <h2 class="text-center">Ваш заказ принят</h2>
        <table width="100%" class="table table-responsive table-bordered">
            <thead>
                <tr>
                    <th>№</th>
                    <th>Изображение</th>
                    <th>Название</th>
                    <th>Упаковка</th>
                    <th>Цена,грн</th>
                    <th>Количество</th>
                    <th>Итого,грн</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order->infos as $key => $info)
                    <tr>
                        <td>{!! ++$key !!}</td>
                        <td><img height=50 src="{!! $info->variantsale->product->image !!}"></td>
                        <td>{!! $info->variantsale->product->shortname !!}</td>
                        <td>{!! $info->variantsale->measurement !!}</td>
                        <td>{!! $info->variantsale->price !!}</td>
                        <td>{!! $info->amount !!}</td>
                        <td>{!! $info->amount * $info->variantsale->price !!}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-sm-offset-8 col-sm-4">
                <div class="cart-total">
                    <span class="cart-total-title">Итого:</span>
                    <span class="cart-total-currency">
                        <span class="cart-cost">{!! $order->history->price !!}</span>
                        <span class="cart-currency">грн</span>
                    </span>
                </div>
            </div>
        </div>
        <hr>
        <h3>Контактная информация</h3>
        <div class="row contact">
            <div class="col-lg-2">Имя:</div>
            <div class="col-lg-10"><span>{!! $order->history->name !!}</span></div>
            <div class="col-lg-2">Телефон:</div>
            <div class="col-lg-10"><span>{!! $order->history->phone !!}</span></div>
            <div class="col-lg-2">E-mail:</div>
            <div class="col-lg-10"><span>{!! $order->history->email !!}</span></div>
        </div>
    </div>
@endsection
