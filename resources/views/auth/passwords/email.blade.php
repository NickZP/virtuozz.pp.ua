@extends('frontend.app')

@section('title', ' - Вход')

@section('scripts-head')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@stop

@section('content')

    <h3 class="text-center">Восстановление пароля</h3>

    @if ($errors->any())
        @include('admin.particial.errors')
    @endif

    @if (Session::has("messages"))
        @include('admin.particial.session')
    @endif
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
        {!! csrf_field() !!}
        
        <div class="form-group">
            <label for="inputName" class="col-sm-3 control-label">E-Mail:</label>
            <div class="col-sm-7">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 g-recaptcha" data-sitekey="6Lf1GSQTAAAAAKPo9MgQ-w2hgkPMSBCSDlxgN06x"></div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-5 col-sm-4">
                {!! Form::submit('Выслать', ['class' => 'btn btn-site']) !!}
            </div>
        </div>
        
    </form>
@endsection
