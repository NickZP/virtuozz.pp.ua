@extends('frontend.app')

@section('title', ' - Вход')

@section('content')

    <h3 class="text-center">Сброс пароля</h3>

    @if ($errors->any())
        @include('admin.particial.errors')
    @endif

    @if (Session::has("messages"))
        @include('admin.particial.session')
    @endif
    
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
        {!! csrf_field() !!}

        <input type="hidden" name="token" value="{{ $token }}">
        
        <div class="form-group">
            <label for="email" class="col-sm-3 control-label">E-Mail:</label>
            <div class="col-sm-7">
                <input type="email" class="form-control" name="email" value="{{ $email or old('email') }}">
            </div>
        </div>
        
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Пароль:</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" name="password">
            </div>
        </div>
        
        <div class="form-group">
            <label for="password_confirmation" class="col-sm-3 control-label">Повторите пароль:</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" name="password_confirmation">
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-offset-5 col-sm-4">
                {!! Form::submit('Сбросить Пароль', ['class' => 'btn btn-site']) !!}
            </div>
        </div>
        
    </form>
@endsection