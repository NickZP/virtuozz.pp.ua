<h2>Заказ № {!! $order->id !!} успешно оформлен</h2>

<h3>Заказанные товары:</h3>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <thead>
        <tr>
            <th>№</th>
            <th>Название</th>
            <th>Упаковка</th>
            <th>Цена</th>
            <th>Количество</th>
            <th>Итого</th>
        </tr>
    </thead>
    <tbody>
        @foreach($order->infos as $key => $info)
            <tr>
                <td align="center">{!! ++$key !!}</td>
                <td align="center">{!! $info->variantsale->product->shortname !!}</td>
                <td align="center">{!! $info->variantsale->measurement !!}</td>
                <td align="center">{!! $info->variantsale->price !!}</td>
                <td align="center">{!! $info->amount !!}</td>
                <td align="center">{!! $info->amount * $info->variantsale->price !!} грн</td>
            </tr>
        @endforeach
    </tbody>
</table>

<hr>
<b>Общая сумма заказа: {!! $order->history->price !!} грн.</b>
<br>
<br>
<h3>Контактная информация:</h3>
<b>Имя:</b> 
    {!! $order->history->name !!}
<br>
<b>Телефон:</b> 
    {!! $order->history->phone !!}
<br>
<b>E-mail:</b> 
    {!! $order->history->email !!}
<br>
<br>
<br>
<a href="{!! route('index') !!}">{!! route('index') !!}</a> - строительные добавки.