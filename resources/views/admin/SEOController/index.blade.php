@extends('admin.app')

@section('css')
    @parent
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')
    @parent
    <script>
        $('#modal-seo').on('show.bs.modal', function (e) {
            var $text = $(e.relatedTarget).text();
            var $id = $(e.relatedTarget).data('product');
            $('.modal-title').text($text);
            var $textarea = $('.modal-body textarea');
            $textarea.attr('data-product', $id);
            $textarea.val('');
            $textarea.css("background-color", 'rgba(0, 0, 0, 0.4)');

            var data = {};
            data['id'] = $id;
            $.ajax({
                url: "/admin/seo/get",
                type: 'post',
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                data:  data,
                beforeSend: function(){
                    $textarea.attr('readonly', 'true');
                    $textarea.css("background-color", 'rgba(0, 0, 0, 0.1)');
                },
                success: function(text){
                    $textarea.val(text);
                    $textarea.removeAttr( 'readonly' );
                    $textarea.css("background-color", 'rgba(0, 0, 0, 0.4)');
                },
                error: function(html){
                    return false;
                } 
            });
        });

        
        $(document).on('click', '.modal-content .btn-site', function(e){
            var data = {};
            data['description'] = $('.modal-body textarea').val();
            data['id'] = $('.modal-body textarea').attr('data-product');
            
            $.ajax({
                url: "/admin/seo/save",
                type: 'post',
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                data:  data,
                success: function(response){
                    if(response.ok == 'true'){
                        $('.list-group-item a').each(function(){
                            if( $(this).attr('data-product') == data['id']){
                                $(this).next().text( response.count );

                                var $parent = $(this).parent();
                                if ( response.count == 0 ){
                                    if($parent.hasClass('success')){
                                        $($parent).removeClass('success');
                                    }
                                }
                                else{
                                    if( !$parent.hasClass('success') ){
                                        $($parent).addClass('success');
                                    }
                                }
                            }
                        });
                    }
                },
                error: function(response){
                    return false;
                } 
            });
            
        });        

    </script>
@stop

@section('content')
    @if (Session::has('messages'))
        @include('admin.particial.success')
    @endif
    
    {!! Breadcrumbs::render('products') !!}

    <div id="sectionNav">

        <div id="contentList" data-url="{!! \Request::segment(2) !!}">
            @include('admin.SEOController.ajax.seoList')
        </div>

    </div>

    @include('admin.templates.modals.modalSEO')
@stop