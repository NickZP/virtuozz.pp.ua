<ul class="list-group">
  @foreach($products as $product)
    @if( !is_null($product->seo) )
        <li class="list-group-item {{ !empty($product->seo->count_keywords) ? 'success' : ''}}">
    @else
        <li class="list-group-item">
    @endif
        <a href="#modal-seo" data-toggle="modal" data-product="{{ $product->id }}">{{ $product->shortname }}</a>
        <span class="product-list">
            @if( is_null($product->seo) )
              0
            @else
              {{ $product->seo->count_keywords }}
            @endif
        </span>
    </li>
  @endforeach
</ul>
<div class="text-center">{!! $products->render() !!}</div>