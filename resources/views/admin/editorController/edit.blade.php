@extends('admin.app')

@section('title', 'Редактирование редактора')

@section('content')
    @if ($errors->any())
        @include('admin.particial.errors')
    @endif
    
    {!! Breadcrumbs::render('editor_edit', $editor) !!}
    
    <div>
        <h3 class="text-center title">Редактирование редактора</h3>
    </div>
    {!! Form::model($editor, ['method' => 'put', 'route' => ['admin.editors.update', $editor->user->id], 'class' => 'form-horizontal']) !!}
        <div class="container-form">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Имя:</label>
                <div class="col-sm-4">
                    {!! Form::text('name', $editor->user->name, [
                                    'class' => 'form-control',
                                    'id' => 'name', 
                                    'placeholder' => 'Имя'
                                    ]) 
                    !!}
                </div>
            </div>
            <div class="form-group">    
                <label for="email" class="col-sm-2 control-label">Эл.почта:</label>
                <div class="col-sm-4">
                    {!! Form::email('email', $editor->user->email, [
                                    'class' => 'form-control',
                                    'id' => 'email', 
                                    'placeholder' => 'Эл. почта'
                                    ]) 
                    !!}
                </div>
            </div>
            <div class="form-group">
                <label for="role" class="col-sm-2 control-label">Роль:</label>
                <div class="col-sm-4">
                    {!! Form::select('roles', $roles, $role, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="phone" class="col-sm-2 control-label">Пароль:</label>
                <div class="col-sm-4">
                    {!! Form::password('password', [
                                    'class' => 'form-control', 
                                    'id' => 'password', 
                                    'placeholder' => 'Введите пароль'
                                    ]) 
                    !!}
                </div>
            </div>
            <div class="form-group">
                <label for="password_confirmation" class="col-sm-2 control-label">Повторите пароль:</label>
                <div class="col-sm-4">
                    {!! Form::password('password_confirmation', [
                                    'class' => 'form-control', 
                                    'id' => 'password_confirmation', 
                                    'placeholder' => 'Повторите ваш пароль'
                                    ]) 
                    !!}
                </div>
            </div>
        </div>
        
        <div class="col-sm-offset-5 col-sm-2">
            {!! Form::submit("Сохранить", ["class" => "btn btn-block btn-primary"] ) !!}
        </div>
    {!! Form::close()!!}
@endSection