@extends('admin.app')

@section('title', 'Редакторы')

@section('content')
    @if (Session::has('messages'))
        @include('admin.particial.success')
    @endif
    
    {!! Breadcrumbs::render('editors') !!}
    
    <div>
        <div class="col-sm-3 button add">
            {!! link_to_route("admin.editors.create", "Добавить редактора", [], ["class" => "btn btn-success btn-block"]) !!}
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-3 search">
            {!! Form::text('search', null, [ 
                                        'class' => 'form-control search-ajax', 
                                        'placeholder' => 'Поиск редактора',
                                        ])
            !!}
        </div>
        <div class="clearfix"></div>
        <div id="sectionNav">
            <div class="clearfix"></div>
            <div id="contentList" data-url="{!! \Request::segment(2) !!}">
                @include('admin.editorController.ajax.editorList')
            </div>
        </div>
    </div>
@endSection