@extends('admin.app')

@section('title', 'Добавление редактора')

@section('content')
    @if ($errors->any())
        @include('admin.particial.errors')
    @endif
    
    {!! Breadcrumbs::render('editor_new') !!}
    
    <div>
        <h3 class="text-center title">Добавить редактора</h3>
    </div>
    {!! Form::open(['route' => 'admin.editors.store', 'class' => 'form-horizontal']) !!}
        <div class="container-form">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Имя:</label>
                <div class="col-sm-4">
                    {!! Form::text('name', null, [
                                    'class' => 'form-control',
                                    'id' => 'name', 
                                    'placeholder' => 'Имя'
                                    ]) 
                    !!}
                </div>
            </div>
            <div class="form-group">    
                <label for="email" class="col-sm-2 control-label">Эл.почта:</label>
                <div class="col-sm-4">
                    {!! Form::email('email', null, [
                                    'class' => 'form-control',
                                    'id' => 'email', 
                                    'placeholder' => 'Эл. почта'
                                    ]) 
                    !!}
                </div>
            </div>
            <div class="form-group">
                <label for="role" class="col-sm-2 control-label">Роль:</label>
                <div class="col-sm-4">
                    {!! Form::select('roles', $roles, $role, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="phone" class="col-sm-2 control-label">Пароль:</label>
                <div class="col-sm-4">
                    {!! Form::password('password', [
                                    'class' => 'form-control', 
                                    'id' => 'password', 
                                    'placeholder' => 'Введите пароль'
                                    ]) 
                    !!}
                </div>
            </div>
            <div class="form-group">
                <label for="password_confirmation" class="col-sm-2 control-label">Повторите пароль:</label>
                <div class="col-sm-4">
                    {!! Form::password('password_confirmation', [
                                    'class' => 'form-control', 
                                    'id' => 'password_confirmation', 
                                    'placeholder' => 'Повторите ваш пароль'
                                    ]) 
                    !!}
                </div>
            </div>
        </div>
        
        <div class="col-sm-offset-5 col-sm-2">
            {!! Form::submit("Добавить", ["class" => "btn btn-block btn-primary"] ) !!}
        </div>
    {!! Form::close()!!}
@endSection