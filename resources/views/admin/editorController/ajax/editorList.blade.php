<ul class="list-group">
  @foreach($editors as $editor)
    <li class="list-group-item">
        {!! $editor->name !!}
        <span class="right-list">
            {!! link_to_route('admin.editors.edit', 'Редактировать', [$editor->id]) !!}
            | 
            {!! link_to_route('admin.editors.delete', 'Удалить', [$editor->id], ['class' => 'deleteCheck']) !!}
        </span>
    </li>
  @endforeach
</ul>
<div class="text-center">{!! $editors->render() !!}</div>