@extends('admin.app')

@section('css')
    @parent
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('js/bootstrap.file-input.js') }}"></script>
    <script>
        if($('.file-input-wrapper')){
            $('.file-inputs').bootstrapFileInput();
            $('.file-input-wrapper').show();
        }
    </script>
@stop

@section('content')
    @if ($errors->any())
        @include('admin.particial.errors')
    @endif
    
    {!! Breadcrumbs::render('category_new') !!}
    
    <div>
        <h3 class="text-center title">Добавить категорию</h3>
    </div>
    {!! Form::open(['route' => 'postCategory_new', 'class' => 'form-horizontal', 'files' => true]) !!}
        <div class="container-form">
            <div class="form-group">
                <label for="CategoryName" class="col-sm-2 control-label">Имя категории:</label>
                <div class="col-sm-4">
                {!! Form::text('name', null, ['class' => 'form-control',
                            'id' => 'CategoryName', 'placeholder' => 'Имя категории']) !!}
                </div>
            </div>

            <div class="form-group">
                <label for="route" class="col-sm-2 control-label">Путь:</label>
                <div class="col-sm-4">
                    {!! Form::text('route', null, ['class' => 'form-control',
                            'id' => 'route', 'placeholder' => 'Путь']) !!}
                </div>
            </div>
            
            <div class="form-group">    
                <label for="Sections" class="col-sm-2 control-label">Раздел:</label>
                <div class="col-sm-4">
                    {!! Form::select('Sections', $sections, 1, ['class' => 'form-control selectpicker']) !!}
                </div>
            </div>

            <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Описание:</label>
                <div class="col-sm-8">
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>
            </div>
            
            <div class="form-group">
                <label for="image" class="col-sm-2 control-label">Изображение:</label>
                <div class="col-sm-8">
                    <a class="file-input-wrapper btn btn-default ">
                        <span>Выбрать</span>
                        {!! Form::file('image') !!}
                    </a>
                    <span class="file-input-name"></span>
                </div>
            </div>
        </div>
        
        <div class="col-sm-offset-5 col-sm-2">
            {!! Form::submit("Добавить", ["class" => "btn btn-block btn-primary"] ) !!}
        </div>
    {!! Form::close() !!}
@stop