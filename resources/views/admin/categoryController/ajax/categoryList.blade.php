<!--<ul>
    @foreach($categories as $category)
        @if($category->section_id)
            <div class="category">
                <li><input type="checkbox" name="select"></li>
                <li><input type="hidden" name="catid" value="{!! $category->id !!}"></li>
                <li><a href="#">{!! $category->text !!}</a></li>
            </div>
            <div class="clear"></div>
        @endif
    @endforeach
</ul>-->
<ul class="list-group">
  @foreach($categories as $category)
    <li class="list-group-item">
        {!! $category->text !!}
        <span class="category-list">
            {!! link_to_route('category_edit', 'Редактировать', [$category->id]) !!}
            | 
            {!! link_to_route('category_delete', 'Удалить', [$category->id], ['class' => 'deleteCheck']) !!}
            <!--<a class="deleteCheck" href="">Удалить</a>-->
        </span>
    </li>
  @endforeach
</ul>
<div class="text-center">{!! $categories->render() !!}</div>
