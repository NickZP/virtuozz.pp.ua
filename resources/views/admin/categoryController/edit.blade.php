@extends('admin.app')

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/bootstrap.file-input.js') }}"></script>
    <script>
        if($('.file-input-wrapper')){
            $('.file-inputs').bootstrapFileInput();
            $('.file-input-wrapper').show();
        }
    </script>
    <script>
        $('#modal').on('show.bs.modal', function (e) {
            var $img = $(e.relatedTarget).find('#img').val();
            $('.modal-body img').attr("src", $img);
        })
    </script>
@stop

@section('content')
        @if ($errors->any())
            @include('admin.particial.errors')
        @endif
        
        {!! Breadcrumbs::render('category_edit', $category) !!}
        
        <div>
            <h3 class="text-center title">Редактирование категории</h3>
        </div>
        {!! Form::open(['route' => 'postCategory_edit', 'class' => 'form-horizontal', 'files' => true]) !!}
            <div class="container-form">
                <div class="form-group">
                    <label for="CategoryName" class="col-sm-2 control-label">Имя категории:</label>
                    <div class="col-sm-4">
                    {!! Form::text('name', $category->text, ['class' => 'form-control',
                                'id' => 'CategoryName', 'placeholder' => 'Имя категории']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="route" class="col-sm-2 control-label">Путь:</label>
                    <div class="col-sm-4">
                        {!! Form::text('route', $category->route, 
                                    [
                                        'class' => 'form-control',
                                        'id' => 'route', 
                                        'placeholder' => 'Путь'
                                    ])
                        !!}
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="sections" class="col-sm-2 control-label">Раздел:</label>
                    <div class="col-sm-4">
                        {!! Form::select('sections', $sections, $category->section_id, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Описание:</label>
                    <div class="col-sm-8">
                        {!! Form::textarea('description', $category->description, ['class' => 'form-control']) !!}
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="image" class="col-sm-2 control-label">Изображение:</label>
                    <div class="col-sm-1 section-image">
                        <a href="#modal" data-toggle="modal">
                            @if($category->image)
                                {!! HTML::image($category->image, $category->text, ["width" => "50"]) !!}
                                {!! Form::hidden('img', $category->image, ['id' => 'img']) !!}
                            @else 
                                <span class="glyphicon glyphicon-picture"></span>
                                {!! Form::hidden('img', '../../../img/not_image.jpg', ['id' => 'img']) !!}
                            @endif
                        </a>
                    </div>
                    <div class="col-sm-8">
                        <a class="file-input-wrapper btn btn-default" style="display: none;">
                            <span>Выбрать</span>
                            {!! Form::file('image') !!}
                        </a>
                        <span class="file-input-name"></span>
                    </div>
                </div>
                
            </div>
            
            <div class="col-sm-offset-5 col-sm-2">
                {!! Form::hidden('id', $category->id) !!}
                {!! Form::submit("Сохранить", ["class" => "btn btn-block btn-primary"] ) !!}
            </div>
        {!! Form::close() !!}
        
        @include('admin.templates.modal')
        
@stop
