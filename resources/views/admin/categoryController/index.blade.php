@extends('admin.app')

@section('css')
    @parent
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}"></script>  
@stop

@section('content')
    @if (Session::has('messages'))
        @include('admin.particial.success')
    @endif
    
    {!! Breadcrumbs::render('categories') !!}
    
    <div>
        <div class="col-sm-3 button add">
            {!! link_to_route("category_new", "Добавить категорию", null, ["class" => "btn btn-success btn-block"]) !!}
        </div>
        <div class="clearfix"></div>
        <div id="sectionNav">
            <div id="categoryNav" class="col-sm-3 button add">
                <select class="form-control selectpicker">
                    <option>{!! $text['all'] !!}</option>
                    @foreach($sections as $section)
                        <option value="{!! $section->text !!}">{!! $section->text !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="clearfix"></div>
            <!--<div id="categoryNav">
                <div id="categoryName">Название раздела</div>
                        
                </div>-->
            <div id="contentList" data-url="{!! \Request::segment(2) !!}">
                @include('admin.categoryController.ajax.categoryList')
            </div>
        </div>
    </div>
@stop

