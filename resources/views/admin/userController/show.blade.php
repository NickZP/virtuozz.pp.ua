@extends('admin.app')

@section('title', 'Просмотр пользователя')

@section('content')
    
    {!! Breadcrumbs::render('user_show', $user) !!}
    
    <div>
        <h3 class="text-center title">{{ $user->name }}</h3>
    </div>
    
    <div class="row">
        <label class="col-md-2">Имя:</label>
        <span class="col-md-4">{{ $user->name }}</span>
    </div>
    
   <div class="row">
        <label class="col-md-2">Телефон:</label>
        <span class="col-md-4">{{ $user->userInfo->phone }}</span>
    </div>
    
   <div class="row">
        <label class="col-md-2">Эл.почта:</label>
        <span class="col-md-4">{{ $user->email }}</span>
    </div>
    
@endSection