<ul class="list-group">
  @foreach($users as $user)
    <li class="list-group-item">
        {!! $user->name !!}
        <span class="right-list">
            {!! link_to_route('admin.users.edit', 'Редактировать', [$user->id]) !!}
            | 
            {!! link_to_route('admin.users.delete', 'Удалить', [$user->id], ['class' => 'deleteCheck']) !!}
            <!--<a class="deleteCheck" href="">Удалить</a>-->
        </span>
    </li>
  @endforeach
</ul>
<div class="text-center">{!! $users->render() !!}</div>