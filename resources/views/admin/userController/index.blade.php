@extends('admin.app')

@section('title', 'Пользователи')

@section('content')
    @if (Session::has('messages'))
        @include('admin.particial.success')
    @endif
    
    {!! Breadcrumbs::render('users') !!}
    
    <div>
        <div class="col-sm-3 button add">
            {!! link_to_route("admin.users.create", "Добавить пользователя", [], ["class" => "btn btn-success btn-block"]) !!}
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-3 search">
            {!! Form::text('search', null, [ 
                                        'class' => 'form-control search-ajax', 
                                        'placeholder' => 'Поиск пользователя'
                                        ])
            !!}
        </div>
        <div class="clearfix"></div>
        <div id="sectionNav">
            <div class="clearfix"></div>
            <div id="contentList" data-url="{!! \Request::segment(2) !!}">
                @include('admin.userController.ajax.userList')
            </div>
        </div>
    </div>
@endSection