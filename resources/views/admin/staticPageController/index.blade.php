@extends('admin.app')

@section('title', 'Статические страницы')

@section('content')
    @if (Session::has('messages'))
        @include('admin.particial.success')
    @endif
    
    {!! Breadcrumbs::render('static-pages') !!}
    
    <div>
        <div class="col-sm-3 button add">
            {!! link_to_route("admin.static-pages.create", "Добавить страницу", [], ["class" => "btn btn-success btn-block"]) !!}
        </div>
        <div class="clearfix"></div>
        <div id="sectionNav">
            <div class="clearfix"></div>
            <div id="contentList">
                <ul class="list-group">
                    @foreach($staticPages as $page)
                        <li class="list-group-item">
                            {!! $page->title !!}
                            <span class="right-list">
                                {!! link_to_route('admin.static-pages.edit', 'Редактировать', [$page->id]) !!}
                                | 
                                {!! link_to_route('admin.static-pages.delete', 'Удалить', [$page->id], ['class' => 'deleteCheck']) !!}
                            </span>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endSection