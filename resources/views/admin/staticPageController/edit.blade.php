@extends('admin.app')

@section('title', 'Редактирование страницы')

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace("body");
    </script>
@stop

@section('content')
    @if ($errors->any())
        @include('admin.particial.errors')
    @endif
    
    {!! Breadcrumbs::render('static-pages_edit', $page) !!}
    
    <div>
        <h3 class="text-center title">Редактировать страницу</h3>
    </div>
    {!! Form::model($page, ['method' => 'put', 'route' => ['admin.static-pages.update', $page->id], 'class' => 'form-horizontal']) !!}
        <div class="container-form">
            <div class="form-group">
                <label for="tag" class="col-sm-2 control-label">Тег:</label>
                <div class="col-sm-4">
                    {!! Form::text('tag', null, [
                                    'class' => 'form-control',
                                    'id' => 'tag', 
                                    'placeholder' => 'Имя'
                                    ]) 
                    !!}
                </div>
            </div>
            
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Заголовок:</label>
                <div class="col-sm-4">
                    {!! Form::text('title', null, [
                                    'class' => 'form-control',
                                    'id' => 'title', 
                                    'placeholder' => 'Имя'
                                    ]) 
                    !!}
                </div>
            </div>
            
            <div class="form-group">    
                <label for="body" class="col-sm-2 control-label">Содержимое:</label>
                <div class="col-sm-9">
                    {!! Form::textarea('body', $page->content) !!}
                </div>
            </div>
        </div>
        
        <div class="col-sm-offset-5 col-sm-2">
            {!! Form::submit("Сохранить", ["class" => "btn btn-block btn-primary"] ) !!}
        </div>
    {!! Form::close()!!}
@endSection