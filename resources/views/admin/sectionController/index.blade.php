@extends('admin.app')

@section('css')
    @parent
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/jquery-sortable.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('js/bootstrap.file-input.js') }}"></script>
    <script>
      $('#accordionSection').sortable({
        containerSelector: 'div#accordionSection',
        itemSelector: 'div.panel-default',
        placeholder: '<div class="panel panel-default placeholder"/>'
        });
    </script>
    <script>
        if($('.file-input-wrapper')){
            $('.file-inputs').bootstrapFileInput();
            $('.file-input-wrapper').show();
        }
    </script>
    <script>
        $('#modal').on('show.bs.modal', function (e) {
            var $img = $(e.relatedTarget).find('#img').val();
            $('.modal-body img').attr("src", $img);
        })
    </script>
@stop

@section('content')
    @if (Session::has('messages'))
        @include('admin.particial.success')
    @endif 
    
    {!! Breadcrumbs::render('sections') !!}
    
    <div>
        <div class="col-sm-3 button add">
            {!! link_to_route("section_new", "Добавить раздел", [], ["class" => "btn btn-success btn-block", "role" => "button"]) !!}
        </div>
        <div class="clearfix"></div>
        {!! Form::open(["route" => "postSections", "class" => "form-horizontal", 'files' => true]) !!}
            <div id="accordionSection" class="panel-group">
                @foreach($sections as $section)
                <div class="panel panel-default" id="panelItem">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <span class="glyphicon glyphicon-move" aria-hidden="true"></span> 
                            <input type="checkbox" name="chkbx-section-{!! $section->id !!}" value="">
                            <a href="#panel-{!! $section->id !!}" data-parent="#accordionSection" data-toggle="collapse"> {!! $section->text !!} </a>
                        </h4>
                    </div>
                    <div id="panel-{!! $section->id !!}" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group">
                                <input type="hidden" name="id[]" value="{!! $section->id !!}">    
                                <label for="text" class="col-sm-2 control-label">Название:</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="text[]" value="{!! $section->text !!}">
                                </div>
                            </div>
                            <div class="form-group"> 
                                <label for="route" class="col-sm-2 control-label">Путь:</label>
                                <div class="col-sm-6">
                                    <input type="route" class="form-control" name="route[]" value="{!! $section->route !!}">
                                </div>
                            </div>
                            <div class="form-group">   
                                <label class="col-sm-2 control-label">
                                    Выделить:
                                </label>
                                <div class="checkbox">
                                    <div class="col-sm-2">
                                        @if($section->select)
                                            <input type="checkbox" name="select-{!! $section->id !!}" value="1" checked>
                                        @else
                                            <input type="checkbox" name="select-{!! $section->id !!}" value="0">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image" class="col-sm-2 control-label">Изображение:</label>
                                <div class="col-sm-1 section-image">
                                    <a href="#modal" data-toggle="modal">
                                        @if($section->image)
                                            {!! HTML::image($section->image, $section->text, ["width" => "50"]) !!}
                                            {!! Form::hidden('img', $section->image, ['id' => 'img']) !!}
                                        @else 
                                            <span class="glyphicon glyphicon-picture"></span>
                                            {!! Form::hidden('img', '../img/not_image.jpg', ['id' => 'img']) !!}
                                        @endif
                                    </a>
                                </div>
                                <div class="col-sm-8">
                                    <a class="file-input-wrapper btn btn-default" style="display: none;">
                                        <span>Выбрать</span>
                                        {!! Form::file('image[]') !!}
                                    </a>
                                    <span class="file-input-name"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end panel -->
                @endforeach
            </div><!--end accordion -->
            <!--<div class="col-sm-4">-->
            <div class="form-group select">
                <div class="col-sm-1">
                    {!! Form::label("", "Действие", ["for" => "action"]) !!}
                </div>
                <div class="col-sm-2">
                    <select class="form-control selectpicker" name="action">
                        <option selected disabled>Выберите:</option>
                        <option value="update">Изменить</option>
                        <option value="delete">Удалить</option>
                    </select>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-offset-4 col-sm-2">
                {!! Form::submit("Сохранить", ["class" => "btn btn-block btn-primary", "disabled" => "disabled"] ) !!}
            </div>
            <div class="col-sm-2">
                {!! link_to_route("section_cancel", "Отмена", [], ["class" => "btn btn-danger btn-block", "role" => "button"]) !!}
            </div>
        {!! Form::close() !!}    
    </div>
    
    @include('admin.templates.modal')
    
@stop
