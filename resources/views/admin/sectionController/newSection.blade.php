@extends('admin.app')

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/bootstrap.file-input.js') }}"></script>
    <script>
        if($('.file-input-wrapper')){
            $('.file-inputs').bootstrapFileInput();
            $('.file-input-wrapper').show();
        }
    </script>
@stop

@section('content')
        @if ($errors->any())
            @include('admin.particial.errors')
        @endif
        
        {!! Breadcrumbs::render('section_new') !!}
        
        <div>
            <h3 class="text-center title">Добавить раздел</h3>
        </div>
        {!! Form::open(['route' => 'postSection_new', 'class' => 'form-horizontal', 'files' => true]) !!}
            <div class="container-form">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Имя раздела:</label>
                    <div class="col-sm-4">
                        {!! Form::text('name', null, ['class' => 'form-control',
                                'id' => 'name', 'placeholder' => 'Имя раздела']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="route" class="col-sm-2 control-label">Путь:</label>
                    <div class="col-sm-4">
                        {!! Form::text('route', null, ['class' => 'form-control',
                                'id' => 'route', 'placeholder' => 'Путь']) !!}
                    </div>
                </div>
                <div class="form-group">    
                    <label for="priority" class="col-sm-2 control-label">Приоритет:</label>
                    <div class="col-sm-2">
                        {!! Form::text('priority', null, ['class' => 'form-control',
                                'id' => 'priority', 'placeholder' => 'Приоритет']) !!}
                    </div>
                </div>
                <div class="form-group">   
                    <label for="select" class="col-sm-2 control-label">Выделить:</label>
                    <div class="checkbox">
                        <div class="col-sm-2">
                            {!! Form::checkbox('select', '0') !!}
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="image" class="col-sm-2 control-label">Изображение:</label>
                    <div class="col-sm-8">
                        <a class="file-input-wrapper btn btn-default ">
                            <span>Выбрать</span>
                            {!! Form::file('image') !!}
                        </a>
                        <span class="file-input-name"></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-offset-5 col-sm-2">
                {!! Form::submit("Добавить", ["class" => "btn btn-block btn-primary"] ) !!}
            </div>
        {!! Form::close() !!}
@stop
