<div id="message-info" class="alert alert-success alert-dismissible" role="alert"> 
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
    <span class="sr-only">Success:</span>
    <!--<span id="message"> -->
        @foreach( Session::get("messages") as $message )
            <div class="message-text">{!! $message !!}</div>
        @endforeach
    <!--</span>-->
</div>