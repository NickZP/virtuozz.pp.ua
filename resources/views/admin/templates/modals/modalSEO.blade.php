<div id="modal-seo" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal"></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <label>Введите ключевые слова через запятую:</label>
                <textarea rows="10" data-product=""></textarea>
                <div class='block-btn-save'>
                    <button class="btn btn-site" type="button" data-dismiss="modal">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</div>