<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="_token" content="{!! csrf_token() !!}" >
        <title>Строительные добавки - админка - @yield('title')</title>
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        @section('css')
            <link href="{{ elixir('css/admin-all.css') }}" rel="stylesheet" type="text/css">
        @show
        <script type="text/javascript" src="{{ asset('js/respond.js') }}"></script>
    </head>
    <body>
        <div class="container body">
            <header class="row">
                <div id="pageHeader">

                    <div id="login">
                            Здравствуйте, {{ Auth::user()->name }}
                            {!! link_to_route('logout', 'выйти', array(), array('class' => 'logout')) !!}
                    </div>
                </div>
                <nav class="navbar navbar-nav admin-menu">
                    <ul class="nav navbar-nav navbar-left">
                        <li class="{{ \App\Helpers\Helper::isCurrent(link_to_route('sections')) ? 'active' : '' }}">
                            {!! link_to_route('sections', 'разделы') !!}
                        </li>
                        <li class="{{ \App\Helpers\Helper::isCurrent(link_to_route('categories')) ? 'active' : '' }}">
                            {!! link_to_route('categories', 'категории') !!}
                        </li>
                        <li class="{{ \App\Helpers\Helper::isCurrent(link_to_route('products')) ? 'active' : '' }}">
                            {!! link_to_route('products', 'товары') !!}
                        </li>
                        @if(\Auth::user()->hasRole(['admin']))
                            <li class="{{ \App\Helpers\Helper::isCurrent(link_to_route('admin.users.index')) ? 'active' : '' }}">
                                {!! link_to_route('admin.users.index', 'Пользователи') !!}
                            </li>
                            <li class="{{ \App\Helpers\Helper::isCurrent(link_to_route('admin.editors.index')) ? 'active' : '' }}">
                                {!! link_to_route('admin.editors.index', 'Редакторы') !!}
                            </li>
                            <li class="{{ \App\Helpers\Helper::isCurrent(link_to_route('admin.static-pages.index')) ? 'active' : '' }}">
                                {!! link_to_route('admin.static-pages.index', 'Страницы') !!}
                            </li>
                            <li class="{{ \App\Helpers\Helper::isCurrent(link_to_route('admin.order.index')) ? 'active' : '' }}">
                                {!! link_to_route('admin.order.index', 'Заказы') !!}
                            </li>
                            <li class="{{ \App\Helpers\Helper::isCurrent(link_to_route('admin.seo.index')) ? 'active' : '' }}">
                                {!! link_to_route('admin.seo.index', 'SEO') !!}
                            </li>
                        @endif
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                    </ul>
                </nav>
            </header>

            <div class="row" id="content">
                @yield('content')
            </div>

            @section('scripts')
                <script type="text/javascript" src="{{ asset('js/all.js') }}"></script>
            @show
        </div>
    </body>
</html>