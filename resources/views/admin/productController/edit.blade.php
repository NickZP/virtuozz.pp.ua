@extends('admin.app')

@section('css')
    @parent
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/product.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/bootstrap.file-input.js') }}"></script>
    <script>
        CKEDITOR.replace("body");

        CKEDITOR.replace("desc-short");
        
        //jQuery('input[type=file]').bootstrapFileInput();
        if($('.file-input-wrapper')){
            $('.file-inputs').bootstrapFileInput();
            $('.file-input-wrapper').show();
        }
    </script>
@stop

@section('content')
    @if ($errors->any())
        @include('admin.particial.errors')
    @endif
    
    {!! Breadcrumbs::render('product_edit', $product) !!}
    
    <div>
        <h3 class="text-center title">Редактировать товар</h3>
    </div>
    {!! Form::model($product, ['route' => 'postProduct_edit', 'class' => 'form-horizontal', 'files' => true]) !!}
        <div class="container-form">
            <div class="form-group">
                <label for="ProductName" class="col-sm-2 control-label">Имя товара:</label>
                <div class="col-sm-8">
                    {!! Form::text('name', null, ['class' => 'form-control',
                            'id' => 'ProductName', 'placeholder' => 'Имя товара']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="ProductShortName" class="col-sm-2 control-label">Краткое имя товара:</label>
                <div class="col-sm-8">
                    {!! Form::text('shortname', null, ['class' => 'form-control',
                            'id' => 'ProductShortName', 'placeholder' => 'Краткое имя товара']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="ProductRoute" class="col-sm-2 control-label">Путь:</label>
                <div class="col-sm-8">
                    {!! Form::text('route', null, ['class' => 'form-control',
                            'id' => 'ProductRoute', 'placeholder' => 'Маршрут для товара']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#variantsales" aria-expanded="false" aria-controls="variantsales">
                        Варианты продаж
                    </button>
                    <div class="collapse" id="variantsales">
                        <div class="variantsales">
                            @foreach ($variantsales as $sale)
                                @include('admin.productController.particial.DBVariantSale')
                            @endforeach
                            @if (!empty(\Session::get("_old_input")['measure']))
                                @for ($i = 0; $i < (count($sessions['measure'])); $i++)
                                    @include('admin.productController.particial.sessionVariantSale')
                                @endfor
                            @endif
                        </div>
                        <div class="grp-variantSale">
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <input type="radio" name="sale" class="form-control">
                                </div>
                                <div class="col-sm-3">
                                <input type="text" data-name="num_measure[]" placeholder="количество" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" data-name="measure[]">
                                        @foreach($measures as $measure)
                                            <option>{!! $measure !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                <input type="text" data-name="price[]" placeholder="цена" class="form-control">
                                </div>
                                <button type="button" class="btn btn-success btn-addVariantSale">
                                    Добавить
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">    
                <label for="Sections[]" class="col-sm-2 control-label">Раздел:</label>
                <div class="col-sm-8">
                    <select name="sections[]" class="form-control selectpicker" data-live-search="true" multiple>
                        @foreach($sections as $section)
                            <optgroup label="{!! $section->text !!}">
                                @foreach($section->categories as $category)
                                    @if(\Session::getOldInput('sections'))
                                        @if(in_array($category->id, \Session::getOldInput('sections')))
                                            <option value="{!! $category->id !!}" selected>{!! $category->text !!}</option>
                                        @else
                                            <option value="{!! $category->id !!}">{!! $category->text !!}</option>
                                        @endif
                                    @else
                                        @if(in_array($category->id, $productCategories))
                                            <option value="{!! $category->id !!}" selected>{!! $category->text !!}</option>
                                        @else
                                            <option value="{!! $category->id !!}">{!! $category->text !!}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="body" class="col-sm-2 control-label">Описание:</label>
                <div class="col-sm-8">
                    {!! Form::textarea('body', $product->productInfo->info) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="desc-short" class="col-sm-2 control-label">Краткое описание:</label>
                <div class="col-sm-8">
                    {!! Form::textarea('desc-short', $product->productInfo->desc_short) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Изображение:</label>
                <div class="col-sm-8">
                    <a href="#modal-product-edit" data-toggle="modal">
                        {!! HTML::image($product->image, $product->shortname, ["width" => "50"]) !!}
                    </a>
                </div>
            </div>
            <div class="form-group">
                <label for="image" class="col-sm-2 control-label">Изменить изображение:</label>
                <div class="col-sm-8">
                    <a class="file-input-wrapper btn btn-default" style="display: none;">
                        <span>Выбрать</span>
                        {!! Form::file('image') !!}
                    </a>
                    <span class="file-input-name"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-5 col-sm-2">
                    {!! Form::hidden('id', $product->id) !!}
                    {!! Form::submit("Редактировать", ["class" => "btn btn-block btn-primary"] ) !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    
    <!--modal window -->
    <div id="modal-product-edit" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <!--<div class="modal-header">
                <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal"></button>
            </div>-->
            <div class="modal-body">
             {!! HTML::image($product->image, $product->shortname, ["class" => "img-responsive", "width" => "500px"]) !!}
             
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
    <!--end modal window -->
@stop