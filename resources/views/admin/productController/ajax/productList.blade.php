<ul class="list-group">
  @foreach($products as $product)
    <li class="list-group-item">
        {!! $product->shortname !!}
        <span class="product-list">
            {!! link_to_route('product_edit', 'Редактировать', [$product->id]) !!}
            | 
            {!! link_to_route('product_delete', 'Удалить', [$product->id], ['class' => 'deleteCheck']) !!}
        </span>
    </li>
  @endforeach
</ul>
<div class="text-center">{!! $products->render() !!}</div>