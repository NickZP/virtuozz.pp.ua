@extends('admin.app')

@section('css')
    @parent
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('js/i18n/defaults-ru_RU.js') }}"></script> 
@stop

@section('content')
    @if (Session::has('messages'))
        @include('admin.particial.success')
    @endif
    
    {!! Breadcrumbs::render('products') !!}
    
    <div class="col-sm-3 button add">
        {!! link_to_route("product_new", "Добавить товар", null, ["class" => "btn btn-success btn-block"]) !!}
    </div>
    <div class="clearfix"></div>

    <div id="sectionNav">
        <div id="categoryNav" class="col-sm-3 button add" data-url="/admin/products">
            <select class="form-control selectpicker" data-live-search="true">
                <optgroup label="{!! $text['all'] !!}"><option>{!! $text['all'] !!}</option></optgroup>
                @foreach($sections as $section)
                    <optgroup label="{!! $section->text !!}">
                        @if(empty($section->categories->id))
                            @foreach($section->categories as $category)
                                <option value="{!! $category->id !!}">{!! $category->text !!}</option>
                            @endforeach
                        @ifelse
                        @endif
                    </optgroup>
                @endforeach
            </select>
        </div>
        <div class="clearfix"></div>
        <div id="contentList" data-url="{!! \Request::segment(2) !!}">
            @include('admin.productController.ajax.productList')
        </div>
        
    </div
@stop