<div class="grp-variantSale">
    <div class="form-group">
        <div class="col-sm-1">
            <input type="radio" name="sale" class="form-control">
        </div>
          <div class="col-sm-3">
            {!! Form::text('num_measure['.$i.']', $sessions['num_measure'], ['class' => 'form-control',
                        'placeholder' => 'количество']) !!}
          </div>
        <div class="col-sm-2">
            {!! Form::select('measure['.$i.']', $measures, $sessions['measure'], ['class' => 'form-control']) !!}
        </div>
          <div class="col-sm-3">
            {!! Form::text('price['.$i.']', $sessions['price'], ['class' => 'form-control',
                        'placeholder' => 'цена']) !!}
          </div>

        <button type="button" class="btn btn-danger btn-addVariantSale">
            Удалить
        </button>
    </div>
</div>
