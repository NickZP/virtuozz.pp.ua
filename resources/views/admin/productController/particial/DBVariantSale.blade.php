<div class="grp-variantSale">
    <div class="form-group">
        <div class="col-sm-1">
            @if ($sale->default)
                <input type="radio" name="sale" class="form-control" checked="">
            @else
                <input type="radio" name="sale" class="form-control">
            @endif
        </div>
          <div class="col-sm-3">
            <?php
                $measurement_arr = explode(" ", $sale->measurement);
                $measurement = array_pop($measurement_arr);
                $measurement_str = implode(" ", $measurement_arr);
            ?>
            {!! Form::text('dbnum_measure['.$sale->id.']', $measurement_str, ['class' => 'form-control',
                        'placeholder' => 'количество']) !!}
          </div>
        <div class="col-sm-2">
            {!! Form::select('dbmeasure['.$sale->id.']', $measures, $measurement, ['class' => 'form-control']) !!}
        </div>
          <div class="col-sm-3">
            {!! Form::text('dbprice['.$sale->id.']', $sale->price, ['class' => 'form-control',
                        'placeholder' => 'цена']) !!}
          </div>

        <button type="button" class="btn btn-danger btn-addVariantSale">
            Удалить
        </button>
    </div>
</div>
