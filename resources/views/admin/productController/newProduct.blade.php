@extends('admin.app')

@section('css')
    @parent
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
    
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/product.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/bootstrap.file-input.js') }}"></script>
    <script>
        CKEDITOR.replace("body");
        CKEDITOR.replace("desc-short");
        $('.file-inputs').bootstrapFileInput();
        //jQuery('input[type=file]').bootstrapFileInput();
    </script>
@stop

@section('content')
    @if ($errors->any())
        @include('admin.particial.errors')
    @endif
    
    {!! Breadcrumbs::render('product_new') !!}
    
    <div>
        <h3 class="text-center title">Добавить товар</h3>
    </div>
    {!! Form::open(['route' => 'postProduct_new', 'class' => 'form-horizontal', 'files' => true]) !!}
        <div class="container-form">
            <div class="form-group">
                <label for="ProductName" class="col-sm-2 control-label">Имя товара:</label>
                <div class="col-sm-8">
                    {!! Form::text('name', null, ['class' => 'form-control',
                            'id' => 'ProductName', 'placeholder' => 'Имя товара']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="ProductShortName" class="col-sm-2 control-label">Краткое имя товара:</label>
                <div class="col-sm-8">
                    {!! Form::text('shortname', null, ['class' => 'form-control',
                            'id' => 'ProductShortName', 'placeholder' => 'Краткое имя товара']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="ProductRoute" class="col-sm-2 control-label">Путь:</label>
                <div class="col-sm-8">
                    {!! Form::text('route', null, ['class' => 'form-control',
                            'id' => 'ProductRoute', 'placeholder' => 'Маршрут для товара']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#variantsales" aria-expanded="false" aria-controls="variantsales">
                    Варианты продаж
                </button>
                <div class="collapse" id="variantsales">
                    <div class="variantsales">
                        @if (!empty(\Session::get("_old_input")['measure']))
                            @for ($i = 0; $i < (count($sessions['measure'])); $i++)
                                @include('admin.productController.particial.sessionVariantSale')
                            @endfor
                        @endif
                    </div>
                    <div class="grp-variantSale">
                        <div class="form-group">
                            <div class="col-sm-1">
                                <input type="radio" name="sale" class="form-control">
                            </div>
                            <div class="col-sm-3">
                            <input type="text" data-name="num_measure[]" placeholder="количество" class="form-control">
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control" data-name="measure[]">
                                    @foreach($measures as $measure)
                                        <option>{!! $measure !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3">
                            <input type="text" data-name="price[]" placeholder="цена" class="form-control">
                            </div>
                            <button type="button" class="btn btn-success btn-addVariantSale">
                                Добавить
                            </button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="form-group">    
                <label for="Sections" class="col-sm-2 control-label">Раздел:</label>
                <div class="col-sm-8">
                    <select name="sections[]" class="form-control selectpicker" data-live-search="true" multiple>
                        @foreach($sections as $section)
                            <optgroup label="{!! $section->text !!}">
                                @foreach($section->categories as $category)
                                    @if(\Session::getOldInput('sections'))
                                        @if(in_array($category->id, \Session::getOldInput('sections')))
                                            <option value="{!! $category->id !!}" selected>{!! $category->text !!}</option>
                                        @else
                                            <option value="{!! $category->id !!}">{!! $category->text !!}</option>
                                        @endif
                                    @else
                                        <option value="{!! $category->id !!}">{!! $category->text !!}</option>
                                    @endif
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="body" class="col-sm-2 control-label">Описание:</label>
                <div class="col-sm-8">
                    {!! Form::textarea('body', null) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="desc-short" class="col-sm-2 control-label">Краткое описание:</label>
                <div class="col-sm-8">
                    {!! Form::textarea('desc-short', null) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="image" class="col-sm-2 control-label">Изображение:</label>
                <div class="col-sm-8">
                    <a class="file-input-wrapper btn btn-default ">
                        <span>Выбрать</span>
                        {!! Form::file('image') !!}
                    </a>
                    <span class="file-input-name"></span>
                </div>
            </div>
        </div>
        <div class="col-sm-offset-5 col-sm-2">
            {!! Form::submit("Добавить", ["class" => "btn btn-block btn-primary"] ) !!}
        </div>
    {!! Form::close() !!}
@stop