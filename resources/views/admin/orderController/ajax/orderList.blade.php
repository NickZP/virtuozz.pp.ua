<ul class="list-group">
    @foreach($orders as $order)
        <li class="list-group-item orders">
            <span class="number">{{ $order->id }}</span>
            <span class="date">{{ $order->created_at }}</span>
            <span class="price">{{ $order->history->currency_price }}</span>
            <span class="user">
                @if(!empty($order->user->id))
                    {{ $order->user->name }}
                @else
                    Неизвестный
                @endif
            </span>
            <span class="state state-{{ $order->state->name }}">{{ $order->state->display_name }}</span>
            <span class="right-list">
                {!! link_to_route('admin.order.view', 'Просмотреть', [$order->id]) !!}
            </span>
        </li>
    @endforeach
</ul>
<div class="text-center">{!! $orders->render() !!}</div>