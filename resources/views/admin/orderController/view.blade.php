@extends('admin.app')

@section('title', 'Просмотр заказа')

@section('content')
    
    {!! Breadcrumbs::render('orders_view', $order) !!}
    
    <div>
        <h3 class="text-center title">Заказ № {{ $order->id }}</h3>
    </div>
    
    <a href="#">История состояний заказа</a>
    <div>Информация о заказе</div>
    
    {!! Form::open(['route' => ['admin.order.state_change', $order->id], 'class' => 'form-horizontal']) !!}
        <div class="container-form">
            <div class="form-group">
                <label for="status" class="col-sm-2 control-label">Статус заказа:</label>
                <div class="col-sm-4">
                    {!! Form::select('status', $state, $order->state()->lists('id')->toArray(), ['class' => 'form-control']) !!}
                </div>
                
                <div class="col-sm-2">
                    {!! Form::submit("Сохранить", ["class" => "btn btn-block btn-primary"] ) !!}
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-8">
                    {!! Form::textarea('comment', $order->orderStateHistories()->get()->last()->comment, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    
    <div class="row">
        <div class="col-md-12">
            <div class="order-tabs">
                 
                <ul role="tablist" class="nav nav-tabs" id="orderlist"> 
                    <li 
                        role="presentation" 
                        class=""
                    >
                        <a 
                            aria-controls="client" 
                            data-toggle="tab" 
                            role="tab" 
                            id="client-tab" 
                            href="#client" 
                            aria-expanded="false"
                         >
                            Клиент
                        </a>
                    </li> 
                    <li 
                        class="active" 
                        role="presentation" 
                    >
                        <a 
                            aria-controls="order" 
                            data-toggle="tab" 
                            id="order-tab" 
                            role="tab" 
                            href="#order" 
                            aria-expanded="true"
                         >
                            Заказ
                        </a>
                    </li> 
                    <li 
                        role="presentation" 
                        class=""
                    >
                        <a 
                            aria-controls="orderStateHistory" 
                            data-toggle="tab" 
                            id="orderStateHistory-tab" 
                            role="tab" 
                            href="#orderStateHistory" 
                            aria-expanded="false"
                        >
                            История состояний заказа
                        </a>
                    </li>

                </ul> 
                
                <div class="tab-content" id="myTabContent"> 
                    <div 
                        aria-labelledby="client-tab" 
                        id="client" 
                        class="row tab-pane fade" 
                        role="tabpanel"
                    > 
                        <div class="col-sm-2 text-label">
                            <div>
                                Имя:                            
                            </div> 
                            <div>
                                Телефон:
                            </div>
                            <div>
                                Email:
                            </div>
                        </div>
                        <div class="col-sm-10 text-desc">
                            <div>
                                {{ $order->history->name}}                            
                            </div> 
                            <div>
                                {{ $order->history->phone}}   
                            </div>
                            <div>
                               {{ $order->history->email}}   
                            </div>
                        </div>
                    </div> 
                    <div 
                        aria-labelledby="order-tab" 
                        id="order" 
                        class="tab-pane fade active in" 
                        role="tabpanel"
                    > 
                        @foreach ($order->history->history as $product)
                            <div class="row">
                                <div class="col-sm-1">
                                    <img src="{{ $product->img }}" alt="{{ $product->title }}" width="50px">
                                </div>
                                <div class="col-sm-7">
                                    <a href="{{ $product->url }}" target="_blank">{{ $product->title }}</a>
                                </div>
                                <div class="col-sm-1">
                                    <span class="measure">{{ $product->measurement }}</span>    
                                </div>
                                <div class="col-sm-3">
                                    <div class="right">
                                        <span class="amount">{{ $product->amount }}</span> 
                                        <span class="bold">x</span>
                                        <span>{{ $product->price }}грн</span>  
                                        <span class="bold">=</span>  
                                        <span class="sum">{{ $product->amount * $product->price }}</span>грн
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        
                        <div class="row">
                            <div class="col-sm-offset-8 col-sm-4">
                                <div class="cart-total">
                                    <span class="cart-total-title">Итого:</span>
                                    <span class="cart-total-currency">
                                        <span class="cart-cost">{!! $order->history->price !!}</span>
                                        <span class="cart-currency">грн</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                    </div> 
                    <div 
                        aria-labelledby="orderStateHistory-tab" 
                        id="orderStateHistory" 
                        class="tab-pane fade" 
                        role="tabpanel"
                    > 
                        <p>
                            История состояний заказа
                        </p> 
                    </div>
                </div> 
                
            </div>
        </div>
    </div>
           
    <!--
    <div>О Товарах</div>
    <form action="">
        <div>Статус</div>
        <div>Комментарий</div>
    </form> 
    --> 
    
@endSection