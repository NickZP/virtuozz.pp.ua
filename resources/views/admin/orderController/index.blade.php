@extends('admin.app')

@section('title', 'Заказы')

@section('content')
    
    @if (Session::has('messages'))
        @include('admin.particial.session')
    @endif
    
    {!! Breadcrumbs::render('orders') !!}
    
    <div>
        <div class="clearfix"></div>
        <div class="col-sm-3 search">
            {!! Form::text('search', null, [ 
                                        'class' => 'form-control search-ajax', 
                                        'placeholder' => 'Поиск'
                                        ])
            !!}
        </div>
        <div class="clearfix"></div>
        <div id="sectionNav">
            <div class="clearfix"></div>
            <div id="contentList" data-url="{!! \Request::segment(2) !!}">
                @include('admin.orderController.ajax.orderList')
            </div>
        </div>
    </div>
@endSection