(function($){
  "use strict";
  $(document).ready(function(){
       $(document).on("click", '.btn-addVariantSale', function(){
           if ($(this).hasClass("btn-success")){
               var template = $('.grp-variantSale').last().clone();
               
                var num_measure = $(template).find(":text").first();
                var price = $(template).find(":text").last();
                //var sale = $(template).find(":radio").first();
                var measure = $(template).find("select").first();
                
                $(num_measure).attr("name", $(num_measure).data("name"));
                $(price).attr("name", $(price).data("name"));
                //$(sale).attr("name", $(sale).data("name"));
                $(measure).attr("name", $(measure).data("name"));
                $(template).find("button").text("Удалить").removeClass('btn-success').addClass('btn-danger');
               
               $(template).appendTo('.variantsales');
               //$(this).text("Удалить");
               //$(this).removeClass('btn-success').addClass('btn-danger');
           }
           else{
               $(this).parents('.grp-variantSale').remove();
           }
       });
       
       $(document).on("click", "input[type='radio']", function(){
           $("input[type='radio']").each(function(index){
               $(this).val(index);
           });
       });
  });
})(jQuery);


