$(document).ready(function(){
    var token = $('meta[name="_token"]').attr('content');
    var grn = ' грн';
    
    //page admin categories
    $('#categoryNav select').on('change', function(){
        getPosts(0);
    });
    
    /*
    * отключение стандартной пагинации и обработка через AJAX
    */
    if(!$('div').hasClass('paginate-post')){
        $(window).on('hashchange', function() {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    //getPosts(page);
                }
            }
        });
    
        $(document).on('click', '.pagination a', function (e) {
            getPosts($(this).attr('href').split('page=')[1]);
            e.preventDefault();
        });
    }

    $(document).on('click', '.sort-by', function() {
        var $sort_by = $('.sorter .sort-by').data('orderby');

        if ( $sort_by == 'asc'){
            $('.sort-by').data('orderby', 'desc');
            $('.sort-by').removeClass('glyphicon-arrow-up').addClass('glyphicon-arrow-down');
        }
        else{
            $('.sort-by').data('orderby', 'asc');
            $('.sort-by').removeClass('glyphicon-arrow-down').addClass('glyphicon-arrow-up');
        }

        getProducts(1);
    });

    $(document).on('click', '.view .glyphicon', function() {
        var $view = $(this).data('view');
        if ( $view == 'th'){
            $('.view .glyphicon-list').removeClass('active');
        }
        else{
            $('.view .glyphicon-th').removeClass('active');
        }
        $(this).addClass('active');

        getProducts(1);
    });

    /*
    * отключение get пагинации в категории при сортировке продуктов
    * работает через (ajax)
    */
    $(document).on('click', '.pagination a', function (e) {
        if( $('div.pagination-block').hasClass('ajax')) {
            e.preventDefault();
            getProducts($(this).attr('href').split('page=')[1]);
        }
    });

    function getProducts(page){
        var data = dataSort();
        data['page'] = page;
        $.ajax({
            url: "/products-sort-ajax",
            type: 'post',
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
            data:  data,
            beforeSend: function(){
                $('.content-list').html('<img width="10%" src="/img/loading.gif" style="display: block; margin: 0 auto;">');
            },
            success: function(html){
                $('.content-list').html(html);
            },
            error: function(html){
                console.log("error:" + html);
                return false;
            }
        });
    }

    function getPosts(page) {
        var data = {};
        data['_token'] = token;
        data['page'] = page;
        data['search'] = '';
        if($('input[name="search"]').hasClass('search-ajax')){
            data['search'] = $('.search-ajax').val();
        }
        else{
            data['search'] = $('#categoryNav select').find(':selected').text();
        }
        var url = $('#contentList').data("url") + "?dummy="+Math.random();
        $.post(url, data)
                .done(function (data) {
                    $('#contentList').html(data);
                    location.hash = page;
                })
                .fail(function (data) {
                    console.log('failed');
                });
    }
    
    //AJAX поиск только в админке
    if(!$('#content').hasClass('main-container')){
        $('.search-ajax').on("keyup", function(){
            getPosts(0);
        });
    }
    /*end pagination*/

    //запрос при удалении записей
    $(document).on('click', '.deleteCheck', function (){
        return confirm('Вы действительно хотите удалить эту запись');
    });

    //сортировка продуктов в категории
    if($('div').hasClass('sorter')){
        $('select[name="sort"]').on('change', function(){
            getProducts(1);
        });
    }

    function dataSort(){
        var data = {};
        data['_token']      = token;
        data['sort-name']   = $('select[name="sort"]').val();
        data['sort-orderby'] = $('.sort .glyphicon').data("orderby");
        data['view']        = $('.view .glyphicon.active').data("view");
        data['uri']         = $('.content-list').data("url");

        return data;
    }

    //неактивная кнопка "сохранить" в разделе section
    if($('div').is('#accordionSection')){
        $('.selectpicker').on('change', function(){
            $('.btn-primary').prop("disabled", false);
        });
    }
    
    //меню категории
    if($('a').hasClass('list-group-item-success')){
        $('.list-group-item-success').on('click', function(e){
            if($(e.target).hasClass('glyphicon')){
                $('.list-group-item-success').removeAttr("data-toggle");
                $(this).attr("data-toggle", "collapse");
                
                $('.list-group-item-success .glyphicon').each(function(){
                    if($(this).parent().attr("data-toggle") !== undefined){
                        $(this).toggleClass('glyphicon-chevron-up glyphicon-chevron-down');
                        $(this).parent().find('.section-text').addClass('active');
                    }
                    else{
                        $(this).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                        $(this).parent().find('.section-text').removeClass('active');
                    }                    
                });                
            }
            else{
                $(this).removeAttr("data-toggle");
                if($(e.target).hasClass('section-text')){
                    $(location).attr('href', '/' + $(e.target).data('url'));
                }
            }
        });        
    }
    
    //Выделение нужного пункта меню при загрузке страницы
    if($('a').hasClass('list-group-item')){

        $('.collapse > .list-group-item').on('click', function(e){
            $(location).attr('href', $(this).data('url'));
        });
        
        var href = window.location.href;
        var arr = href.split('/');
        var $section = '';
        if (arr.length >= 5){
            $('#MainMenu .section-text').each(function(){
                if($(this).data('url') === arr[3]){
                    $(this).next().trigger('click');
                    $(this).parent().next().find('.list-group-item').each(function(){
                        var pos = arr[4].indexOf('?');
                        if(-1 < pos){
                            arr[4] = arr[4].substring(0, pos);
                        }
                        if($(this).data('url') === '/'+arr[3] + '/' + arr[4]){
                            $(this).addClass('act');
                        }
                    });
                }
            })
        }
    }
    
    $(".dropdown-menu li a").click(function(){
        $(this).parents(".btn-group").find('.selection').text($(this).text());
        $(this).parents(".btn-group").find('.selection').val($(this).text());
    });
    
    //
    //if($('div').hasClass('amount')){
        $(document).on('click', '.amount > .glyphicon', function(e){
            
           var $value = $(this).parent().find('.number').val();
           
           if($(e.target).hasClass('glyphicon-minus')){
               if($value >= 2){
                    $value--;
                    $(this).parent().find('.number').val($value);
                    $(this).parent().find('.number').trigger('change');
               }
           } 
           else{
               $value++;
               $(this).parent().find('.number').val($value);
               $(this).parent().find('.number').trigger('change');
           }
        });
    //}
    
    //корзина(покупка)
    //if($('div').hasClass('sale-line')){
        //новый товар
        $(document).on('click', '.sale-line .btn-buy', function(e){
            var item_id = parseInt($(this).data('id'));
            var measurement = $(this).parents('.sale-line').find('.measurement').text();
            var price = $(this).parents('.sale-line').find('.price').text();
            var amount = parseInt($(this).parents('.sale-line').find('.number').val());
            if($(this).parents().is('.modal-dialog')){
                var img = $(this).parents('.modal-dialog').find('.img-responsive').attr('src');
                var title = $(this).parents('.modal-dialog').find('.img-responsive').attr('alt');
                var url = $(this).parents('.modal-dialog').find('.modal-title').data("url");
                console.log('url:' + url);
            }
            else{
                //var img = $(this).parents('#content').find('.img-responsive').attr('src');
                var img = $(this).parents('#content').find('.img-responsive').data('zoo-image');
                var title = $(this).parents('.row').find('.product-name').text();
                var url = $(this).parents('.row').find('.product-name').data("url");
            }
            
            //console.log("id: " + item_id + " measurement: " + measurement +" price: " + price + " amount: " + amount +" image: " + img + " title: " + title);
            
            //узнаём есть ли такой товар в куках
            order = $.cookie('basket');
            !order ? order=[]: order = JSON.parse(order);
            if(order.length==0){
                order.push({'item_id': item_id, 'measurement':measurement, 'price':price, 'amount':amount, 'img':img, 'title':title, 'url':url});//добавляем объект к пустому массиву
            }
            else{
                var flag = false; //флаг, который указывает, что такого товара в корзине нет
                for(var i=0; i<order.length; i++) //перебираем массив в поисках наличия товара в корзине
                {
                    if(order[i].item_id == item_id && order[i].measurement == measurement){
                        order[i].amount += amount; //если товар уже в корзине, то добавляем +1 к количеству (amount)
                        flag=true; //поднимаем флаг, что такой товар есть и с ним делать ничего не нужно
                    }
                }
                if(!flag){
                    order.push({'item_id': item_id, 'measurement':measurement, 'price':price, 'amount':amount, 'img':img, 'title':title, 'url':url});
                }
            }
            
            successStack();
            $.cookie('basket', JSON.stringify(order), { path: '/' }); // переделываем массив с объектами в строку и сохраняем в куки
            count_order();
        });
    //}
    
    //оформление заказа
    if($('div').hasClass('zakaz')){
        var val = "";
        $('.number').each(function(){
            val += $(this).val() + " ";
        });
        
        $(document).on('change', '.number', function(){
            
          var value = $(this).val();
          var item_id = $(this).parents('tr').find('.id').val();
          var measurement = $(this).parents('tr').find('.measurement').text();          

          if(value.match(/[^0-9]/g) || value<=0){
                 $(this).val('1');
          }          
          
          set_amount(item_id, measurement, value);
          
          $('.zakaz').addClass('active');
          
          $.get('/basket' + "?dummy="+Math.random(), function(){
          })
                .done(function (data) {
                    $('.zakaz').html(data);
                })
                .complete(function(){
                    $('.zakaz').removeClass('active');
                })
                .fail(function (data) {
                    console.log('failed');
                });
        });
        
        $(document).on('click', '.del_order', function(){
            var string = $(this).parent().parent();
            var item_id = $(this).parents('tr').find('.id').val(); //получаем id товара
            var measurement = $(this).parents('tr').find('.measurement').text();
            
            string.remove();// удаляем строку
            
            del_item(item_id, measurement); //удаляем элемент из куки
            count_order(); //обновляем корзину
            all_order=$('tr'); //получаем все строки таблицы
            location.reload();
        });
    }
    
    //вывод заказа в корзине
    if($('div').hasClass('cart')){
        $('.cart-info .orders').on('mouseover', function(){
           if(!$('.cart-content').hasClass('active')){
               $('.cart-content').addClass('active');
               create_cart();
           }
        });
        
        $('.cart-content').on('mouseleave', function(){
           if($('.cart-content').hasClass('active')){
               $('.cart-content').removeClass('active');
           }
        });
    }
    
    //удаление элемента из корзины
    $(document).on('click', '.delete', function(e){
        e.preventDefault();
        var $div = $(this).parents('.cart-item');
        var item_id = $($div).data("id"); //получаем id товара
        var measurement = $($div).data("measurement");
        del_item(item_id, measurement);
        $($div).remove();
        if($('div').hasClass('zakaz')){
            location.reload();
        }
        else{
            var order = JSON.parse($.cookie('basket'));
            
            if(order.length == 0){
                createCartEmpty();
            }
            else{
                var total = 0
                for(var i=0; i<order.length; i++)
                {
                    total += order[i].price * order[i].amount;
                }
                
                $('.cart-content .total').text(total.toFixed(2) + grn); 
            }
            
            count_order();
        }
        
    });
    
    //вызов окна при нажатии "купить"  на продукте
    if($('div').hasClass('product-buy') || $('div').hasClass('product-view-list')){
        $('#modal-buy-product').on('show.bs.modal', function (e) {
            $('.variants').empty();
            var $img_src = $(e.relatedTarget).parents('.product-buy').find('img').attr("src");
            var $img_alt = $(e.relatedTarget).parents('.product-buy').find('img').attr("alt");
            var $text = $.trim($(e.relatedTarget).parents('.product-buy').find('.text').text());
            var $url = $(e.relatedTarget).data("url");
            var data = {};
            $('.modal-footer .btn.about').attr("href", $('.modal-footer .btn.about').data("url") + '/' + $url);
            $('.modal-body img').attr("src", $img_src);
            $('.modal-body img').attr("alt", $img_alt);
            $('.modal-title').text($text);
            $('.modal-title').data('url', $(e.relatedTarget).parents('.product-buy').find('.image').attr("href"));

            data['id'] = $(e.relatedTarget).data("id");
            $.ajax({
                url: "/product-ajax",
                type: 'post',
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                data:  data,
                beforeSend: function(){
                    $('.variants').html('<img width="20%" src="/img/loading.gif">');
                },
                success: function(html){
                    //$('.variants').append(html);
                    $('.variants').html(html);
                },
                error: function(html){
                    console.log("error:" + html);
                    return false;
                }
            });   
        });
    }
    
    if($('header').hasClass('top-header')){
        count_order();
    }
     
});

function del_item(item_id, measurement){
    var order = JSON.parse($.cookie('basket'));//получаем массив с объектами из куки
    for(var i=0; i<order.length; i++)
    {
        if(order[i].item_id==item_id && order[i].measurement == measurement)
        {
                order.splice(i,1); //удаляем из массива объект
        }
    }
    $.cookie('basket', JSON.stringify(order), { path: '/' });//сохраняем объект в куки
}

function count_order(){
    var order = $.cookie('basket'); //получаем куки
    order ? order = JSON.parse(order) : order = []; //если заказ есть, то куки переделываем в массив с объектами
    var count = 0; // количество товаров
    if(order.length > 0)
    {
        for(var i=0; i<order.length; i++)
        {
            count = count + parseInt(order[i].amount);
        }
    }
    $('.count_order').html(count);// отображаем количество товаров корзине.
}

function set_amount(item_id, measurement, amount)
{
    var order=JSON.parse($.cookie('basket')); //получаем куки и переделываем в массив с объектами
    for(var i=0; i<order.length; i++) //перебераем весь массив с объектами
    {
        if(order[i].item_id == item_id && order[i].measurement == measurement) //ищем нужный id
        {
            order[i].amount = amount; // устанавливаем количество товара
        }
    }

    $.cookie('basket', JSON.stringify(order), { path: '/' }); // сохраняем все в куки
    count_order(); //не забываем обновлять количество товаров в корзине
}

function create_cart(){
    $(".cart-item").remove();
    var orders = $.cookie('basket');
    var total_price = 0;
    var grn = ' грн';
    orders ? orders = JSON.parse(orders) : orders = [];
    if(orders.length){
        for(var i = 0; i < orders.length; i++){
            var $div = '<div class="cart-item" data-id="' + orders[i].item_id + '" data-measurement="' + orders[i].measurement + '">';
            var price = Number((orders[i].amount * orders[i].price).toFixed(2));
            total_price += price;
            
            $div += '<div class="left-info"><span class="cart-img"><img src="' + orders[i].img +'"></span></div>';
            $div += '<div class="center-info">';
            $div += '<a href="' + orders[i].url +'" class="cart-name">' + orders[i].title + '</a><br>';
            $div += '<span class="quantity">' + orders[i].amount + ' <em class="spr">x</em></span>';
            $div += '<span class="cart-price">' + orders[i].price + grn + ' (' + orders[i].measurement + ') = ' + '</span>';
            $div += '<span class="cart-sum">' + price + grn + '</span>';
            $div += '</div>';
            $div += '<a class="delete" href="#"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></div>';
            $('.cart-bottom').show();
            $($div).insertBefore('.cart-bottom');
        }
    }
    else{
        createCartEmpty();
    }

    $('.cart-bottom .total span').text(total_price.toFixed(2) + grn);
}

//показывать сообщение о дабавлении продукта в корзину
function successStack(){
    $('.messageStackSuccess').show().delay(3000).slideUp(300);
}

//создание надписи "корзина пуста" в корзине
function createCartEmpty(){
    var $div = '<div class="cart-item"><span>Корзина пуста</span></div>';
    $('.cart-bottom').hide();
    $($div).insertBefore('.cart-bottom');
}

function ajaxpost(url, data){
    $.ajax({
        url: url,
        type: 'post',
        dataType: "json",
        headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},//,'Content-Type': "application/json"},
        data:  data,//, "X-CSRF-TOKEN": token},
        success: function(html){
            window.location.href = '/admin/categories';
            //return html;
        },
        error: function(html){
            console.log("error:" + html);
            
            return false;
        } 
    });    
}


