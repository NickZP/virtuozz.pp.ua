var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    //mix.sass('app.scss');
    elixir.config.sourcemaps = false;
    
    mix.styles([
        'bootstrap.css',
        'main.css'
    ], null, 'public/css');
    
    //для админки
    mix.styles([
        'bootstrap.css',
        'main.css',
        'admin-custom.css'
    ], 'public/css/admin-all.css', 'public/css');
    
    mix.scripts([
        'jquery-latest.min.js',
        'jquery.cookie.js',
        'bootstrap.js',
        'main.js',
    ], null, 'public/js');
    
    
    mix.version([
        'public/css/all.css', 
        'public/css/admin-all.css',
        'public/js/all.js'
    ]);
    
    
    mix.copy('public/fonts', 'public/build/fonts');
});
