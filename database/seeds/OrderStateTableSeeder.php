<?php

use Illuminate\Database\Seeder;

use App\Models\OrderState;

class OrderStateTableSeeder extends Seeder
{
    public function run()
    {
        $state = [
            'processing'    => 'В обработке',
            'accepted'      => 'Принят',
            'expect_call'   => 'Ожидает звонка',
            'locked'        => 'Заблокирован',
            'completed'     => 'Завершён',
        ];
        
        foreach($state as $key => $val){
            OrderState::create([
                'name'          => $key,
                'display_name'  => $val
            ]);
        }
    }
}
