<?php

use Illuminate\Database\Seeder;

use App\Models\Measurement;

class MeasurementTableSeeder extends Seeder
{
    public function run()
    {
          $measures = [ 
                        'gr' => ['гр', 'грамм'], 
                        'kg' => ['кг', 'килограмм'], 
                        'ml' => ['мл', 'миллилитр'], 
                        'l'  => ['л', 'Литр'],
                    ];
         
         foreach ($measures as $measure => $prop){
             Measurement::create([
                 'name'         => $measure,
                 'display_name' => $prop[0],
                 'description'  => $prop[1]
             ]);
         }
                    
    }
}
