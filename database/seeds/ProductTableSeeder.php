<?php

use Illuminate\Database\Seeder;

use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\Category;
use App\Models\VariantSale;

class ProductTableSeeder extends Seeder
{

    public function run()
    {
        foreach ($this->getData() as $name => $prop){
            $category = Category::where('text', $name)->first();
            foreach($prop['products'] as $product => $p){
                $product = Product::where('route', $p['route'])->get();
                if($product->isEmpty()){
                   $productDB = Product::create([
                        'name' => $p['name'],
                        'shortname' => $p['shortname'],
                        'route' => $p['route'],
                        'image' => '',
                    ]);
                    
                    $category->products()->attach($productDB->id);
                    
                    $productDB->copyImage($p["image"], $prop['dir']);
                
                    foreach($p['sales'] as $sale => $arr){
                        $productDB->variantsales()->create([
                            'measurement'   => $arr['num_measure'] . ' ' . $arr['measure'],
                            'price'         => $arr['price'],
                            'default'       => $arr['default'],
                        ]);
                    }
                    
                    $productDB->productInfo()->create(['info' => '']);
                }
                else{
                    $category->products()->attach($product->first()->id);
                }
            }
        }
    }
    
    public function getData(){
            
        return [
                    'Противоморозные добавки' => [
                        'products' => [
                            'antifriz' => [
                                'name' => 'Виртуоз Антифриз №1 (сухой) концентрат до -15С',
                                'shortname' => 'Виртуоз Антифриз №1 (сухой)',
                                'route' => 'antifriz-suhoy',
                                'image' => 'antifriz1.jpeg',
                                'info'  => "<p><strong>Противоморозная добавка в бетон</strong> «Антифриз» используется в производстве растворов, бетонов, а также клея, при необходимости работы в минусовую температуру до 15 градусов ниже ноля.</p>

<p>Из плюсов данной противоморозной добавки можно выделить то, что у нее достаточно низкий расход на квадратный метр, в несколько раз ниже, чем у аналогов. После ее добавления Вы не обнаружите осадков на дне емкости и вздутия канистр и концентрация добавки в бетон достаточно высокая. Если кратко, то антифриз работает больше как <strong>противоморозные добавки в бетон</strong>, и позволяет работать с бетоном, до 15 градусов ниже ноля. При этом гидратация цемента сохраняет свою интенсивность затвердевания в течении первых 24 часов.</p>

<p>Но как и у каждой морозостойкой добавки у Антифриза есть свои минусы. Его нужно разводить в воде до 3-х минут, чего делать не нужно в готовых жидких добавках. &nbsp;Если Вы используете «напряженный бетон» (через бетонируемое место пропускается электрический ток), в таком случае добавку в бетон «Антифриз использовать нельзя». Из минусов владеет низкой пластификацией бетонов до П4. Для получения необходимого Вам результата, при применении «Антифриз», если Вы хотите получить прочный бетон с высоким коофициэнтом водонепронецаемости, вместе с ним стоит использовать <script>
    document.write('&lt;p&gt;Суперпластификатор «Виртуоз-31» или Гиперпластификатор «Эвробетон».&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;ТЕХНИЧЕСКИЕ ДАННЫЕ:&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;Вид &mdash; сухой&lt;/p&gt;&lt;p&gt;Основа &ndash; сульфаты.&lt;/p&gt;&lt;p&gt;Не разрушает арматуру.&lt;/p&gt;&lt;p&gt;ДСТУ Б В.2.7-171:2008&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;РАСХОД:&lt;/p&gt;&lt;p&gt;Подается в раствор с водой затворения.&lt;/p&gt;&lt;p&gt;При t: 0…-5&lt;sup&gt;0&lt;/sup&gt;С, расход 0,4% от массы цемента&lt;/p&gt;&lt;p&gt;При t: -5…-10&lt;sup&gt;0&lt;/sup&gt;С, расход 0,7% от массы цемента&lt;/p&gt;&lt;p&gt;При t: -10…-15&lt;sup&gt;0&lt;/sup&gt;С, расход 1% от массы цемента&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;Рекомендации для зимнего бетонирования:&lt;/p&gt;&lt;p&gt;1. Не забывать контролировать количество воды затворения. В/ц около 0,5 или на 1 мешок (50 кг) цемента добавлять 25 л воды. Для растворов это соотношение чуть выше 0,7.&lt;/p&gt;&lt;p&gt;2. Максимально стараться сохранить тепло в бетоне, растворе. Укрывать плёнкой, матами, щитами - делать термос. Чем дольше продержится тепло в растворе, особенно важно в первые часы, тем больше пройдет набор марки бетона и тем выше будет качество и прочность бетона.&lt;/p&gt;&lt;p&gt;СРОК ХРАНЕНИЯ:&amp;nbsp;12 месяцев.&lt;/p&gt;&lt;p&gt;МЕРЫ БЕЗОПАСНОСТИ:&amp;nbsp;отвечает санитарно-гигиеническим нормам.&lt;/p&gt;&lt;/div&gt;&lt;/p&gt;');
  </script></p><p>Суперпластификатор «Виртуоз-31» или Гиперпластификатор «Эвробетон».</p><p>&nbsp;</p><p>ТЕХНИЧЕСКИЕ ДАННЫЕ:</p><p>&nbsp;</p><p>Вид &mdash; сухой</p><p>Основа &ndash; сульфаты.</p><p>Не разрушает арматуру.</p><p>ДСТУ Б В.2.7-171:2008</p><p>&nbsp;</p><p>РАСХОД:</p><p>Подается в раствор с водой затворения.</p><p>При t: 0…-5<sup>0</sup>С, расход 0,4% от массы цемента</p><p>При t: -5…-10<sup>0</sup>С, расход 0,7% от массы цемента</p><p>При t: -10…-15<sup>0</sup>С, расход 1% от массы цемента</p><p>&nbsp;</p><p>Рекомендации для зимнего бетонирования:</p><p>1. Не забывать контролировать количество воды затворения. В/ц около 0,5 или на 1 мешок (50 кг) цемента добавлять 25 л воды. Для растворов это соотношение чуть выше 0,7.</p><p>2. Максимально стараться сохранить тепло в бетоне, растворе. Укрывать плёнкой, матами, щитами - делать термос. Чем дольше продержится тепло в растворе, особенно важно в первые часы, тем больше пройдет набор марки бетона и тем выше будет качество и прочность бетона.</p><p>СРОК ХРАНЕНИЯ:&nbsp;12 месяцев.</p><p>МЕРЫ БЕЗОПАСНОСТИ:&nbsp;отвечает санитарно-гигиеническим нормам.</p>",
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 55.80,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 5,
                                        'price' => 297.30,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 10,
                                        'price' => 618.96,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 20,
                                        'price' => 1312.08,
                                        'default' => 0, 
                                    ],
                                ]
                            ],
                            'protivmoroz.jpg' => [
                                'name' => 'Противоморозная добавка "Виртуоз" до -20С',
                                'shortname' => 'Противоморозная добавка "Виртуоз"',
                                'route' => 'protivomoroznaya-dobavka',
                                'image' => 'protivmoroz.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' =>  97.00,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 1789.00,
                                        'default' => 0, 
                                    ],
                                ]
                            ],
                        ],
                        'dir' => 'moroz',
                    ],
                    'Пигменты, красители' => [
                        'products' => [
                            'beliy-zhemchuzniy-p98' => [
                                'name' => 'Белый жемчужный пигмент П-98, максимальная светоотражающая способность до 98%, т.е. высокая белизна в бетоне',
                                'shortname' => 'Белый жемчужный пигмент П-98',
                                'route' => 'zhemchuzhniy-pigment',
                                'image' => 'beliy-zhemchuzniy-p98.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 11004.00,
                                        'default' => 1, 
                                    ],    
                                ]
                            ],
                            'beliy-p99' => [
                                'name' => 'Белый пигмент П-99. Не выгорает. светоотражение до 92%(бетон слабо отбеливает)',
                                'shortname' => 'Белый пигмент П-99',
                                'route' => 'white-pigment',
                                'image' => 'beliy-p99.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 0.5,
                                        'price' => 48.96,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 97.80,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 3,
                                        'price' => 293.46,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2217.24,
                                        'default' => 0, 
                                    ],    
                                ]
                            ],
                            'visheviy-p23' => [
                                'name' => 'Вишневый пигмент П-23. Сочный, не выгорает.',
                                'shortname' => 'Вишневый пигмент П-23',
                                'route' => 'vishneviy-pigment',
                                'image' => 'visheviy-p23.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 3,
                                        'price' => 323.46,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2492.16,
                                        'default' => 0, 
                                    ],  
                                ]
                            ],
                            'zheltiy-p43' => [
                                'name' => 'Желтый пигмент П-43, не выгорает, яркий - Красота!',
                                'shortname' => 'Желтый пигмент П-43',
                                'route' => 'zheltiy-pigment',
                                'image' => 'zheltiy-p43.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 107.88,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 3,
                                        'price' => 323.46,
                                        'default' => 0, 
                                    ],  
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 20,
                                        'price' => 2093.88,
                                        'default' => 0, 
                                    ], 
                                ],
                            ],
                            'zheltiy-temniy-p42' => [
                                'name' => 'Желтый темный пигмент П-42, не выгорает, насыщенный - Красота!',
                                'shortname' => 'Желтый темный пигмент П-42',
                                'route' => 'zheltiy-temniy',
                                'image' => 'zheltiy-temniy-p42.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 0.5,
                                        'price' => 53.94,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 3,
                                        'price' => 323.46,
                                        'default' => 0, 
                                    ],  
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2492.16,
                                        'default' => 0, 
                                    ], 
                                ],
                            ],
                            'zeleniy-p53' => [
                                'name' => 'Зеленый пигмент П-53, не выгорает, яркий - Красота!',
                                'shortname' => 'Зеленый пигмент П-53',
                                'route' => 'zeleniy-pigment',
                                'image' => 'zeleniy-p53.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 0.5,
                                        'price' => 63.96,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 127.80,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2817.24,
                                        'default' => 0, 
                                    ], 
                                ],
                            ],
                            'kirpichniy-p25' => [
                                'name' => 'Кирпичный пигмент П-25, не выгорает, яркий - Красота!',
                                'shortname' => 'Кирпичный пигмент П-25',
                                'route' => 'kirpichniy-pigment',
                                'image' => 'kirpichniy-p25.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 0.5,
                                        'price' => 53.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 107.88,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 3,
                                        'price' => 323.46,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2492.16,
                                        'default' => 0, 
                                    ], 
                                ],
                            ],
                            'korichneviy-krasniy-p16' => [
                                'name' => 'Коричневый пигмент с красным оттенком П-16, не выгорает, яркий - Красота!',
                                'shortname' => 'Коричневый пигмент с красным оттенком П-16',
                                'route' => 's-krasnim-ottenkom',
                                'image' => 'korichneviy-krasniy-p16.jpg',
                                'sales' => [ 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 3,
                                        'price' => 293.46,
                                        'default' => 1, 
                                    ], 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2217.24,
                                        'default' => 0, 
                                    ], 
                                ],
                            ],
                            'korichneviy-temniy-p14' => [
                                'name' => 'Коричневый темный пигмент П-14, не выгорает',
                                'shortname' => 'Коричневый темный пигмент П-14',
                                'route' => 'kor-temniy-pigment',
                                'image' => 'korichneviy-temniy-p14.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 0.5,
                                        'price' => 48.96,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 97.80,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 3,
                                        'price' => 293.46,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2217.24,
                                        'default' => 0, 
                                    ], 
                                ],
                            ],
                            'krasniy-p24' => [
                                'name' => 'Красный пигмент П-24, не выгорает, яркий - Красота!',
                                'shortname' => 'Красный пигмент П-24',
                                'route' => 'krasniy-pigment',
                                'image' => 'krasniy-p24.jpg',
                                'sales' => [ 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 100.00,
                                        'default' => 1, 
                                    ], 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2300.00,
                                        'default' => 0, 
                                    ], 
                                ],
                            ],
                            'krasniy-temniy-p21' => [
                                'name' => 'Красный темный пигмент П-21, не выгорает, яркий - Красота!',
                                'shortname' => 'Красный темный пигмент П-21',
                                'route' => 'krasniy-temniy-pigment',
                                'image' => 'krasniy-temniy-p21.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 0.5,
                                        'price' => 53.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 107.88,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 3,
                                        'price' => 323.46,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2492.16,
                                        'default' => 0, 
                                    ], 
                                ],
                            ],
                            'oranzheviy-temniy-p36' => [
                                'name' => 'Оранжевый темный пигмент П-36, не выгорает, яркий - Красота!',
                                'shortname' => 'Оранжевый темный пигмент П-36',
                                'route' => 'oranzheviy-pigment-temniy',
                                'image' => 'oranzheviy-temniy-p36.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 0.5,
                                        'price' => 63.96,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 127.80,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 3,
                                        'price' => 383.46,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2817.24,
                                        'default' => 0, 
                                    ], 
                                ],
                            ],
                            'cherniy-p10' => [
                                'name' => 'Черный пигмент П-10 Насыщенный чёрный смолянистый! Низкая дозировка - 0,25кг на 10кг цемента, не выгорает.',
                                'shortname' => 'Черный пигмент П-10',
                                'route' => 'cherniy-pigment',
                                'image' => 'cherniy-p10.jpg',
                                'sales' => [ 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 107.88,
                                        'default' => 1, 
                                    ], 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 20,
                                        'price' => 2093.88,
                                        'default' => 0, 
                                    ], 
                                ],
                            ],
                            'cherniy-tepliy' => [
                                'name' => 'Чёрный тёплый (с коричневым оттенком) П-17 не смолянисто чёрный, не выгорает.',
                                'shortname' => 'Чёрный тёплый (с коричневым оттенком) П-17',
                                'route' => 'tepliy-pigment',
                                'image' => 'cherniy-tepliy.jpg',
                                'sales' => [ 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2217.24,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'yantarniy-p41' => [
                                'name' => 'Янтарный пигмент П-41, не выгорает',
                                'shortname' => 'Янтарный пигмент П-41',
                                'route' => 'yantarniy-pigment',
                                'image' => 'yantarniy-p41.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 0.5,
                                        'price' => 53.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 107.88,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 3,
                                        'price' => 323.46,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2492.16,
                                        'default' => 0, 
                                    ], 
                                ],
                            ],
                            'yantarniy-temniy-p40' => [
                                'name' => 'Янтарный темный пигмент П-40, не выгорает, насыщенный',
                                'shortname' => 'Янтарный темный пигмент П-40',
                                'route' => 'yantar-temniy',
                                'image' => 'yantarniy-temniy-p40.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 107.88,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 3,
                                        'price' => 323.46,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 2492.16,
                                        'default' => 0, 
                                    ], 
                                ],
                            ],
                        ],
                        'dir' => 'pigmenti',
                    ],
                    'Пластификаторы для цемента и гипса' => [
                        'products' => [
                            'virtuoz-21' => [
                                'name' => 'Супер пластификатор для теплого пола "Виртуоз-21"',
                                'shortname' => 'Супер пластификатор "Виртуоз-21"',
                                'route' => 'superplastifikator-tepliy-pol',
                                'image' => 'virtuoz-21.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 25.86,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 100.00,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 196.56,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'antifriz-1' => [
                                'name' => 'Виртуоз Антифриз №1 (сухой) концентрат до -15С',
                                'shortname' => 'Виртуоз Антифриз №1 (сухой)',
                                'route' => 'antifriz-suhoy',
                                'image' => 'antifriz-1.jpeg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 55.80,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 5,
                                        'price' => 297.30,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 10,
                                        'price' => 618.96,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 1312.08,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'evrobeton' => [
                                'name' => 'Гиперпластификатор «Евробетон». (для цемента и гипса)',
                                'shortname' => 'Гиперпластификатор «Евробетон»',
                                'route' => 'giperplastifikator-evrobeton',
                                'image' => 'evrobeton.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 52.86,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 264.36,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 528.78,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'multiplatex-m-7' => [
                                'name' => 'ГРУНТ «Мультилатекс М-7». Штукатурка не отслаивается, уменьшается расход краски, шпаклёвка не осыпается, плитка не отскочит',
                                'shortname' => 'ГРУНТ «Мультилатекс М-7»',
                                'route' => 'grunt-multilateks',
                                'image' => 'multiplatex-m-7.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 133.44,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 638.70,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'virtuoz-PST-31a' => [
                                'name' => 'Добавка для тонких стяжек "Виртуоз ПСТ-31А". Европа',
                                'shortname' => 'Добавка "Виртуоз ПСТ-31А"',
                                'route' => 'dlya-tonkih-styazhek',
                                'image' => 'virtuoz-PST-31a.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 69.00,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 345.00,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 690.00,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'zaminitel-izvesti-9' => [
                                'name' => 'Заменитель извести №9 концентрат. 1литр заменяет 1 тонну извести',
                                'shortname' => 'Заменитель извести №9',
                                'route' => 'zamenitel-izvesti',
                                'image' => 'zaminitel-izvesti-9.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 135.00,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'plastifikator-c3' => [
                                'name' => 'Пластификатор (сухой) С-3 для бетона, плитки. Украина',
                                'shortname' => 'Пластификатор (сухой) С-3',
                                'route' => 'plastifikato-c3',
                                'image' => 'plastifikator-c3.jpeg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 73.20,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 5,
                                        'price' => 384.78,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 10,
                                        'price' => 761.74,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 25,
                                        'price' => 1749.06,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'plastifikator-kash' => [
                                'name' => 'Пластификатор «КэШ» для кладочного и штукатурного растворов.',
                                'shortname' => 'Пластификатор «КэШ»',
                                'route' => 'plastifikator-kesh',
                                'image' => 'plastifikator-kash.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 25.86,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 196.56,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'virtuoz-31' => [
                                'name' => 'Суперпластификатор Виртуоз-31',
                                'shortname' => 'Суперпластификатор Виртуоз-31',
                                'route' => 'plastifikato-c31',
                                'image' => 'virtuoz-31.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 25.86,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'turbodefomer-TDF77' => [
                                'name' => 'ТурбоДефомер ТДФ-77. Франция',
                                'shortname' => 'ТурбоДефомер ТДФ-77',
                                'route' => 'turbodefomer',
                                'image' => 'turbodefomer-TDF77.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'г',
                                        'num_measure' => 100,
                                        'price' => 40.56,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 399.06,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'shatl' => [
                                'name' => 'Ускоритель «Шатл»',
                                'shortname' => 'Ускоритель «Шатл»',
                                'route' => 'uskoritel-shatl',
                                'image' => 'shatl.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 25.14,
                                        'default' => 1, 
                                    ],  
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 97.20,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 1189.36,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'virtuoz-start' => [
                                'name' => 'Ускоритель схватывания «Виртуоз Старт». Франция',
                                'shortname' => 'Ускоритель схватывания «Виртуоз Старт»',
                                'route' => 'uskoritel-shvativaniya',
                                'image' => 'virtuoz-start.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'г',
                                        'num_measure' => 100,
                                        'price' => 22.74,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'г',
                                        'num_measure' => 300,
                                        'price' => 67.20,
                                        'default' => 0, 
                                    ], 
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 219.84,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'virtuoz-AB-1' => [
                                'name' => 'Фибра Виртуоз АВ-1. Франция',
                                'shortname' => 'Фибра Виртуоз АВ-1',
                                'route' => 'fibra-ab1',
                                'image' => 'virtuoz-AB-1.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 622.08,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                        ],
                        'dir' => 'plastifikatori',
                    ],
                    'Добавки для строительства дорог' => [
                        'products' => [
                            'LBS-stabilizator' => [
                                'name' => 'LBS Жидкий стабилизатор грунта (USA)',
                                'shortname' => 'LBS Жидкий стабилизатор грунта',
                                'route' => 'LBS-plastificator',
                                'image' => 'LBS-stabilizator.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 560.00,
                                        'default' => 1, 
                                    ],
                                ]
                            ],
                            'Stabilizator-M10+50' => [
                                'name' => 'Стабилизатор грунта М10+50',
                                'shortname' => 'Стабилизатор грунта М10+50',
                                'route' => 'stabilizator-M10',
                                'image' => 'Stabilizator-M10+50.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 200.00,
                                        'default' => 1, 
                                    ],
                                ]
                            ],
                        ],
                        'dir' => 'dorogi',
                    ],
                    'Гидрофобизаторы и очистители' => [
                        'products' => [
                            'gidrophobizator-virtuoz-f1' => [
                                'name' => 'Гидрофобизатор «Виртуоз Ф-1» Акрил, ЛАК "мокрый камень", Концентрат 1:3, расход 1л на 20м2, Европа',
                                'shortname' => 'Гидрофобизатор «Виртуоз Ф-1»',
                                'route' => 'gidrofobizator-Virtuoz-f1',
                                'image' => 'gidrophobizator-virtuoz-f1.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 650.00,
                                        'default' => 1, 
                                    ],
                                ]
                            ],
                            'gidrophobizator-virtuoz-f3' => [
                                'name' => 'Гидрофобизатор Виртуоз Ф-3, концентрат 1:20 (аналог "ГКЖ-11") силикон, Украина',
                                'shortname' => 'Гидрофобизатор Виртуоз Ф-3',
                                'route' => 'gidrofobizator-Virtuoz-f3',
                                'image' => 'gidrophobizator-virtuoz-f3.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 871.20,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 1738.44,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'gidrophobizator-virtuoz-f9' => [
                                'name' => 'Гидрофобизатор Виртуоз Ф-9 кремнийорганика, концентрат 1:24, прозрачный (для кирпича, бетона, древесины и др.). 1литр на 125м2',
                                'shortname' => 'Гидрофобизатор Виртуоз Ф-9',
                                'route' => 'gidrofobizator-Virtuoz-f9',
                                'image' => 'gidrophobizator-virtuoz-f9.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 2531.28,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'gidrophobizator-virtuoz-f11' => [
                                'name' => 'Гидрофобизатор Виртуоз ФМ-11 на органическом растворителе с эффектом "мокрый камень"',
                                'shortname' => 'Гидрофобизатор Виртуоз ФМ-11',
                                'route' => 'gidrofobizator-Virtuoz-f11',
                                'image' => 'gidrophobizator-virtuoz-f11.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 0,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'ochistitel-soft1' => [
                                'name' => 'Очиститель СОФТ-1 Концентрат 1:4 (мгновенно растворяет высолы, белые пятна соли, цемент, ржавчину. Безопасный для человека), США',
                                'shortname' => 'Очиститель СОФТ-1',
                                'route' => 'ochistitel-soft1',
                                'image' => 'ochistitel-soft1.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 0,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'ochistitel-soft2' => [
                                'name' => 'Очиститель СОФТ-2 Концентрат 1:2 (мгновенно растворяет высолы, белые пятна соли, цемент, ржавчину. Безопасный), США',
                                'shortname' => 'Очиститель СОФТ-2',
                                'route' => 'ochistitel-soft2',
                                'image' => 'ochistitel-soft2.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 0.5,
                                        'price' => 79.98,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 159.96,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'Pronikayushchaya-gidroizolyatsiya-os1' => [
                                'name' => 'Проникающая гидроизоляция бетона "Виртуоз ОС-1" (проникающая в бетон до 1м толщины), расход 1кг/1м2',
                                'shortname' => 'Проникающая гидроизоляция бетона "Виртуоз ОС-1"',
                                'route' => 'Pronikayushchaya-gidroizolyatsiya-os1',
                                'image' => 'Pronikayushchaya-gidroizolyatsiya-os1.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 15,
                                        'price' => 1968.78,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                        ],
                        'dir' => 'gidrophobizator',
                    ],
                    'Мастика гидроизоляционная' => [
                        'products' => [
                            'mastika-kauchukovaya' => [
                                'name' => 'Мастика гидроизоляционная битумно-каучуковая "Барьер", не густая, жидкая (более гибкая на морозе чем просто битум, наносить можно в мороз до -20С)',
                                'shortname' => 'Мастика гидроизоляционная битумно-каучуковая "Барьер"',
                                'route' => 'mastika-kauchukovaya',
                                'image' => 'mastika-kauchukovaya.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 5,
                                        'price' => 292.50,
                                        'default' => 1, 
                                    ],
                                ]
                            ],
                            'mastika-silver' => [
                                'name' => 'Мастика гидроизоляционная каучуковая "Сильвер", гибкая, не трескается зимой, наносить можно в мороз до -20С, летом не плавится как битум). Цвет СЕРЫЙ',
                                'shortname' => 'Мастика гидроизоляционная каучуковая "Сильвер"',
                                'route' => 'mastika-silver',
                                'image' => 'mastika-silver.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 3,
                                        'price' => 258.36,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 5,
                                        'price' => 433.38,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 10,
                                        'price' => 848.94,
                                        'default' => 0, 
                                    ],
                                ]
                            ],
                        ],
                        'dir' => 'mastika',
                    ],
                    'Обмазочная гидроизоляция (со стороны действия воды)' => [
                        'products' => [
                            'virtuoz-akva' => [
                                'name' => 'Виртуоз «АКВА» гидроизоляция эластичная, жидкая. Европа',
                                'shortname' => 'Виртуоз «АКВА» гидроизоляция эластичная',
                                'route' => 'virtuoz-akva',
                                'image' => 'virtuoz-akva.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 203.94,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 1019.58,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'obmazochnaya-gidroizolyaciya' => [
                                'name' => 'Обмазочная гидроизоляция "Виртуоз ОС-4", для фундаментов, бассейнов, санузлов, душевых комнат',
                                'shortname' => 'Обмазочная гидроизоляция "Виртуоз ОС-4"',
                                'route' => 'obmazochnaya-gidroizolyaciya',
                                'image' => 'obmazochnaya-gidroizolyaciya.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 337.50,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 5,
                                        'price' => 1012.00,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                        ],
                        'dir' => 'obmazochnaya',
                    ],
                    'Проникающая гидроизоляция бетона' => [
                        'products' => [
                            'Pronikayushchaya-gidroizolyatsiya-os1' => [
                                'name' => 'Проникающая гидроизоляция бетона "Виртуоз ОС-1" (проникающая в бетон до 1м толщины), расход 1кг/1м2',
                                'shortname' => 'Проникающая гидроизоляция бетона "Виртуоз ОС-1"',
                                'route' => 'Pronikayushchaya-gidroizolyatsiya-os1',
                                'image' => 'Pronikayushchaya-gidroizolyatsiya-os1.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 15,
                                        'price' => 1968.78,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'Gidrodobavka-betona' => [
                                'name' => 'Гидродобавка для бетона "Виртуоз ОС-2" проникающая гидроизоляция (добавка для жидкого бетона); расход 1кг на 100 кг цемента',
                                'shortname' => 'Гидродобавка для бетона "Виртуоз ОС-2"',
                                'route' => 'Gidrodobavka-betona',
                                'image' => 'Gidrodobavka-betona.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 4,
                                        'price' => 558.30,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 12,
                                        'price' => 1687.50,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'Pronikayushchaya-gidroizolyatsiya-os3' => [
                                'name' => 'Проникающая гидроизоляция бетона "Виртуоз ОС-3" (для швов, трещин, стыков, примыканий), расход 1,5 кг/1метр погонный',
                                'shortname' => 'Проникающая гидроизоляция бетона "Виртуоз ОС-3"',
                                'route' => 'Pronikayushchaya-gidroizolyatsiya-os3',
                                'image' => 'Pronikayushchaya-gidroizolyatsiya-os3.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 5,
                                        'price' => 618.78,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 15,
                                        'price' => 1822.50,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'modifikator-os5' => [
                                'name' => 'Модификатор Виртуоз ОС-5, цемент твердеет очень быстро от 5 мин до 2-х часов, может твердеть прямо в воде, 1л на 100 кг цемента',
                                'shortname' => 'Модификатор Виртуоз ОС-5',
                                'route' => 'modifikator-os5',
                                'image' => 'modifikator-os5.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 1490.64,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 2981.28,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'Pronikayushchaya-gidroizolyatsiya-os7' => [
                                'name' => 'Гидропломба "Виртуоз ОС-7" (проникающая быстротвердеющая от 30сек гидроизоляция по бетону)',
                                'shortname' => 'Гидропломба "Виртуоз ОС-7"',
                                'route' => 'Pronikayushchaya-gidroizolyatsiya-os7',
                                'image' => 'Pronikayushchaya-gidroizolyatsiya-os7.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 146.28,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 5,
                                        'price' => 697.50,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 10,
                                        'price' => 2025.00,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                        ],
                        'dir' => 'pronikayuschaya',
                    ],
                    'Морилки' => [
                        'products' => [
                            'Morilka-beitz1' => [
                                'name' => 'Морилка черная Бейц-1, водорастворимая, экологичная, без запаха.',
                                'shortname' => 'Морилка черная Бейц-1',
                                'route' => 'Morilka-beitz1',
                                'image' => 'Morilka-beitz1.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 53.16,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 260.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 521.94,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'Morilka-beitz2' => [
                                'name' => 'Морилка коричневая Бейц-2, водорастворимая, экологичная, без запаха.',
                                'shortname' => 'Морилка коричневая Бейц-2',
                                'route' => 'Morilka-beitz2',
                                'image' => 'Morilka-beitz2.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 53.16,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 260.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 521.94,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'Morilka-beitz3' => [
                                'name' => 'Морилка красная Бейц-3, водорастворимая, экологичная, без запаха.',
                                'shortname' => 'Морилка красная Бейц-3',
                                'route' => 'Morilka-beitz3',
                                'image' => 'Morilka-beitz3.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 53.16,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 260.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 521.94,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'Morilka-beitz4' => [
                                'name' => 'Морилка розовая Бейц-4, водорастворимая, экологичная, без запаха',
                                'shortname' => 'Морилка розовая Бейц-4',
                                'route' => 'Morilka-beitz4',
                                'image' => 'Morilka-beitz4.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 53.16,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 260.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 521.94,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'Morilka-beitz5' => [
                                'name' => 'Морилка оранжевая Бейц-5, водорастворимая, экологичная, без запаха.',
                                'shortname' => 'Морилка оранжевая Бейц-5',
                                'route' => 'Morilka-beitz5',
                                'image' => 'Morilka-beitz5.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 53.16,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 260.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 521.94,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'Morilka-beitz6' => [
                                'name' => 'Морилка желтая Бейц-6, водорастворимая, экологичная, без запаха',
                                'shortname' => 'Морилка желтая Бейц-6',
                                'route' => 'Morilka-beitz6',
                                'image' => 'Morilka-beitz6.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 53.16,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 260.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 521.94,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'Morilka-beitz7' => [
                                'name' => 'Морилка зеленая Бейц-7, водорастворимая, экологичная, без запаха.',
                                'shortname' => 'Морилка зеленая Бейц-7',
                                'route' => 'Morilka-beitz7',
                                'image' => 'Morilka-beitz7.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 53.16,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 260.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 521.94,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'Morilka-beitz8' => [
                                'name' => 'Морилка синяя Бейц-8, водорастворимая, экологичная, без запаха',
                                'shortname' => 'Морилка синяя Бейц-8',
                                'route' => 'Morilka-beitz8',
                                'image' => 'Morilka-beitz8.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 53.16,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 260.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 521.94,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'Morilka-beitz9' => [
                                'name' => 'Морилка фиолетовая Бейц-9, водорастворимая, экологичная, без запаха.',
                                'shortname' => 'Морилка фиолетовая Бейц-9',
                                'route' => 'Morilka-beitz9',
                                'image' => 'Morilka-beitz9.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 53.16,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 260.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 521.94,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'Morilka-beitz10' => [
                                'name' => 'Морилка белая Бейц-10, водорастворимая, экологичная, без запаха',
                                'shortname' => 'Морилка белая Бейц-10',
                                'route' => 'Morilka-beitz10',
                                'image' => 'Morilka-beitz10.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 53.16,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 260.94,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 521.94,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                        ],
                        'dir' => 'morilki',
                    ],
                    'Огнебиозащита' => [
                        'products' => [
                            'Ognebiozashchita-drevesiny299' => [
                                'name' => 'Огнебиозащита для древесины Виртуоз-299 Концентрат. Тонирующая, "5в1" = защита от огня 2-й категории+влаги+плесени+жучков (не вымывается дождём). Экологичная, без запаха; концентрат 1:9',
                                'shortname' => 'Огнебиозащита для древесины Виртуоз-299',
                                'route' => 'Ognebiozashchita-drevesiny299',
                                'image' => 'Ognebiozashchita-drevesiny299.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 135.30,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 660.96,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 1312.50,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                        ],
                        'dir' => 'ognebiozaschita',
                    ], 
                    'Антиплесень' => [
                        'products' => [
                            'antigribkovaya-propitka' => [
                                'name' => 'Антигрибковая пропитка Плесень-1 (биосеребро) мгновенно убивает плесень, вирусы, бактерии, грибы, безопасная для человека. США',
                                'shortname' => 'Антигрибковая пропитка Плесень-1',
                                'route' => 'antigribkovaya-propitka',
                                'image' => 'antigribkovaya-propitka.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 750.00,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 1500.00,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'antigribkovaya-propitka-2' => [
                                'name' => 'Антигрибковая пропитка-добавка Плесень-2. Уничтожение + долговременная защита. Европа',
                                'shortname' => 'Антигрибковая пропитка-добавка Плесень-2',
                                'route' => 'antigribkovaya-propitka-2',
                                'image' => 'antigribkovaya-propitka-2.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 0.5,
                                        'price' => 45.00,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 90.00,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 450.00,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                        ],
                        'dir' => 'antigribok',
                    ], 
                    'Грунтовки' => [
                        'products' => [ 
                            'virtuoz-akva' => [
                                'name' => 'Виртуоз «АКВА» гидроизоляция эластичная, жидкая. Европа',
                                'shortname' => 'Виртуоз «АКВА» гидроизоляция эластичная',
                                'route' => 'virtuoz-akva',
                                'image' => 'virtuoz-akva.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 203.94,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 1019.58,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'gidrofabizator-mokriy-kamen' => [
                                'name' => 'Гидрофобизатор «Виртуоз Ф-1» Акрил, ЛАК "мокрый камень"',
                                'shortname' => 'Гидрофобизатор «Виртуоз Ф-1» Акрил',
                                'route' => 'gidrofabizator-mokriy-kamen',
                                'image' => 'gidrofabizator-mokriy-kamen.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 20.02,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 350.10,
                                        'default' => 0, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 10,
                                        'price' => 700.20,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'gidrofabizator-mokriy-kamen' => [
                                'name' => 'Гидрофобизатор Виртуоз Ф-9 кремнийорганика, концентрат 1:24, прозрачный (для кирпича, бетона, древесины и др.). 1литр на 125м2',
                                'shortname' => 'Гидрофобизатор Виртуоз Ф-9 кремнийорганика',
                                'route' => 'Gidrofobizator-Virtuoz-f9',
                                'image' => 'Gidrofobizator-Virtuoz-f9.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 506.28,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 2531.28,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'grunt-multilateks' => [
                                'name' => 'ГРУНТ «Мультилатекс М-7». Штукатурка не отслаивается, уменьшается расход краски, шпаклёвка не осыпается, плитка не отскочит, Европа',
                                'shortname' => 'ГРУНТ «Мультилатекс М-7»',
                                'route' => 'grunt-multilateks',
                                'image' => 'grunt-multilateks.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 1,
                                        'price' => 133.44,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'л',
                                        'num_measure' => 5,
                                        'price' => 638.70,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                        ],
                        'dir' => 'gruntovki',
                    ],   
                    'Клей для плитки' => [
                        'products' => [  
                             'premiks-standart' => [
                                'name' => 'Премикс "ПСМ-11" стандарт КЛЕЙ для ПЛИТКИ, пластичный, отлично удерживает плитку, долговечный, 1 пакет конц. 60грамм=75кг. клея= на 5м2 Европа-Корея',
                                'shortname' => 'Премикс "ПСМ-11" стандарт КЛЕЙ для ПЛИТКИ',
                                'route' => 'premiks-standart',
                                'image' => 'premiks-standart.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'г',
                                        'num_measure' => 60,
                                        'price' => 29.28,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'premiks-pm12' => [
                                'name' => 'Премикс "ПСМ-12" КЛЕЙ для ГРЕСС плиток - пластичный, отлично удерживает плитку, долговечный, не боится мороза , 1пакет=32грн на 5м2. Европа-Корея',
                                'shortname' => 'Премикс "ПСМ-12" КЛЕЙ для ГРЕСС плиток',
                                'route' => 'premiks-pm12',
                                'image' => 'premiks-pm12.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'г',
                                        'num_measure' => 300,
                                        'price' => 82.14,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'premiks-pm17' => [
                                'name' => 'Премикс "ПСМ-17" Премиум клей супер эластичный, держит плитку при нагревании (полы с подогревом), охлаждении, долговечный, морозостойкий 1пакет на 5м2. Европа-Корея',
                                'shortname' => 'Премикс "ПСМ-17" Премиум клей супер эластичный',
                                'route' => 'premiks-pm17',
                                'image' => 'premiks-pm17.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'г',
                                        'num_measure' => 560,
                                        'price' => 140.64,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'premiks-pst83' => [
                                'name' => 'Премикс "ПСТ-83", «Стандарт» - клей для пенопласта, отличная фиксация, не боится мороза, долговечный, 1пакет на 5м2). Европа-Корея',
                                'shortname' => 'Премикс "ПСТ-83", «Стандарт» - клей для пенопласта',
                                'route' => 'premiks-pst83',
                                'image' => 'premiks-pst83.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'г',
                                        'num_measure' => 300,
                                        'price' => 82.14,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'premiks-pst85' => [
                                'name' => 'Премикс "ПСТ-85" Премиум клей и защита для пенопласта. Долговечный, морозостойкий клей. 1 пакет на 5м2. Корея',
                                'shortname' => 'Премикс "ПСТ-85" Премиум клей и защита для пенопласта',
                                'route' => 'premiks-pst85',
                                'image' => 'premiks-pst85.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'г',
                                        'num_measure' => 460,
                                        'price' => 118.14,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'premiks-pm115' => [
                                'name' => 'Премикс "ПСМ-115" Клей для мрамора и мозаики (морозостойкий, долговечный, эластичный, отлично держит плитки, 1пакет на 5м2). Европа-Корея',
                                'shortname' => 'Премикс "ПСМ-115" Клей для мрамора и мозаики',
                                'route' => 'premiks-pm115',
                                'image' => 'premiks-pm115.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'г',
                                        'num_measure' => 460,
                                        'price' => 118.14,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'premiks-psm117' => [
                                'name' => 'Премикс "ПСМ-117" Клей для натурального и искусственного камня, отлично держит, морозостойкий, долговечный, 1пакет на 5м2). Европа-Корея',
                                'shortname' => 'Премикс "ПСМ-117" Клей для натурального и искусственного камня',
                                'route' => 'premiks-psm117',
                                'image' => 'premiks-psm117.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'г',
                                        'num_measure' => 460,
                                        'price' => 118.14,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'premiks-pst190' => [
                                'name' => 'Премикс "ПСТ-190" Премиум клей для минеральной ваты и защиты, морозостойкий, долговечный, прочный, не трескается, 1пакет на 5м2. Европа-Корея',
                                'shortname' => 'Премикс "ПСТ-190" Премиум клей для минеральной ваты и защиты',
                                'route' => 'premiks-pst190',
                                'image' => 'premiks-pst190.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'г',
                                        'num_measure' => 460,
                                        'price' => 118.14,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                            'premiks-pst220' => [
                                'name' => 'Премикс "ПСТ-220" Клей для гипсокартона, отличная фиксация, увеличенное время работы от 60 минут, Европа-Корея',
                                'shortname' => 'Премикс "ПСТ-220" Клей для гипсокартона',
                                'route' => 'premiks-pst220',
                                'image' => 'premiks-pst220.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'г',
                                        'num_measure' => 400,
                                        'price' => 101.28,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                        ],
                        'dir' => 'kleya',
                    ], 
                    'Краска для предпосевной подготовки семян' => [
                        'products' => [
                            'kraska-dla-semyan' => [
                                'name' => 'краска для семян матовая',
                                'shortname' => 'краска для семян матовая',
                                'route' => 'kraska-dla-semyan',
                                'image' => 'kraska-dla-semyan.png',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 180.00,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 30,
                                        'price' => 3600.00,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'kraska-perlamutrovaya' => [
                                'name' => 'краска для семян перламутровая',
                                'shortname' => 'краска для семян перламутровая',
                                'route' => 'kraska-perlamutrovaya',
                                'image' => 'kraska-perlamutrovaya.png',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 180.00,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                        ],
                        'dir' => 'dly-predposevnoy',
                    ],  
                    'Обработка растений' => [
                        'products' => [
                            'prilipatel-ink2' => [
                                'name' => 'Прилипатель ИНК-2',
                                'shortname' => 'Прилипатель ИНК-2',
                                'route' => 'prilipatel-ink2',
                                'image' => 'prilipatel-ink2.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 1,
                                        'price' => 35.00,
                                        'default' => 1, 
                                    ],
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 20,
                                        'price' => 700.00,
                                        'default' => 0, 
                                    ],
                                ],
                            ],
                            'skleivatel-ink6' => [
                                'name' => 'Склеиватель ИНК-6',
                                'shortname' => 'Склеиватель ИНК-6',
                                'route' => 'skleivatel-ink6',
                                'image' => 'skleivatel-ink6.jpg',
                                'sales' => [
                                    [
                                        'measure' => 'кг',
                                        'num_measure' => 20,
                                        'price' => 1200.00,
                                        'default' => 1, 
                                    ],
                                ],
                            ],
                        ],
                        'dir' => 'obrabotka-rasteniy',
                    ],
        ];
    }
}
