<?php

use App\Models\User;
use App\Models\UserInfo;
use App\Models\Role;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
            //очистка таблицы
            User::truncate();
            
            //создание администратора
            $role = Role::where('name', 'admin')->first();
            $user = User::create([
                    'name' => 'admin',
                    'password' => \Hash::make('virtuozz@#admin'),
                    'email' => 'admin@admin.com',
                    'user_type' => $role->id
                ]); 
            $info = new UserInfo(['phone' => $faker->phoneNumber]);
            $user->userInfo()->save($info);
            $user->attachRole($role);
            
            //создание редактора
            /*
            $role = Role::where('name', 'editor')->first();
            $user = User::create([
                    'name' => 'editor',
                    'password' => \Hash::make('123456'),
                    'email' => 'editor@editor.com',
                    'user_type' => $role->id
                ]); 
            $info = new UserInfo(['phone' => $faker->phoneNumber]);
            $user->userInfo()->save($info);
            $user->attachRole($role);
            */
            
            //генерация 20 пользователей
            /*
            $role = Role::where('name', 'user')->first();     
            for($i=0; $i < 20; $i++)
            {
                $user = User::create([
                    'name' => $faker->word($nb = 6),
                    'password' => $faker->word($nb = 6),
                    'email' => $faker->email(),
                    'user_type' => 3
                ]);  
                
                $info = new UserInfo(['phone' => $faker->phoneNumber]);
                $user->userInfo()->save($info);
                $user->attachRole($role);
            }
            */
    }
}
