<?php

use Illuminate\Database\Seeder;

use App\Models\ProductSpecial;

class ProductSpecialTableSeeder extends Seeder
{
    public function run()
    {
        $category = [
            'leader'    => 'Лидеры продаж',
            'promo'     => 'Акции'
        ];
        
        foreach($category as $key => $val){
            ProductSpecial::create([
                'name'          => $key,
                'display_name'  => $val,
            ]);
        }
        
    }
}
