<?php

use App\Models\Role;
use App\Models\Permission;
use App\Models\User;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        DB::table('permissions')->delete();
        
        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'Администратор сайта';
        $admin->description  = 'Может всё';
        $admin->save();

        $editor = new Role();
        $editor->name         = 'editor';
        $editor->display_name = 'Редактор сайта';
        $editor->description  = 'Может редактировать разделы,продукты и т.д. на сайте';
        $editor->save();
        
        $user = new Role();
        $user->name         = 'user';
        $user->display_name = 'Зарегистрированный пользователь сайта';
        $user->description  = 'Есть свой профиль на сайте';
        $user->save();
        
        $editUser = new Permission();
        $editUser->name         = 'edit-users';
        $editUser->display_name = 'Редактирование пользователей';
        $editUser->description  = 'Создание/редактирование пользователей';
        $editUser->save();
        
        $editProducts = new Permission();
        $editProducts->name         = 'edit-products';
        $editProducts->display_name = 'Редактирование продукта';
        $editProducts->description  = 'Создание/редактиррование продукта';
        $editProducts->save();
        
        $admin->attachPermissions([$editUser, $editProducts]);
        $editor->attachPermission($editProducts);
        
        //$user = User::where('name', '=', 'admin')->first();
        //$user->attachRole($admin);
    }
}
