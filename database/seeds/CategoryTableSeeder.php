<?php

use App\Models\Category;
use App\Models\Section;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        //$faker = Faker\Factory::create();
        
        //очистка таблицы
        //Category::truncate();
        
        $sections = [
                        'Добавки и пластификаторы' => [
                            "categories" => [
                                'Противоморозные добавки'               => [
                                    "image" => "protivdobavki.jpg", 
                                    "route" => "zamorozka",
                                ], 
                                'Пигменты, красители'                   => [
                                    "image" => "kraski.jpg",
                                    "route" => "pigmenti",
                                ],
                                'Пластификаторы для цемента и гипса'    => [
                                    "image" => "plastcementgips.jpg",
                                    "route" => "plastificatory",
                                ],
                                'Добавки для строительства дорог'       => [
                                    "image" => "stroit.jpg",
                                    "route" => "dlya-dorog",
                                ],
                            ],
                            "img"   => "dob-plast.jpg",
                            "name"  => "dobavki",
                            "route" => "dobavki",
                         ],
                         'Гидроизоляция' => [
                            "categories" => [
                                'Гидрофобизаторы и очистители'                          => [
                                    "image" => "gidrofabiz.jpg",
                                    "route" => "gidrofabizatory-ocist",
                                ],
                                'Мастика гидроизоляционная'                             => [
                                    "image" => "mastika.jpg",
                                    "route" => "mastika",
                                ],
                                'Обмазочная гидроизоляция (со стороны действия воды)'   => [
                                    "image" => "obmaz.jpg",
                                    "route" => "obmazochnaya",
                                ],
                                'Проникающая гидроизоляция бетона'                      => [
                                    "image" => "pronik.jpg",
                                    "route" => "pronikayuschaya",
                                ],
                            ],
                            "img"   => "gidro.jpg",
                            "name"  => "gidro",
                            "route" => "gidroizolyaciya",
                         ],
                         'Обработка древесины' => [
                            "categories" => [
                                'Морилки'       => [
                                    "image" => "morilki.jpg",
                                    "route" => "morilki",
                                ],
                                'Огнебиозащита' => [
                                    "image" => "ogbiozasch.jpg",
                                    "route" => "ognebiozaschita",
                                ],
                            ],
                            "img"   => "obr-drev.jpg",
                            "name"  => "drevisina",
                            "route" => "obrabotka-drevesiny",
                         ],
                         'Для ремонта и отделки' => [
                            "categories" => [
                                'Антиплесень'       => [
                                    "image" => "antiplesen.jpg",
                                    "route" => "antigribok",
                                ],
                                'Грунтовки'         => [
                                    "image" => "grunt.jpg",
                                    "route" => "gruntovki",
                                ],
                                'Клей для плитки'   => [
                                    "image" => "kley.jpg",
                                    "route" => "kleya",
                                ],
                                'Краски'            => [
                                    "image" => "kaski.jpg",
                                    "route" => "kraski",
                                ],
                            ],
                            "img"   => "rem-otd.jpg",
                            "name"  => "remont",
                            "route" => "remont-i-otdelka",
                         ],
                         'Агро' => [
                            "categories" => [
                                'Краска для предпосевной подготовки семян'  => [
                                    "image" => "kraskasemyan.jpg",
                                    "route" => "dly-predposevnoy",
                                ],
                                'Обработка растений'                        => [
                                    "image" => "obrrasteniy.jpg",
                                    "route" => "obrabotka-rasteniy",
                                ],
                            ],
                            "img"   => "agro.jpg",
                            "name"  => "agro",
                            "route" => "agro",
                         ],   
                     ];
        
        $i = 1;               
        foreach ($sections as $section => $property){
            $sectionDB = Section::create([
                'text'      => $section,
                'priority'  => $i,
                'select'    => 0,
                'image'     => "",
                'route'     => "/" . $property["route"],
            ]);
            
            $sectionDB->copyImageFromSection($property["img"]);
            
            foreach($property["categories"] as $category => $prop){
                $categoryDB = Category::create([
                    'text'          => $category,
                    'section_id'    => $sectionDB->id,
                    'image'         => "",
                    'route'         => "/" . $property["route"] . "/" . $prop["route"],
                ]);    
                
                $categoryDB->copyImageFromCategory($prop["image"], $property["name"]);
            }
            
            $i++; 
        }

    }   

}
