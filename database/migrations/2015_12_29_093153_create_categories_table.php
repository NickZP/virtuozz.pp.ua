<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('section_id')->unsigned();
            $table->string('text', 100)->unique();
            $table->string('route');
            $table->string('image');
            $table->timestamps();
        });
        
        Schema::table("categories", function(Blueprint $table){
            $table->foreign('section_id')->references('id')->on('sections')->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::drop('categories');
    }
}
