<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoTable extends Migration
{

    public function up()
    {
        Schema::create('seo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('keywords');
            $table->text('description');
            $table->text('og_description');
        });
    }

    public function down()
    {
        Schema::table('seo', function (Blueprint $table) {
            Schema::dropIfExists('seo');
        });
    }
}
