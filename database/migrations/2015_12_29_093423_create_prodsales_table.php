<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdsalesTable extends Migration
{
    public function up()
    {
        Schema::create("prodsales", function(Blueprint $table){
            $table->integer("id")->nullable();
            $table->integer("prod_id")->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists("prodsales");
    }
}
