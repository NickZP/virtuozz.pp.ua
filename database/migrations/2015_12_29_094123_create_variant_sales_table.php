<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariantSalesTable extends Migration
{
    public function up()
    {
        Schema::create('variant_sales', function(Blueprint $table){
           $table->increments('id');
           $table->integer('product_id')->unsigned();
           $table->string('measurement');
           $table->decimal('price', 6, 2);
           $table->boolean('default')->default(0);
           $table->timestamps();
        });
        
        Schema::table('variant_sales', function(Blueprint $table){
            $table->foreign('product_id')->references('id')->on('products')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('variant_sales');
    }
}
