<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderInfosTable extends Migration
{
    public function up()
    {
        Schema::create("order_infos", function(Blueprint $table){
            $table->increments("id");
            $table->integer("order_id")->unsigned();
            $table->integer("variant_sale_id")->unsigned();
            $table->integer("amount");
        });
        
        Schema::table("order_infos", function(Blueprint $table){
           $table->foreign("order_id")
                    ->references("id")
                    ->on("orders")
                    ->onUpdate('cascade')
                    ->onDelete('cascade'); 
           $table->foreign("variant_sale_id")
                    ->references("id")
                    ->on("variant_sales");            
        });
    }

    public function down()
    {
        Schema::drop("order_infos");
    }
}
