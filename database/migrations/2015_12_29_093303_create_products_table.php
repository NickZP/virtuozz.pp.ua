<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create("products", function(Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->string("shortname")->unique();
            $table->string("route")->unique();
            $table->integer("comment")->unsigned();
            //с этим товаром покупают
            $table->integer("product_sale")->unsigned();
            $table->string("image");
            $table->timestamps();           
        });
        
        Schema::create("category_product", function(Blueprint $table){
           $table->integer("category_id")->unsigned();
           $table->integer("product_id")->unsigned();
           
           $table->primary(["category_id", "product_id"]); 
        });
        
        Schema::table("category_product", function(Blueprint $table){
           $table->foreign("category_id")
                        ->references('id')
                        ->on('categories')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists("category_product");
        Schema::dropIfExists("products");
    }
}
