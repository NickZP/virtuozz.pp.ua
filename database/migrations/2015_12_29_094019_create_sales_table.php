<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    public function up()
    {
        Schema::create("sales", function(Blueprint $table){
            $table->integer("id");
            $table->string("qt");
            $table->float("price");
        });
    }

    public function down()
    {
        Schema::dropIfExists("sales");
    }
}
