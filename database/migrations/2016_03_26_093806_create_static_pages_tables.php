<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticPagesTables extends Migration
{
    public function up()
    {
        Schema::create('static_pages', function(Blueprint $table){
            $table->increments('id');
            $table->string('tag')->unique();
            $table->string('title');
            $table->text('content');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('static_pages');
    }
}
