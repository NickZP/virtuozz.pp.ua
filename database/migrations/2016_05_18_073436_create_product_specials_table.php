<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSpecialsTable extends Migration
{

    public function up()
    {
        Schema::create('product_specials', function(Blueprint $table){
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name');
            $table->string('description');
        });
        
        Schema::create('product_product_special', function(Blueprint $table){
            $table->integer('product_id')->unsigned();
            $table->integer('product_special_id')->unsigned();
           
            $table->primary(['product_id', 'product_special_id']); 
        });
        
        Schema::table("product_product_special", function(Blueprint $table){
           $table->foreign('product_id')
                        ->references('id')
                        ->on('products')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('product_product_special');
        Schema::dropIfExists('product_specials');
    }
}
