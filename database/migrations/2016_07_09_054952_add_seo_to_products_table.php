<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoToProductsTable extends Migration
{
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('seo_id')->after('image')->unsigned()->nullable();

            $table->foreign('seo_id')
                        ->references('id')
                        ->on('seo')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('products_seo_id_foreign');
            $table->dropColumn('seo_id');
        });
    }
}
