<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasurementTable extends Migration
{
    public function up()
    {
        Schema::create('measurements', function(Blueprint $table){
           $table->increments('id');
           $table->string('name', 30)->unique();
           $table->string('display_name', 30)->unique();
           $table->string('description'); 
        });
    }

    public function down()
    {
        Schema::dropIfExists('measurements');
    }
}
