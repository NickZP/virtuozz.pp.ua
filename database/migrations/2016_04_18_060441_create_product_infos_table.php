<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductInfosTable extends Migration
{
    public function up()
    {
        Schema::create("product_infos", function(Blueprint $table){
            $table->increments("id");
            $table->integer("product_id")->unsigned();
            $table->text("info");
        });
        
        Schema::table("product_infos", function(Blueprint $table){
           $table->foreign("product_id")
                    ->references("id")
                    ->on("products")
                    ->onUpdate('cascade')
                    ->onDelete('cascade');          
        });
    }

    public function down()
    {
        Schema::dropIfExists("product_infos");
    }
}
