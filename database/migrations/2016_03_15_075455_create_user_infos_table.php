<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfosTable extends Migration
{
    public function up()
    {
        Schema::create('user_infos', function(Blueprint $table){
           $table->increments('id');
           $table->integer('user_id')->unsigned();
           $table->string('name');
           $table->string('email'); 
           $table->string('phone'); 
        });
        
        Schema::table('user_infos', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('user_infos');
    }
}
