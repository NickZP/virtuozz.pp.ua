<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderStatesTable extends Migration
{

    public function up()
    {
        Schema::create('order_states', function(Blueprint $table){
            $table->increments('id');
            $table->string('name', 100)->unique();
            $table->string('display_name')->unique();
            $table->timestamps();
        });
        
        Schema::create('order_state_histories', function(Blueprint $table){
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('order_state_id')->unsigned();
            $table->string('comment');
            $table->dateTime('changed_at');
            $table->timestamps();
        });
        
        Schema::table('order_state_histories', function(Blueprint $table){
            $table->foreign('order_id')
                    ->references('id')
                    ->on('orders')
                    ->onUpdate('cascade')
                    ->onDelete('cascade'); 
            $table->foreign('order_state_id')
                    ->references('id')
                    ->on('order_states');     
        });
    }

    public function down()
    {
        Schema::drop('order_state_histories');
        Schema::dropIfExists('order_states');
    }
}
