<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderHistoriesTable extends Migration
{
    public function up()
    {
        Schema::create("order_histories", function(Blueprint $table){
            $table->increments("id");
            $table->integer("order_id")->unsigned();
            $table->text("history");
            $table->decimal("price", 8, 2);
            $table->string("email");
            $table->string("name");
            $table->string("phone");
            $table->string("address");
        });
        
        Schema::table("order_histories", function(Blueprint $table){
           $table->foreign("order_id")
                    ->references("id")
                    ->on("orders")
                    ->onUpdate('cascade')
                    ->onDelete('cascade');          
        });
    }

    public function down()
    {
        Schema::drop("order_histories");
    }
}
